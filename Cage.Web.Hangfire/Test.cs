﻿using System;
using System.Diagnostics;
using System.Threading;

namespace Cage.Web.Hangfire
{
    public class Test
    {
        private static readonly object Lock = new object();

        public void Execute()
        {
            if (Monitor.TryEnter(Lock, TimeSpan.FromSeconds(5)))
            {
                Debug.WriteLine($"[{System.Threading.Thread.CurrentThread.ManagedThreadId}][{DateTime.Now}] /_/_/_/_/_/_/ Recurring start. /_/_/_/_/_/_/");
                System.Threading.Thread.Sleep(60 * 2 * 1000);
                Debug.WriteLine($"[{System.Threading.Thread.CurrentThread.ManagedThreadId}][{DateTime.Now}] /_/_/_/_/_/_/ Recurring end. /_/_/_/_/_/_/");
                Monitor.Exit(Lock);
            }
            else
            {
                Debug.WriteLine($"[{System.Threading.Thread.CurrentThread.ManagedThreadId}][{DateTime.Now}] /_/_/_/_/_/_/ Recurring skip. /_/_/_/_/_/_/");
            }
        }
    }
}
