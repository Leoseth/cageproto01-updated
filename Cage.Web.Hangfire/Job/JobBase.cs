﻿using System;
using System.Threading;
using Cage.DAL.Repository.Infrastructure;

namespace Cage.Web.Hangfire.Job
{
    public abstract class JobBase
    {
        protected readonly ICageUnitOfWorkAsync UnitOfWork;

        protected JobBase(
            ICageUnitOfWorkAsync unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        private static readonly object Lock = new object();
        public void Exec()
        {
            if (!Monitor.TryEnter(Lock, TimeSpan.FromSeconds(60))) return;

            Job();
            Monitor.Exit(Lock);
        }
        protected abstract void Job();
    }
}
