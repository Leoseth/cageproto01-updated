﻿using System;
using System.Linq;
using Cage.Core;
using Identity.UI.ViewModels.Account;
using Microsoft.AspNet.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cage.Web.Areas.Admin.Controllers;
using Cage.DAL.Db.Models;

namespace Cage.Web.Test
{
    [TestClass]
    public class UnitTest1 : TestBase
    {
        [TestMethod]
        public void Register()
        {
            // Act
            var register = new RegisterViewModel
            {
                Email = "test001@abc.def",
                Password = "zaq1",
                ConfirmPassword = "zaq1",
                ReferrerMode = "default",
                Referrer = "world",
                UserName = "test001",
                Consultant = ""
            };

            var result =
                AccountController.Register(register).Result;

            // Assert
            Assert.IsNotNull(result);

            var addedUser = UserManager.FindByEmail(register.Email);
            Assert.IsNotNull(addedUser);
            //Assert.AreEqual(TestBirthDate, addedUser.BirthDate);
        }

        [TestMethod]
        public void Registers()
        {
            foreach (var i in Enumerable.Range(1, 100))
            {
                var register = new RegisterViewModel
                {
                    Email = $"test{i:000}@abc.def",
                    Password = "zaq1",
                    ConfirmPassword = "zaq1",
                    ReferrerMode = "default",
                    Referrer = "world",
                    UserName = $"test{i:000}"
                };

                var result =
                    AccountController.Register(register).Result;
            }

            Assert.IsNull(null);
        }

        [TestMethod]
        public void CreateUserTest()
        {
            var result = UserManager.Create(new ApplicationUser
            {
                UserName = "world",
                Email = "world@abc.def",
                Created = DateTime.UtcNow,
                UserDetails = new ApplicationUserDetails
                {
                    ReferrerUserId = Guid.NewGuid(),
                    UserType = (int)CageCode.UserType.Member,
                    UserClass = (int)CageCode.UserClass.Grade_00
                }
            }, "zaq1");

            //AGoZu+FXOBdLC4XksDy1HSxwZdFffe0S0jWr/JVHCORojkSUdSvVHCzf/6ukvgVOrg==
            Assert.IsNotNull(result);
        }
    }
}
