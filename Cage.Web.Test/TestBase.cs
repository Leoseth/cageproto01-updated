﻿using Cage.DAL.Db;
using Cage.DAL.Db.Models;
using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service;
using Cage.DAL.Service.Interfaces;
using Cage.DAL.Service.Manager;
//using Cage.Models;
using Cage.Web.Areas.Admin.Controllers;
//using Cage.Web.Controllers;
using Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataProtection;
using Moq;

namespace Cage.Web.Test
{
    public class TestBase
    {
        private CageDataContext Context = new CageDataContext();
        protected CageUnitOfWork UnitOfWork;
        protected ApplicationUserManager UserManager;
        protected ApplicationSignInManager SignInManager;

        //protected IPaymentMasterService PaymentMasterService;
        //protected IFeeMasterService FeeMasterService;

        protected IApplicationUserService ApplicationUserService;
        protected IApplicationUserDetailService ApplicationUserDetailService;

        //protected IUserPassportService UserPassportService;
        //protected IUserPassportHistoryService UserPassportHistoryService;

        //protected IUserPaymentService UserPaymentService;
        //protected IUserPaymentHistoryService UserPaymentHistoryService;

        //protected IUserPaymentQueueService UserPaymentQueueService;
        //protected IUserPaymentQueueExpiredService UserPaymentQueueExpiredService;

        protected IUserProvideHelpService UserProvideHelpService;
        //protected IUserGetHelpService UserGetHelpService;

        protected IUserHelpAssignedService UserHelpAssignedService;

        protected IProvideHelpQueueService ProvideHelpQueueService;
        protected IProvideHelpQueueHistoryService ProvideHelpQueueHistoryService;

        //protected IGetHelpQueueService GetHelpQueueService;
        //protected IGetHelpQueueHistoryService GetHelpQueueHistoryService;

        //protected IUserHelpPaymentService UserHelpPaymentService;

        protected IUserHierarchyService UserHierarchyService;
        protected IUserHierarchyReservedService UserHierarchyReservedService;

        //protected IUpgradePaymentService UpgaPaymentService;

        //protected IUserReferralService UserReferralService;

        protected IFileService FileService;

        protected IUserProvideHelpMonthlyHistoryService UserProvideHelpMonthlyHistoryService;
        protected IUserProvideHelpRelesedHistoryService UserProvideHelpRelesedHistoryService;

        protected ProvideHelpManager ProvideHelpManager;

        protected ManageController ManageController;
        //protected UpgradeController UpgradeController;
        protected AccountController AccountController;

        public TestBase()
        {
            UnitOfWork = new CageUnitOfWork(Context);

            var id = new IdentityFactoryOptions<ApplicationUserManager>
            {
                DataProtectionProvider = new DpapiDataProtectionProvider("Test")
            };

            // Manager
            UserManager = Startup.CreateApplicationUserManager(id, Context);
            var mockAuthenticationManager = new Mock<IAuthenticationManager>();
            SignInManager = new ApplicationSignInManager(
                UserManager,
                mockAuthenticationManager.Object
                );

            // Service
            InitService();

            // Controller
            AccountController = new AccountController(UserManager, SignInManager, ApplicationUserDetailService);
            //ManageController = new ManageController(UserManager, SignInManager, UnitOfWork, FileService);
            //UpgradeController = new UpgradeController(UnitOfWork, UserPaymentService, UserPaymentHistoryService,
            //    UserPaymentQueueService, UserHierarchyReservedService,
            //    FeeMasterService, UpgaPaymentService, ApplicationUserDetailService, UserPassportService, UserHierarchyService);

            // Other Managers
            ProvideHelpManager = new ProvideHelpManager(
                UnitOfWork,
                UserProvideHelpService,
                UserProvideHelpRelesedHistoryService,
                UserProvideHelpMonthlyHistoryService,
                UserHelpAssignedService,
                ProvideHelpQueueService,
                ProvideHelpQueueHistoryService);

        }

        private void InitService()
        {
            UnitOfWork = new CageUnitOfWork(Context);
            //PaymentMasterService =
            //    new PaymentMasterService(new CageRepository<PaymentMaster>(Context, UnitOfWork));

            //FeeMasterService = new FeeMasterService(new CageRepository<FeeMaster>(Context, UnitOfWork));

            ApplicationUserService = new ApplicationUserService(new CageRepository<ApplicationUser>(Context, UnitOfWork));
            ApplicationUserDetailService =
                new ApplicationUserDetailService(new CageRepository<ApplicationUserDetails>(Context, UnitOfWork));

            UserHierarchyService = new UserHierarchyService(new CageRepository<UserHierarchy>(Context, UnitOfWork));

            //UserPassportService = new UserPassportService(new CageRepository<UserPassport>(Context, UnitOfWork));
            //UserPassportHistoryService = new UserPassportHistoryService(new CageRepository<UserPassportHistory>(Context, UnitOfWork));

            //UserPaymentQueueService =
            //    new UserPaymentQueueService(new CageRepository<UserPaymentQueue>(Context, UnitOfWork));
            //UserPaymentQueueExpiredService =
            //    new UserPaymentQueueExpiredService(new CageRepository<UserPaymentQueueExpired>(Context, UnitOfWork));
            //UserPaymentHistoryService =
            //    new UserPaymentHistoryService(new CageRepository<UserPaymentHistory>(Context, UnitOfWork));
            //UserPaymentService =
            //    new UserPaymentService(new CageRepository<UserPayment>(Context, UnitOfWork));

            UserProvideHelpService =
                new UserProvideHelpService(new CageRepository<UserProvideHelp>(Context, UnitOfWork));
            //UserGetHelpService =
            //    new UserGetHelpService(new CageRepository<UserGetHelp>(Context, UnitOfWork));

            UserHelpAssignedService =
                new UserHelpAssignedService(new CageRepository<UserHelpAssigned>(Context, UnitOfWork));

            ProvideHelpQueueService =
                new ProvideHelpQueueService(new CageRepository<ProvideHelpQueue>(Context, UnitOfWork));
            ProvideHelpQueueHistoryService =
                new ProvideHelpQueueHistoryService(new CageRepository<ProvideHelpQueueHistory>(Context, UnitOfWork));

            //GetHelpQueueService =
            //    new GetHelpQueueService(new CageRepository<GetHelpQueue>(Context, UnitOfWork));
            //GetHelpQueueHistoryService =
            //    new GetHelpQueueHistoryService(new CageRepository<GetHelpQueueHistory>(Context, UnitOfWork));

            //UserHelpPaymentService =
            //    new UserHelpPaymentService(new CageRepository<UserHelpPayment>(Context, UnitOfWork));

            //UserReferralService =
            //    new UserReferralService(new CageRepository<UserReferral>(Context, UnitOfWork));

            FileService = new FileService(new CageRepository<File>(Context, UnitOfWork));

            UserProvideHelpMonthlyHistoryService =
                new UserProvideHelpMonthlyHistoryService(new CageRepository<UserProvideHelpMonthlyHistory>(Context,
                    UnitOfWork));
        }
    }
}
