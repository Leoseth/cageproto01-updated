﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cage.Web.Test
{
    [TestClass]
    public class InsertDummyData : TestBase
    {

        [TestMethod]
        public void InsertDummy_UserProvideHelpMonthlyHistories()
        {
            const int maxPh = 30;
            var now = DateTime.UtcNow;
            var users = UserManager.Users.ToArray();
            foreach (var applicationUser in users)
            {
                var ph = 0;
                var userId = applicationUser.Id;
                foreach (var i in Enumerable.Range(1, 5))
                {
                    var month = now.Date.AddMonths(-i);
                    var phLimit = maxPh - ph;
                    var newPh = new Random().Next(1, phLimit);
                    if (newPh > 0)
                        ProvideHelpManager.UpsertUserProvideHelpMonthlyHistoriesAmount(userId, month, newPh);
                    ph += newPh;
                }
                UnitOfWork.SaveChanges();
            }
            Assert.IsNull(null);
        }
    }
}
