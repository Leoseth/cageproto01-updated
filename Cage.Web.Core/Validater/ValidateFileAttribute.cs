﻿using System.ComponentModel.DataAnnotations;
using System.Web;
using Abl.Mvc;

namespace Cage.Web.Core.Validater
{
    public class ValidateFileAttribute : RequiredAttribute
    {
        private readonly int _fileSizeMb;
        public ValidateFileAttribute(int fileSizeMb = 1)
        {
            _fileSizeMb = fileSizeMb;
        }
        public override bool IsValid(object value)
        {
            var file = value as HttpPostedFileBase;
            if (file == null)
                return false;

            if (!file.IsImage())
                return false;

            if (file.ContentLength > 1 * 1024 * 1024 * _fileSizeMb)
                return false;
            return false;
        }
    }
}
