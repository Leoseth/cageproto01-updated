using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Cage.Core;

namespace Cage.Web.Core
{
    public class AuthorizeRoleAttribute : AuthorizeAttribute
    {
        public AuthorizeRoleAttribute(CageCode.RolesEnum role)
        {
            var code = role.GetHashCode();
            var list = new List<string>();
            foreach (var item in Enum.GetValues(typeof(CageCode.RolesEnum)))
            {
                var tmpCode = item.GetHashCode();
                if (tmpCode >= code)
                {
                    list.Add(item.ToString());
                }
            }
            Roles = string.Join(",", list);
        }
    }
}