﻿using System.Net.Http;

namespace Cage.Web.Core.Service
{
    public class IpAddressUtils
    {
        private static readonly HttpClient HttpClient;
        private const string IpToCountryCodeApiUrlFormat = "https://ipapi.co/{0}/country/";

        static IpAddressUtils()
        {
            HttpClient = new HttpClient();
        }

        /// <summary>
        /// Ipから国コードを判定して取得する
        /// </summary>
        /// <param name="ip"></param>
        /// <returns>JP, US etc</returns>
        public static string Lookup(string ip)
        {
            var url = string.Format(IpToCountryCodeApiUrlFormat, ip);
            var resultStr = "";
            var response = HttpClient.GetAsync(url).Result;
            if (response.IsSuccessStatusCode)
            {
                resultStr = response.Content.ReadAsStringAsync().Result;
            }
            return resultStr;
        }
    }
}
