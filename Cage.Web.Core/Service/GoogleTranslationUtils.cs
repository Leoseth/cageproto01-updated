﻿using System.Net.Http;

namespace Cage.Web.Core.Service
{
    public class GoogleTranslationUtils
    {
        private static readonly HttpClient HttpClient;
        private const string ApiUrlFormat = "https://translation.googleapis.com/language/translate/v2?key={0}&source={1}&target={2}&q={3}";
        public const string ApiKey = "AIzaSyCl707U8uQfQoMTeBm3cY79kPfh22Jf4SM";

        static GoogleTranslationUtils()
        {
            HttpClient = new HttpClient();
        }

        public static string Translate(string words, string sourceLang = "en", string targetLang = "ja")
        {
            var encordedWords = System.Web.HttpUtility.UrlEncode(words);

            var url = string.Format(ApiUrlFormat, ApiKey, sourceLang, targetLang, encordedWords);
            var resultStr = "";
            var response = HttpClient.GetAsync(url).Result;
            if (response.IsSuccessStatusCode)
            {
                resultStr = response.Content.ReadAsStringAsync().Result;
            }
            return resultStr;
        }
    }
}
