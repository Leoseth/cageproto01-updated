﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;
using System.Web.Routing;
using Cage.Core;

namespace Cage.Web.Core
{
    /// <summary>
    /// ユーザが持つ最高権限のロールに対して表示ページをハンドリングする
    /// </summary>
    public class AuthorizeCheckUsersHighestAuthorityAttribute : AuthorizeAttribute
    {
        private readonly CageCode.RolesEnum _targetRole;
        private readonly string _controller;
        private readonly string _action;
        public AuthorizeCheckUsersHighestAuthorityAttribute(CageCode.RolesEnum targetRole, string controller, string action)
        {
            _targetRole = targetRole;
            _controller = controller;
            _action = action;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var allowAnonymous = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true) ||
                                    filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(
                                        typeof(AllowAnonymousAttribute), true);
            if (allowAnonymous)
                return;

            var ignore = filterContext.ActionDescriptor.IsDefined(typeof(IgnoreAuthorizeCheckUsersHighestAuthorityAttribute), true) ||
                                    filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(
                                        typeof(IgnoreAuthorizeCheckUsersHighestAuthorityAttribute), true);

            if(ignore)
                return;

            var user = filterContext.HttpContext.User as ClaimsPrincipal;
            var r = user.Claims.Where(x => x.Type == ClaimTypes.Role);
            if (!r.Any())
                return;

            // ユーザが保持する最高権限
            var usersHighestAuthority = r.Select(role => Enum.Parse(typeof(CageCode.RolesEnum), role.Value).GetHashCode()).Concat(new[] {0}).Max();

            // 最高権限が指定ロールであればリダイレクト
            if (_targetRole.GetHashCode() == usersHighestAuthority)
            {
                // リダイレクト先が同じ場合は無限ループになるので回避
                if (!filterContext.RouteData.Values["controller"].ToString().ToLower().Equals(_controller.ToLower()) &&
                    !filterContext.RouteData.Values["action"].ToString().ToLower().Equals(_action.ToLower()))
                    filterContext.Result =
                        new RedirectToRouteResult(
                            new RouteValueDictionary(new {controller = _controller, action = _action}));

            }

            base.OnAuthorization(filterContext);
        }
    }

    /// <summary>
    /// AuthorizeCheckUsersHighestAuthorityAttributeを無視する(Logoutなど)
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class IgnoreAuthorizeCheckUsersHighestAuthorityAttribute : Attribute
    {
    }
}
