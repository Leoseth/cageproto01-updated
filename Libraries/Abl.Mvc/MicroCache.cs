﻿using System;
using System.Runtime.Caching;
using System.Threading;

namespace Abl.Mvc
{
    public interface IMicroCache<T>
    {
        bool Contains(string key);
        T GetOrAdd(string key, Func<T> loadFunction, Func<CacheItemPolicy> getCacheItemPolicyFunction);
        void Remove(string key);
    }

    public class MicroCache<T> : IMicroCache<T>
    {
        public MicroCache(ObjectCache objectCache)
        {
            if (objectCache == null)
                throw new ArgumentNullException(nameof(objectCache));

            this._cache = objectCache;
        }
        private readonly ObjectCache _cache;
        private readonly ReaderWriterLockSlim _synclock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);

        public bool Contains(string key)
        {
            _synclock.EnterReadLock();
            try
            {
                return this._cache.Contains(key);
            }
            finally
            {
                _synclock.ExitReadLock();
            }
        }

        public T GetOrAdd(string key, Func<T> loadFunction, Func<CacheItemPolicy> getCacheItemPolicyFunction)
        {
            LazyLock<T> lazy;
            bool success;

            _synclock.EnterReadLock();
            try
            {
                success = this.TryGetValue(key, out lazy);
            }
            finally
            {
                _synclock.ExitReadLock();
            }

            if (!success)
            {
                _synclock.EnterWriteLock();
                try
                {
                    if (!this.TryGetValue(key, out lazy))
                    {
                        lazy = new LazyLock<T>();
                        var policy = getCacheItemPolicyFunction();
                        this._cache.Add(key, lazy, policy);
                    }
                }
                finally
                {
                    _synclock.ExitWriteLock();
                }
            }

            return lazy.Get(loadFunction);
        }

        public void Remove(string key)
        {
            _synclock.EnterWriteLock();
            try
            {
                this._cache.Remove(key);
            }
            finally
            {
                _synclock.ExitWriteLock();
            }
        }


        private bool TryGetValue(string key, out LazyLock<T> value)
        {
            value = (LazyLock<T>)this._cache.Get(key);
            if (value != null)
            {
                return true;
            }
            return false;
        }

        private sealed class LazyLock<T>
        {
            private volatile bool got;
            private T value;

            public T Get(Func<T> activator)
            {
                if (!got)
                {
                    if (activator == null)
                    {
                        return default(T);
                    }

                    lock (this)
                    {
                        if (!got)
                        {
                            value = activator();

                            got = true;
                        }
                    }
                }

                return value;
            }
        }
    }
}
