﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Abl.Mvc
{
    public static class EnumMvcExtensions
    {
        public static IEnumerable<SelectList> ToSelectList(this Enum enumValue)
        {
            var list = new SelectList(enumValue.ToDictionary(), "Key", "Value");
            return null;
        }
        public static IEnumerable<SelectListItem> ToSelectListItem(this Enum enumValue)
        {
            return from Enum e in Enum.GetValues(enumValue.GetType())
                select new SelectListItem
                {
                    Selected = e.Equals(enumValue),
                    Text = e.ToDescription(),
                    Value = e.ToString()
                };
        }
    }
}

