﻿using System;
using System.Collections.Generic;
using NLog;

namespace Abl.Mvc.Logger
{
    public static class LogEntry
    {
        private static readonly ILogger Logger = NLog.LogManager.GetCurrentClassLogger();

        //public static string GetLogEntry(object message)
        //{
        //    return GetLogEntry(message, HttpContext.Current.Request);
        //}

        public static string GetLogEntry(object message, System.Web.HttpRequestBase ctx)
        {
            if (ctx != null)
            {
                List<string> parameters = new List<string>();
                try
                {
                    foreach (string key in ctx.Form.AllKeys)
                    {
                        if (!key.StartsWith("__"))
                            parameters.Add($"{key}={ctx.Form[key]}");
                    }
                }
                catch (Exception ex)
                {
                    parameters.Add($"{"error"}={ex.Message}");
                }

                try
                {
                    string url = "";
                    try
                    {
                        url = ctx.Url.Host + ctx.RawUrl;
                    }
                    catch
                    {
                        //RawUrlが取れない場合があるため
                        url = ctx.Url.ToString();
                    }

                    string userId = "";
                    try
                    {
                        //TODO:UserId
                        //userId = User.Identity.GetUserId();
                    }
                    catch
                    {
                    }

                    return string.Format(
                        string.Join(Environment.NewLine,
                            new string[]
                            {
                                "{0}",
                                " Url: {1}",
                                " User: {2}",
                                " Parameters: {3}",
                                " UserAgent : {4}"
                            }
                            ),
                        message,
                        url,
                        userId,
                        string.Join("&", parameters.ToArray()),
                        ctx.UserAgent
                        );
                }
                catch (Exception ex)
                {
                    Logger.Warn(ex, "詳細情報が取得できませんでした");
                    return $"{message}{Environment.NewLine} 詳細情報が取得できませんでした";
                }
            }

            return $"{message}";
        }
    }
}
