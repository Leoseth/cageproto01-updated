﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace Abl.Helpers
{
    public class WebsiteThumbnail
    {
        protected string Url;
        protected int Width, Height;
        protected int ThumbWidth, ThumbHeight;
        protected Bitmap Bmp;

        /// <summary>
        /// Generates a website thumbnail for the given URL
        /// </summary>
        /// <param name="url">Address of website from which to generate the
        /// thumbnail</param>
        /// <param name="width">Browser width</param>
        /// <param name="height">Browser height</param>
        /// <param name="thumbWidth">Width of generated thumbnail</param>
        /// <param name="thumbHeight">Height of generated thumbnail</param>
        /// <returns></returns>
        public static Bitmap GetThumbnail(string url, int width, int height,
          int thumbWidth, int thumbHeight)
        {
            var thumbnail = new WebsiteThumbnail(url, width, height, thumbWidth, thumbHeight);
            return thumbnail.GetThumbnail();
        }

        public static void SaveThumbnail(string url, int width, int height, int thumbWidth, int thymbHeight, string savePath)
        {
            using (var bitmap = GetThumbnail(url, width, height, thumbWidth, thymbHeight))
            {
                bitmap.Save(savePath, System.Drawing.Imaging.ImageFormat.Png);
            }
        }

        /// <summary>
        /// Protected constructor
        /// </summary>
        protected WebsiteThumbnail(string url, int width, int height,
          int thumbWidth, int thumbHeight)
        {
            Url = url;
            Width = width;
            Height = height;
            ThumbWidth = thumbWidth;
            ThumbHeight = thumbHeight;
        }

        /// <summary>
        /// Returns a thumbnail for the current member values
        /// </summary>
        /// <returns>Thumbnail bitmap</returns>
        protected Bitmap GetThumbnail()
        {
            // WebBrowser is an ActiveX control that must be run in a
            // single-threaded apartment so create a thread to create the
            // control and generate the thumbnail
            var thread = new Thread(GetThumbnailWorker);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            thread.Join();
            return Bmp.GetThumbnailImage(ThumbWidth, ThumbHeight,
              null, IntPtr.Zero) as Bitmap;
        }

        /// <summary>
        /// Creates a WebBrowser control to generate the thumbnail image
        /// Must be called from a single-threaded apartment
        /// </summary>
        protected void GetThumbnailWorker()
        {
            using (var browser = new WebBrowser())
            {
                browser.ClientSize = new Size(Width, Height);
                browser.ScrollBarsEnabled = false;
                browser.ScriptErrorsSuppressed = true;
                browser.Navigate(Url);

                // Wait for control to load page
                while (browser.ReadyState != WebBrowserReadyState.Complete)
                    Application.DoEvents();

                // Render browser content to bitmap
                Bmp = new Bitmap(Width, Height);
                browser.DrawToBitmap(Bmp, new Rectangle(0, 0,
                  Width, Height));
            }
        }
    }
}
