﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abl.Utils
{
    public static class DateTimeUtil
    {
        public static DateTime GetFirstDayOfNextMonth(DateTime dateTime)
        {
            var nextMonth = dateTime.AddMonths(1);
            return new DateTime(nextMonth.Year, nextMonth.Month, 1);
        }
    }
}
