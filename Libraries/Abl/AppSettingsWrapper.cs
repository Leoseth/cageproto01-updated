﻿using System;
using System.Collections.Specialized;
using System.Configuration;

namespace Abl
{
    /// <summary>
    /// System.Core.Configuration.AppSettingsのラッパークラス
    /// </summary>
    public abstract class AppSettingsWrapper
    {
        private static readonly NameValueCollection AppSettings = ConfigurationManager.AppSettings;

        /// <summary>
        /// string型の設定値を取得します。
        /// </summary>
        /// <remarks>
        /// 指定されたキーが不正な場合、string.Emptyを返却します。
        /// </remarks>
        /// <param name="key">キー</param>
        /// <returns>string型の設定値</returns>
        protected static string GetSetting(string key)
        {
            return AppSettings[key];
        }

        /// <summary>
        /// string array
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="separator">separator</param>
        /// <returns></returns>
        protected static string[] GetSetting(string key, char separator)
        {
            try
            {
                return string.IsNullOrWhiteSpace(GetSetting(key))
                    ? new[] { "" }
                    : GetSetting(key).Split(separator);
            }
            catch (Exception ex)
            {
                throw new Exception("invalid key:" + key, ex);
            }
        }

        /// <summary>
        /// int型の設定値を取得します。
        /// </summary>
        /// <remarks>
        /// 指定されたキーが不正な場合、エラーを返却します。
        /// </remarks>
        /// <param name="key">キー</param>
        /// <returns>int型の設定値</returns>
        protected static int GetSettingAsInt(string key)
        {
            try
            {
                return int.Parse(GetSetting(key));
            }
            catch (Exception ex)
            {
                throw new Exception("invalid key:" + key, ex);
            }
        }

        /// <summary>
        /// float型の設定値を取得します。
        /// </summary>
        /// <remarks>
        /// 指定されたキーが不正な場合、エラーを返却します。
        /// </remarks>
        /// <param name="key">キー</param>
        /// <returns>float型の設定値</returns>
        protected static float GetSettingAsFloat(string key)
        {
            try
            {
                return float.Parse(GetSetting(key));
            }
            catch (Exception ex)
            {
                throw new Exception("invalid key:" + key, ex);
            }
        }

        /// <summary>
        /// double型の設定値を取得します。
        /// </summary>
        /// <remarks>
        /// 指定されたキーが不正な場合、エラーを返却します。
        /// </remarks>
        /// <param name="key">キー</param>
        /// <returns>float型の設定値</returns>
        protected static double GetSettingAsDouble(string key)
        {
            try
            {
                return double.Parse(GetSetting(key));
            }
            catch (Exception ex)
            {
                throw new Exception("invalid key:" + key, ex);
            }
        }

        /// <summary>
        /// decimal型の設定値を取得します。
        /// </summary>
        /// <param name="key"></param>
        /// <returns>decimal型の設定値</returns>
        protected static decimal GetSettingAsDecimal(string key)
        {
            try
            {
                return decimal.Parse(GetSetting(key));
            }
            catch (Exception ex)
            {
                throw new Exception("invalid key:" + key, ex);
            }
        }

        /// <summary>
        /// decimal array
        /// </summary>
        /// <param name="key"></param>
        /// <param name="separators"></param>
        /// <returns></returns>
        protected static decimal[] GetSettingAsDecimal(string key, char separators)
        {
            try
            {
                return Array.ConvertAll(GetSetting(key, separators), decimal.Parse);
            }
            catch(Exception ex)
            {
                throw new Exception("invalid key:" + key, ex);
            }
        }

        /// <summary>
        /// bool型の設定値を取得します。
        /// </summary>
        /// <remarks>
        /// 指定されたキーが存在しない場合、falseを返却します。
        /// </remarks>
        /// <param name="key">キー</param>
        /// <returns>bool型の設定値</returns>
        protected static bool GetSettingAsBool(string key)
        {
            try
            {
                return bool.Parse(GetSetting(key));
            }
            catch (Exception ex)
            {
                throw new Exception("invalid key:" + key, ex);
            }
        }

        /// <summary>
        /// DateTime型の設定値を取得します。
        /// </summary>
        /// <remarks>
        /// 指定されたキーが存在しない場合、現在時刻を返却します。
        /// </remarks>
        /// <param name="key">キー</param>
        /// <returns>DateTime型の設定値</returns>
        protected static DateTime? GetSettingAsDateTime(string key)
        {
            try
            {
                return DateTime.Parse(GetSetting(key));
            }
            catch (Exception ex)
            {
                throw new Exception("invalid key:" + key, ex);
            }
        }

        /// <summary>
        /// TimeSpan型の設定値を取得します。
        /// </summary>
        /// <remarks>
        /// 指定されたキーが存在しない場合TimeSpan.Zeroを返却します。
        /// </remarks>
        /// <param name="key">キー</param>
        /// <returns>TimeSpan型の設定値</returns>
        protected static TimeSpan GetSettingAsTimeSpan(string key)
        {
            try
            {
                return TimeSpan.Parse(GetSetting(key));
            }
            catch (Exception ex)
            {
                throw new Exception("invalid key:" + key, ex);
            }
        }
    }
}
