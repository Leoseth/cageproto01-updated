﻿using System;
using System.Runtime.Caching;
using System.Threading.Tasks;

namespace Abl
{
    /// http://cpratt.co/thread-safe-strongly-typed-memory-caching-c-sharp/
    
    public static class ObjectCacheExtensions
    {
        public static T AddOrGetExisting<T>(this ObjectCache cache,
                                            string key,
                                            Func<T> valueFactory,
                                            CacheItemPolicy policy)
        {
            var newValue = new Lazy<T>(valueFactory);
            var oldValue = cache.AddOrGetExisting(key, newValue, policy) as Lazy<T>;

            try
            {
                return (oldValue ?? newValue).Value;
            }
            catch
            {
                cache.Remove(key);
                throw;
            }
        }

        public static async Task<T> AddOrGetExistingAsync<T>(
            this ObjectCache cache,
            string key,
            Func<Task<T>> valueFactory,
            CacheItemPolicy policy)
        {
            var newValue = new AsyncLazy<T>(valueFactory);
            var oldValue = cache.AddOrGetExisting(key, newValue, policy) as Lazy<T>;

            try
            {
                return oldValue != null ? oldValue.Value : await newValue.Value;
            }
            catch
            {
                cache.Remove(key);
                throw;
            }
        }
    }

    public class AsyncLazy<T> : Lazy<Task<T>>
    {
        public AsyncLazy(Func<T> valueFactory) :
            base(() => Task.Factory.StartNew(valueFactory))
        { }
        public AsyncLazy(Func<Task<T>> taskFactory) :
            base(() => Task.Factory.StartNew(() => taskFactory()).Unwrap())
        { }
    }
}
