﻿using System;

namespace Abl
{
    public static class IntExtensions
    {
        /// <summary>
        /// yyyyMm(6桁整数値)
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static int ToYm(this DateTime datetime)
        {
            return datetime.Year * 100 + datetime.Month;
        }
    }
}
