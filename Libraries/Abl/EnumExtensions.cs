﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Abl
{
    public static class EnumExtensions
    {
        public static T GetValue<T>(this Enum o)
        {
            return GetValue<T>(o, null);
        }

        public static T GetValue<T>(this Enum o, string category)
        {
            Type type = o.GetType();
            FieldInfo fieldInfo = type.GetField(o.ToString());
            EnumValueAttribute[] attribs = fieldInfo.GetCustomAttributes(typeof(EnumValueAttribute), false) as EnumValueAttribute[];

            if (attribs.Count(a => a.Category == category) > 0)
                return (T)Convert.ChangeType(attribs.Where(a => a.Category == category).ElementAt(0).Value, typeof(T));
            else
                return default(T);
        }

        public static string GetName<T>(this Enum o, string category)
        {
            return Enum.GetName(typeof(T), category);
        }
        public static string GetName<T>(this Enum o, int category)
        {
            return Enum.GetName(typeof(T), category);
        }

        public static string GetDisplayName(this Enum enumValue)
        {
            return enumValue.GetType()
                            .GetMember(enumValue.ToString())
                            .First()
                            .GetCustomAttribute<DisplayAttribute>()
                            .GetName();
        }

        public static object ParseByValue(Type enumType, object value)
        {
            return ParseByValue(enumType, value, null);
        }

        public static object ParseByValue(Type enumType, object value, string category)
        {
            foreach (FieldInfo fieldInfo in enumType.GetFields())
            {
                EnumValueAttribute[] attribs = fieldInfo.GetCustomAttributes(typeof(EnumValueAttribute), false) as EnumValueAttribute[];

                if (attribs.Count(a => a.Category == category) > 0 && attribs.Where(a => a.Category == category).ElementAt(0).Value.ToString() == value.ToString())
                    return (Enum)fieldInfo.GetValue(null);
            }
            throw new ArgumentException(string.Format("\"{0}\"を{1}に変換できません", value, enumType));
        }

        public static Dictionary<int, string> ToDictionary(this Enum value)
        {
            Dictionary<int, string> dictReturn = new Dictionary<int, string>();
            foreach (var x in Enum.GetValues(value.GetType()))
            {
                dictReturn.Add((int)x, x.ToString());
            }
            return dictReturn;
        }

        public static string ToDescription(this Enum value)
        {
            if (value == null) return string.Empty;

            var attributes = (DescriptionAttribute[])value.GetType().GetField(
                Convert.ToString(value)).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : Convert.ToString(value);
        }

        public static T PickRandom<T>(this IEnumerable<T> source)
        {
            return source.PickRandom(1).Single();
        }

        public static IEnumerable<T> PickRandom<T>(this IEnumerable<T> source, int count)
        {
            return source.Shuffle().Take(count);
        }

        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
        {
            return source.OrderBy(x => Guid.NewGuid());
        }

        public static string GetDescription(this Enum enumValue)
        {
            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            return attributes.Length > 0 ? attributes[0].Description : enumValue.ToString();
        }
    }

    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true)]
    public class EnumValueAttribute : Attribute
    {
        public string Category { get; private set; }
        public object Value { get; private set; }

        public EnumValueAttribute(object val)
        {
            Value = val;
        }

        public EnumValueAttribute(string category, object val)
        {
            Category = category;
            Value = val;
        }
    }
}