﻿using System;

namespace Abl
{
    public static class StringExtensions
    {
        public static Guid ToGuid(this string source)
        {
            Guid result;
            if ((String.IsNullOrWhiteSpace(source)) ||
                (!Guid.TryParse(source, out result)))
            {
                return Guid.Empty;
            }

            return result;
        }

        /// <summary>
        /// yyyy-MM-dd
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static string ToSortableYmd(this DateTime datetime)
        {
            return datetime.ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// yyyy-MM-dd HH:mm
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static string ToSortableYmdHhMm(this DateTime datetime)
        {
            return datetime.ToString("yyyy-MM-dd HH:mm");
        }

        /// <summary>
        /// yyyy-MM-dd HH:mm:ss
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static string ToSortableYmdHhMmSs(this DateTime datetime)
        {
            return datetime.ToString("yyyy-MM-dd HH:mm:ss");
        }

        /// <summary>
        /// yyyyMM
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static string ToSortableYyyyMm(this DateTime datetime)
        {
            return datetime.ToString("yyyyMM");
        }

        /// <summary>
        /// yyyyMM
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static string ToSortableYyyyMmDd(this DateTime datetime)
        {
            return datetime.ToString("yyyyMMdd");
        }
    }
}
