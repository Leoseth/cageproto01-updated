﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Cage.DAL.Db.Models;
using Identity.Extensions;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace Identity
{
    // Configure the application sign-in manager which is used in this 
    // application.
    public class ApplicationSignInManager :
        SignInManager<ApplicationUser, Guid>
    {
        public ApplicationSignInManager(
            ApplicationUserManager userManager,
            IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(
            ApplicationUser user)
        {
            return
                user.GenerateUserIdentityAsync(
                    (ApplicationUserManager) UserManager);
        }

        public override Guid ConvertIdFromString(string id)
        {
            return string.IsNullOrEmpty(id) ? Guid.Empty : new Guid(id);
        }

        public override string ConvertIdToString(Guid id)
        {
            return id.Equals(Guid.Empty) ? string.Empty : id.ToString();
        }
    }
}