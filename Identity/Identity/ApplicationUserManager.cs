﻿using System;
using Cage.Models;
using Cage.DAL.Db.Models;
using Microsoft.AspNet.Identity;

using Repository.Pattern.Infrastructure;

namespace Identity
{
    public class ApplicationUserManager : UserManager<ApplicationUser, Guid>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser, Guid> store)
            : base(store)
        {            
    }
    }
}

