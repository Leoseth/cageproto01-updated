﻿using System;
using Cage.DAL.Db.Models;
using Microsoft.AspNet.Identity;

namespace Identity
{
    public class ApplicationRoleManager : RoleManager<ApplicationRole, Guid>
    {
        public ApplicationRoleManager(
            IRoleStore<ApplicationRole, Guid> roleStore)
            : base(roleStore)
        {
        }
    }
}