﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cage.Core;

namespace Identity.UI.ViewModels.Account
{
    public class RegisterViewModel:IValidatableObject
    {
        // These properties are used if the user initiates the registration
        // process from their oAuth Provider Account
        public string ProviderEmailAddress { get; set; }

        /*
        ** Application User
        */

        [Display(Name = "Email",
            ResourceType = typeof(Resources.AccountResources))]
        [Required(ErrorMessageResourceName = "Email_Required",
            ErrorMessageResourceType = typeof(Resources.AccountResources),
            ErrorMessage = null)
        ]
        [EmailAddress(ErrorMessageResourceName = "Email_Invalid",
            ErrorMessageResourceType = typeof(Resources.AccountResources),
            ErrorMessage = null)
        ]
        public string Email { get; set; }

        [Display(Name = "Password",
            ResourceType = typeof(Resources.AccountResources))]
        [Required(ErrorMessageResourceName = "Password_Required",
            ErrorMessageResourceType = typeof(Resources.AccountResources),
            ErrorMessage = null)
        ]
#if DEBUG
        [StringLength(100,
            ErrorMessageResourceName = "Password_Format",
            ErrorMessageResourceType = typeof(Resources.AccountResources),
            ErrorMessage = null,
            MinimumLength = 4)]
#else
        [StringLength(100,
            ErrorMessageResourceName = "Password_Format",
            ErrorMessageResourceType = typeof (Resources.AccountResources),
            ErrorMessage = null,
            MinimumLength = 8)]
#endif
        [DataType(DataType.Password,
            ErrorMessageResourceName = "Password_Format",
            ErrorMessageResourceType = typeof(Resources.AccountResources),
            ErrorMessage = null)
        ]
        public string Password { get; set; }

        [Display(Name = "ConfirmPassword",
            ResourceType = typeof(Resources.AccountResources))]
        [Compare("Password",
            ErrorMessageResourceName = "ConfirmPassword_Compare",
            ErrorMessageResourceType = typeof(Resources.AccountResources),
            ErrorMessage = null)
        ]
        [DataType(DataType.Password,
            ErrorMessageResourceName = "Password_Format",
            ErrorMessageResourceType = typeof(Resources.AccountResources),
            ErrorMessage = null)
        ]
        public string ConfirmPassword { get; set; }

        [Display(Name = "UserName",
            ResourceType = typeof(Resources.AccountResources))]
        [Required(ErrorMessageResourceName = "UserName_Required",
            ErrorMessageResourceType = typeof(Resources.AccountResources),
            ErrorMessage = null)
        ]
        public string UserName { get; set; }

        /*
        ** ApplicationUserDetails
        */

        [Display(Name = "Referrer",
            ResourceType = typeof(Resources.AccountResources))]
        //[Required(ErrorMessageResourceName = "Referrer_Required",
        //    ErrorMessageResourceType = typeof(Resources.AccountResources),
        //    ErrorMessage = null)
        //]
        public string Referrer { get; set; }

        public string ReferrerMode { get; set; }

        [Display(Name = "Consultant",
            ResourceType = typeof(Resources.AccountResources))]
        //[Required(ErrorMessageResourceName = "Referrer_Required",
        //    ErrorMessageResourceType = typeof(Resources.AccountResources),
        //    ErrorMessage = null)
        //]
        public string Consultant { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (ReferrerMode == Enum.GetName(typeof(CageCode.ReferrerMode), CageCode.ReferrerMode.Manual) &&
                string.IsNullOrWhiteSpace(Referrer))
            {
                yield return new ValidationResult(Resources.AccountResources.Referrer_Required);
            }
        }
    }
}