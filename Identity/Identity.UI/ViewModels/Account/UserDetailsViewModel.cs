﻿using System.ComponentModel.DataAnnotations;
using Abl;

namespace Identity.UI.ViewModels.Account
{
    public class UserDetailsViewModel
    {
        /*
        ** ApplicationUserDetails
        */

        [Display(Name = "FirstName",
            ResourceType = typeof (Resources.AccountResources))]
        //[Required(ErrorMessageResourceName = "FirstName_Required",
        //    ErrorMessageResourceType = typeof (Resources.AccountResources),
        //    ErrorMessage = null)
        //]
        [RegularExpression(RegexPatterns.PlainStringPattern,
            ErrorMessageResourceName = "PlainString_Format",
            ErrorMessageResourceType = typeof (Resources.AccountResources),
            ErrorMessage = null)
        ]
        public string FirstName { get; set; }

        [Display(Name = "LastName",
            ResourceType = typeof (Resources.AccountResources))]
        [RegularExpression(RegexPatterns.PlainStringPattern,
            ErrorMessageResourceName = "PlainString_Format",
            ErrorMessageResourceType = typeof (Resources.AccountResources),
            ErrorMessage = null)
        ]
        public string LastName { get; set; }

        [Display(Name = "Gender",
            ResourceType = typeof(Resources.AccountResources))]
        public bool? Gender { get; set; } = null;

        [Display(Name = "Address",
            ResourceType = typeof(Resources.AccountResources))]
        [RegularExpression(RegexPatterns.PlainStringPattern,
            ErrorMessageResourceName = "PlainString_Format",
            ErrorMessageResourceType = typeof(Resources.AccountResources),
            ErrorMessage = null)
        ]
        public string Address { get; set; }
    
        [Display(Name = "City",
            ResourceType = typeof(Resources.AccountResources))]
        [RegularExpression(RegexPatterns.PlainStringPattern,
            ErrorMessageResourceName = "PlainString_Format",
            ErrorMessageResourceType = typeof(Resources.AccountResources),
            ErrorMessage = null)
        ]
        public string City { get; set; }

        [Display(Name = "Zipcode",
            ResourceType = typeof(Resources.AccountResources))]
        [RegularExpression(RegexPatterns.PlainStringPattern,
            ErrorMessageResourceName = "PlainString_Format",
            ErrorMessageResourceType = typeof(Resources.AccountResources),
            ErrorMessage = null)
        ]
        public string Zipcode { get; set; }

        [Display(Name = "State",
            ResourceType = typeof(Resources.AccountResources))]
                [RegularExpression(RegexPatterns.PlainStringPattern,
            ErrorMessageResourceName = "PlainString_Format",
            ErrorMessageResourceType = typeof(Resources.AccountResources),
            ErrorMessage = null)
        ]
        public string State { get; set; }

        [Display(Name = "Country",
            ResourceType = typeof(Resources.AccountResources))]
        [RegularExpression(RegexPatterns.PlainStringPattern,
            ErrorMessageResourceName = "PlainString_Format",
            ErrorMessageResourceType = typeof(Resources.AccountResources),
            ErrorMessage = null)
        ]
        public string Country { get; set; }

        [Display(Name = "Phone",
            ResourceType = typeof (Resources.AccountResources))]
        [Phone(ErrorMessageResourceName = "PhoneNumber_Invalid",
            ErrorMessageResourceType = typeof (Resources.AccountResources),
            ErrorMessage = null)
        ]
        public string Phone { get; set; }

        [Display(Name = "Mobile",
            ResourceType = typeof(Resources.AccountResources))]
        [Phone(ErrorMessageResourceName = "MobileNumber_Invalid",
            ErrorMessageResourceType = typeof(Resources.AccountResources),
            ErrorMessage = null)
        ]
        public string Mobile { get; set; }
    }
}