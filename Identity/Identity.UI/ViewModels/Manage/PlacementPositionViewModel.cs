﻿namespace Identity.UI.ViewModels.Manage
{
    public class PlacementPositionViewModel
    {
        public string Username { get; set; }
        public string Position { get; set; }
        public string Mode { get; set; } 
    }
}
