﻿using System.Web;
using Cage.Core;
using Cage.Web.Core.Validater;

namespace Identity.UI.ViewModels.Manage
{
    public class AddAvatarViewModel
    {
        [ValidateFile(
            fileSizeMb:5, 
            ErrorMessageResourceType = typeof (WebResources), 
            ErrorMessageResourceName = "AddAvatarViewModel_File_Please_select_a_image_file_smaller_than_5MB")
        ]
        public HttpPostedFileBase File { get; set; }
    }
}