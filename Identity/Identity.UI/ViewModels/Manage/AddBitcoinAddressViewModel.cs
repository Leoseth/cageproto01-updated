﻿using System.ComponentModel.DataAnnotations;

namespace Identity.UI.ViewModels.Manage
{
    public class AddBitcoinAddressViewModel
    {
        [Display(Name = "BitcoinAddress",
            ResourceType = typeof (Resources.AccountResources))]
        [Required(ErrorMessageResourceName = "BitcoinAddress_Required",
            ErrorMessageResourceType = typeof (Resources.AccountResources),
            ErrorMessage = null)
        ]
        public string BitcoinAddress { get; set; }
    }
}