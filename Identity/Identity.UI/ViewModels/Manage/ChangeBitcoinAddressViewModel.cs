﻿using System.ComponentModel.DataAnnotations;

namespace Identity.UI.ViewModels.Manage
{
    public class ChangeBitcoinAddressViewModel
    {
        [Display(Name = "CurrentBitcoinAddress",
                    ResourceType = typeof(Resources.AccountResources))]
        public string OldBitcoinAddress { get; set; }

        [Display(Name = "NewBitcoinAddress",
            ResourceType = typeof(Resources.AccountResources))]
        [Required]
        public string NewBitcoinAddress { get; set; }
    }
}