﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class UserUnilevel : IObjectState
    {
        public Guid UserId { get; set; }
        public Guid? UplineUserId { get; set; }
        /// <summary>
        /// dummy
        /// </summary>
        public int? Level { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updateded { get; set; }

        [NotMapped]
        public ObjectState ObjectState { get; set; }

        // Navigation
        public virtual ApplicationUser User { get; set; }

        public UserUnilevel()
        {
            Created = Updateded = DateTime.UtcNow;
        }
    }
}
