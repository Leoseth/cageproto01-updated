﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class UserHelpPayment : IObjectState
    {
        public Guid UserPaymentQueueId { get; set; }
        public Guid UserGetHelpId { get; set; }
        public Guid UserProvideHelpId { get; set; }

        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        public UserHelpPayment()
        {
            Created = Created = Updated = DateTime.UtcNow;
        }

        [NotMapped]
        public ObjectState ObjectState { get; set; }

    }
}
