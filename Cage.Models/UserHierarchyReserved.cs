﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class UserHierarchyReserved : IObjectState
    {
        public Guid UserId { get; set; }
        public int Mode { get; set; }
        public int? Position { get; set; }
        public Guid? UplineUserId { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updateded { get; set; }

        [NotMapped]
        public ObjectState ObjectState { get; set; }

        // Navigation

        public UserHierarchyReserved()
        {
            Created = Updateded = DateTime.UtcNow;
        }
    }
}
