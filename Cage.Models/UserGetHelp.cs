﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class UserGetHelp : IObjectState
    {
        public Guid UserGetHelpId { get; set; }
        public Guid UserId { get; set; }
        public decimal Amount { get; set; }
        public decimal Filled { get; set; }
        public int Status { get; set; }
        public DateTime? Assigned { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        public decimal Remained => Amount - Filled;

        public UserGetHelp()
        {
            UserGetHelpId = Guid.NewGuid();
            Created = Created = Updated = DateTime.UtcNow;
        }

        [NotMapped]
        public ObjectState ObjectState { get; set; }

    }
}
