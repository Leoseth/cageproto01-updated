﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class PaymentMaster : IObjectState
    {
        public string Address { get; set; }
        public DateTime Expired { get; set; }
        [NotMapped]
        public ObjectState ObjectState { get; set; }
    }
}
