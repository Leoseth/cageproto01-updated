﻿using System;
using System.Collections.Generic;

namespace Cage.Models.ViewModels.Network
{
    public class HierarchyViewModel : LayoutMemberViewModel
    {
        public HierarchyUser FirstLineUser { get; set; }
        public IList<HierarchyUser> SecondLineUsers { get; set; }
        public IList<HierarchyUser> ThirdLineUsers { get; set; }
        public IList<HierarchyUser> ForthLineUsers { get; set; }
        public Guid FirstLineUserId { get; set; }
        public Guid UplineUserId { get; set; }
        public bool ShowDownLine { get; set; }
        public int HierarchyLevel { get; set; }

        public HierarchyViewModel(ApplicationUser firstLineUser, int hierarchyLv) : base(firstLineUser)
        {
            FirstLineUserId = firstLineUser.Id;
            ShowDownLine = false;
            HierarchyLevel = hierarchyLv;
            SecondLineUsers = new List<HierarchyUser>();
            ThirdLineUsers = new List<HierarchyUser>();
            ForthLineUsers = new List<HierarchyUser>();
        }
    }

    public class HierarchyUser
    {
        public Guid UserId { get; set; }
        public int? Position { get; set; }
        public string UserName { get; set; }
        public string ImageUrl { get; set; }
        public string ClassName { get; set; }
        public string ClassImageUrl { get; set; }
        public bool IdVerified { get; set; }
        public bool PhotoVerified { get; set; }

        public int HierarchyLevel { get; set; }
        public bool IsEmpty { get; set; }

        public HierarchyUser()
        {
        }

        public HierarchyUser(int position, bool isEmpty = false)
        {
            Position = position;
            IsEmpty = isEmpty;
        }
    }
}