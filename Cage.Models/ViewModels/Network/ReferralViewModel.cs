﻿using PagedList;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Cage.Models.ViewModels.Network
{
    public class ReferralViewModel : LayoutMemberViewModel
    {
        public UserReferral Me { get; set; }
        public IPagedList<UserReferral> UserReferrals { get; set; }

        [Display(Name = "Find by UserName")]
        public string SearchString { get; set; }

        public string SortOrder { get; set; }

        [Display(Name = "Select page size")]
        public int PageSize { get; set; }

        public SelectList PageSizeList { get; set; }

        public ReferralViewModel(ApplicationUser user) : base(user)
        {
            PageSizeList = new SelectList(new[] { 10, 20, 100 });
        }
    }
}