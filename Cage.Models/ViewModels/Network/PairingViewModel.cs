﻿using System.Collections.Generic;
using Cage.Core;

namespace Cage.Models.ViewModels.Network
{
    public class PairingViewModel
    {
        public IDictionary<CageCode.UserPosition, decimal> PairingValues { get; set; }

        public PairingViewModel()
        {
            PairingValues = new Dictionary<CageCode.UserPosition, decimal>();
        }
    }
}
