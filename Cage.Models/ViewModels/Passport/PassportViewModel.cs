﻿using System.Collections.Generic;
using Abl.Data;

namespace Cage.Models.ViewModels.Passport
{
    public class PassportViewModel : LayoutMemberViewModel
    {
        public int Balance { get; set; }
        public int Quantity { get; set; } = 1;
        public PassportHistoryViewModel PassportHistoryViewModel { get; set; }

        public PassportViewModel(ApplicationUser user) : base(user)
        {
            PassportHistoryViewModel = new PassportHistoryViewModel();
        }
    }

    public class PassportHistoryViewModel
    {
        public PagingInfo PagingInfo { get; set; }
        public IEnumerable<UserPassportHistory> UserPassportHistoryPagedList { get; set; }

        public PassportHistoryViewModel()
        {
            PagingInfo = new PagingInfo("Id", "Id", SortDirection.Descending)
            {
                PageNo = 1,
                PageSize = 10
            };
        }
    }
}
