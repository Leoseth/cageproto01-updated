﻿namespace Cage.Models.ViewModels
{
    public class LayoutMemberViewModel
    {
        public string AvatarImageUrl { get; set; }

        // TODO:Notificationsクラス追加

        public ApplicationUser User { get; private set; }

        public LayoutMemberViewModel(ApplicationUser user)
        {
            User = user;
            AvatarImageUrl = "/images/profiles/no_img.jpg";
        }
    }
}