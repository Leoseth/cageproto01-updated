﻿using System;
using System.Collections.Generic;
using System.Linq;
using Abl.Utils;

namespace Cage.Models.ViewModels.Member
{
    public class DashboardViewModel : LayoutMemberViewModel
    {
        // TODO:LayoutMemberViewModelのNotificationsの最新かつ、publishdateを見て取得する
        public Announcement Announce { get; set; }

        public DashboardAssistanceGivebackSummalyViewModel AssistanceGivebackSummaryView { get; set; }
        public DashboardUserShareViewModel UserShareViewModel { get; set; }

        public DashboardPhViewModel PhViewModel { get; set; }
        public DashboardPairingViewModel PairingViewModel { get; set; }
        public DashboardMembersSlotViewModel MembersSlotViewModel { get; set; }
        //TODO:GH summary
        //TODO:user status

        public ApplicationUserDetails UserDetails { get; set; }
        public ApplicationUser ReffererUser { get; set; }

        public DashboardViewModel(ApplicationUser user) : base(user)
        {
            if (user != null)
            {
                UserDetails = user.UserDetails;
            }
        }
    }

    public class DashboardAssistanceGivebackSummalyViewModel
    {
        public decimal SentAssistance { get; set; }
        public decimal ReceivedAssistance { get; set; }
        public decimal ReceivedGiveback { get; set; }
        public List<UserHierarchySummalyView> UserHierarchySummalyViews { get; set; }

        public int MemberCount => UserHierarchySummalyViews.Sum(x => x.MemberCount);
        public decimal TotalSent => SentAssistance;
        public decimal TotalReceive => ReceivedAssistance + ReceivedGiveback;
    }

    public class DashboardPhViewModel
    {
        public decimal TotalPh { get; set; }
        public decimal TotalDividend { get; set; }
    }

    public class DashboardGhViewModel
    {
        public decimal TotalGh { get; set; }
    }

    public class DashboardUserPaymentViewModel
    {
        
    }

    public class DashboardUserShareViewModel
    {
        public decimal PassportReferral { get; set; }
        public decimal PassportOveriding { get; set; }
        public decimal PhCapital { get; set; }
        public decimal PhDividend { get; set; }
        public decimal PhReferral { get; set; }
        public decimal PhOveriding { get; set; }
        public decimal FlexPairing { get; set; }
    }

    public class DashboardPairingViewModel
    {
        public decimal CurrentPairing { get; set; }
        public TimeSpan NextPairingDate => DateTimeUtil.GetFirstDayOfNextMonth(DateTime.UtcNow) - DateTime.UtcNow;

        public string NextPairingDateStr
        {
            get
            {
                var date = NextPairingDate;
                return $"{date.Days} days, {date.Hours} hours, {date.Minutes} minutes, {date.Seconds} seconds.";
            }
        }
    }

    public class DashboardMembersSlotViewModel
    {
        public ApplicationUser User { get; set; }
        public ApplicationUserDetails UserDetails { get; set; }
    }
}
