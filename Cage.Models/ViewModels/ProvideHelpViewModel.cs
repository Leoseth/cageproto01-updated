﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Abl;
using Cage.Core;

namespace Cage.Models.ViewModels
{
    public class ProvideHelpIndexViewModel
    { 
        public IList<ProvideHelpIndex> PledgePh { get; set; }
        public IList<ProvideHelpIndex> ActivePh { get; set; }
        public IList<ProvideHelpIndex> EndedPh { get; set; }

        public bool HasPassport { get; set; }
        public bool EnabledPh { get; set; }
        public PhModalViewModel PhModalViewModel { get; set; }
        public ProvideHelpIndexViewModel()
        {
            PledgePh = new List<ProvideHelpIndex>();
            ActivePh = new List<ProvideHelpIndex>();
            EndedPh = new List<ProvideHelpIndex>();
            PhModalViewModel = new PhModalViewModel();
        }
    }

    public class PhModalViewModel
    {
        public decimal AllowdProvideHelpValue { get; set; }
        public IEnumerable<SelectListItem> ProvideHelpSelectListBtc { get; set; }
        //public IEnumerable<SelectListItem> ProvideHelpSelectListShare { get; set; }

    }

    public class ProvideHelpIndex
    {
        public Guid UserProvideHelpId { get; set; }
        public DateTime Date { private get; set; }
        public decimal Ph { get; set; }
        public decimal Filled { get; set; }
        public int Day => (DateTime.UtcNow - Date).Days;
        public decimal InterestRates { get; set; }
        public decimal Dividend { get; set; }
        /// <summary>
        /// Sum of dividend value
        /// </summary>
        public decimal Released { get; set; }
        /// <summary>
        /// CageCode.ProvideHelpStatus
        /// </summary>
        public int Status { get; set; }
        public DateTime? Activated { get; set; }
        public int ActiveDays => Activated != null ? (DateTime.UtcNow - (DateTime) Activated).Days : 0;
        public int MaturityDays => CageCode.ProvideHelpMaturityDays - ActiveDays;

        public string DispDate => Date.ToLocalTime().ToSortableYmdHhMm();
        public string DispInterestRates => $"{InterestRates}%";
        public string DispStatus => ((CageCode.ProvideHelpStatus) Status).GetDescription();
        public string DispDivided { get; set; }
        public string DispDivindedAndCapital { get; set; }
    }

    public class ProvideHelpAssignedListModel
    {
        public Guid UserProvideHelpId { get; set; }
        public Guid UserGetHelpId { get; set; }
        public int No { get; set; }
        public DateTime AssignedDate { get; set; }
        public decimal Amount { get; set; }
        public int Status { get; set; }
        public string DispStatus => ((CageCode.HelpAssignedStatus)Status).GetDescription();
    }
    public class ProvideHelpReleasedListModel
    {
        public int No { get; set; }
        public DateTime ReleasedDate { get; set; }
        public int Type { get; set; }
        public decimal Amount { get; set; }

        public string DispType
            => ((CageCode.HelpAssignedStatus) Type).GetName<CageCode.HelpAssignedStatus>(Type);
    }

    public class PledgeNewViewModel
    {
        public decimal Amount { get; set; }
    }

    public class PledgeCancelViewModel
    {
        public string ProvideHelpId { get; set; }
    }

    public class ReleaseViewModel
    {
        public string ProvideHelpId { get; set; }
        public bool IsAll { get; set; }
    }
}
