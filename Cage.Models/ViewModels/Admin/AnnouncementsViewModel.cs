﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Cage.Models.ViewModels.Admin
{
    public class AnnouncementsViewModel
    {
        public int Id { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public DateTime PublishDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool DelFlg { get; set; }

        public DateTime PublishDateLocal => PublishDate.ToLocalTime();
        public DateTime EndDateLocal => EndDate.ToLocalTime();

        public AnnouncementsViewModel()
        {
            PublishDate = DateTime.Today.AddDays(1);
            EndDate = DateTime.Today.AddYears(1);
        }

        public AnnouncementsViewModel(Announcement announcement)
        {
            Id = announcement.Id;
            Subject = announcement.Subject;
            Message = announcement.Message;
            PublishDate = announcement.PublishDate;
            EndDate = announcement.EndDate;
            DelFlg = announcement.DelFlg;
        }
    }

    public class CreateAnnouncementsViewModel
    {
        [Required]
        public string Subject { get; set; }
        [Required]
        [DataType(DataType.MultilineText)]
        public string Message { get; set; }
        [Required]
        public DateTime PublishDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }

        public DateTime PublishDateUtc => PublishDate.ToUniversalTime();
        public DateTime EndDateUtc => EndDate.ToUniversalTime();

        public CreateAnnouncementsViewModel()
        {
            var now = DateTime.UtcNow.AddDays(1).Date;
            PublishDate = now;
            EndDate = now.AddYears(1);
        }

        public Announcement ConvertToEntity()
        {
            return new Announcement
            {
                    Subject = Subject,
                    Message = Message,
                    PublishDate = PublishDateUtc,
                    EndDate = EndDateUtc
            };
        }
    }

    public class EditAnnouncementsViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Subject { get; set; }
        [Required]
        [DataType(DataType.MultilineText)]
        public string Message { get; set; }
        [Required]
        public DateTime PublishDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        [Required]
        public bool DelFlg { get; set; }

        public DateTime PublishDateUtc => PublishDate.ToUniversalTime();
        public DateTime EndDateUtc => EndDate.ToUniversalTime();

        public EditAnnouncementsViewModel()
        {
        }

        public EditAnnouncementsViewModel(Announcement announcement)
        {
            Id = announcement.Id;
            Subject = announcement.Subject;
            Message = announcement.Message;
            PublishDate = announcement.PublishDate;
            EndDate = announcement.EndDate;
            DelFlg = announcement.DelFlg;
        }

        public Announcement ConvertToEntity()
        {
            return new Announcement
            {
                Id = Id,
                Subject = Subject,
                Message = Message,
                PublishDate = PublishDateUtc,
                EndDate = EndDateUtc,
                DelFlg = DelFlg
            };
        }
    }
}
