﻿using System;
using Cage.Core;

namespace Cage.Models.ViewModels.Payment
{
    public class PaymentViewModel
    {
        public int Category { get; set; }
        public string Address { get; set; }
        public decimal Amount { get; set; }
        public int TimeoutMin { get; } = CageSettings.PaymentTimeoutMin;
        public string PaymentTxRelativePath { get; set; }
        public int PaymentResult { get; set; }
        public Guid UserPaymentQueueId { get; set; }
    }
}