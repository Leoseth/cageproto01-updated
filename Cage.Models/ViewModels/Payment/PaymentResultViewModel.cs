﻿namespace Cage.Models.ViewModels.Payment
{
    public class PaymentResultViewModel
    {
        public string SenderAddress { get; set; }
        public string ReceiverAddress { get; set; }
        public string TransactionHash { get; set; }
        public decimal Amount { get; set; }
    }
}
