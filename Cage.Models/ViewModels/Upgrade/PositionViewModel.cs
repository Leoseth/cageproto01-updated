﻿using Cage.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace Cage.Models.ViewModels.Upgrade
{
    public class PositionViewModel : LayoutMemberViewModel
    {
        public string Username { get; set; }
        public int Mode { get; set; }
        public int Position { get; set; }

        [Display(Name = "Placement")]
        public IEnumerable<SelectListItem> ModeSelectList { get; set; }

        public IEnumerable<SelectListItem> PositionSelectList { get; set; }
        public bool HasPosition { get; set; }
        public bool HasPassport { get; set; }

        public PositionViewModel(ApplicationUser user) : base(user)
        {
            ModeSelectList =
                Enum.GetValues(typeof(CageCode.PlacementMode))
                    .Cast<CageCode.PlacementMode>()
                    .Select(x => new SelectListItem
                    {
                        Text = x.ToString(),
                        Value = ((int)x).ToString(),
                        Selected = x == CageCode.PlacementMode.Auto
                    }).ToList();

            PositionSelectList =
                Enum.GetValues(typeof(CageCode.UserPosition))
                    .Cast<CageCode.UserPosition>()
                    .Select(x => new SelectListItem
                    {
                        Text = x.ToString(),
                        Value = ((int)x).ToString(),
                        Disabled = true
                    }).ToList();

            HasPosition = false;
        }
    }

    public class UpgradeModalViewModel
    {
        public bool HasPassport { get; set; }
    }

    public class PlacementSlot
    {
        public bool Status { get; set; }
        public List<int> Slots { get; set; }
        public string ErrorMessage { get; set; }

        public PlacementSlot()
        {
            Slots = new List<int>();
        }
    }
}