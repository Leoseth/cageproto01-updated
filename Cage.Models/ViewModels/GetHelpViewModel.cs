﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Abl;
using Cage.Core;

namespace Cage.Models.ViewModels
{
    public class GetHelpIndexViewModel
    {
        public IList<GetHelpIndex> PendingGh { get; set; }
        public IList<GetHelpIndex> CompleatedGh { get; set; }

        public bool HasPassport { get; set; }
        public decimal AllowdGetHelpValue { get; set; }
        public decimal GetHelpSelectValue { get; set; }
        public IEnumerable<SelectListItem> GetHelpSelectList { get; set; }
        public GetHelpIndexViewModel()
        {
            PendingGh = new List<GetHelpIndex>();
            CompleatedGh = new List<GetHelpIndex>();
        }
    }

    public class GetHelpIndex
    {
        public Guid UserGetHelpId { get; set; }
        public DateTime Date { private get; set; }
        public decimal Gh { get; set; }
        public decimal Filled { get; set; }
        public int Status { get; set; }
        public int? Queue { get; set; }

        public string DispDate => Date.ToLocalTime().ToSortableYmdHhMm();
        public string DispStatus => ((CageCode.GetHelpStatus)Status).GetDescription();

    }

    public class GetHelpAssignedListModel
    {
        public Guid UserGetHelpId { get; set; }
        public Guid UserProvideHelpId { get; set; }
        public int No { get; set; }
        public DateTime AssignedDate { get; set; }
        public decimal Amount { get; set; }
        public int Status { get; set; }
        public string DispAssignedDate => AssignedDate.ToLocalTime().ToSortableYmdHhMm();
        public string DispStatus => ((CageCode.HelpAssignedStatus)Status).GetDescription();
    }
    public class GetHelpNewViewModel
    {
        public decimal Amount { get; set; }
    }

    public class GetHelpCancelViewModel
    {
        public string GetHelpId { get; set; }
    }
}
