﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abl;
using Cage.Core;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class UserReferral : IObjectState
    {
        [Key]
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public int UserType { get; set; }
        public int UserClass { get; set; }
        public Guid UplineUserId { get; set; }
        public string UplineUserName { get; set; }
        public int Position{ get; set; }
        public int Referrals { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }

        public int Level { get; set; }

        public string UserTypeDisp => ((CageCode.UserType)UserType).GetDescription();
        public string UserClassDisp => ((CageCode.UserClass)UserClass).GetDescription();
        public string PositionDisp => ((CageCode.UserPosition)Position).GetDescription();

        [NotMapped]
        public ObjectState ObjectState { get; set; }
    }
}
