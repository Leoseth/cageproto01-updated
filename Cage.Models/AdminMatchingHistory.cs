﻿using System;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class AdminMatchingHistory : IObjectState

    {
        public int Id { get; set; }
        public int PhTotalCount { get; set; }
        public decimal PhTotalAmount { get; set; }
        public int GhTotalCount { get;set; }
        public decimal GhTotalAmount { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        public AdminMatchingHistory()
        {
            Created = Created = Updated = DateTime.UtcNow;
        }

        public ObjectState ObjectState { get; set; }
    }
}
