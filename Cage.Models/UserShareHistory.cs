﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class UserShareHistory : IObjectState
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public int Category { get; set; }
        public decimal Amount { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        public UserShareHistory()
        {
            Created = Created = Updated = DateTime.UtcNow;
        }

        [NotMapped]

        public ObjectState ObjectState { get; set; }
    }
}
