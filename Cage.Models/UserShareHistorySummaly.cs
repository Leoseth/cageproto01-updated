using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class UserShareHistorySummaly : IObjectState
    {
        public Guid UserId { get; set; }
        public int Category { get; set; }
        public decimal Amount { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        [NotMapped]
        public ObjectState ObjectState { get; set; }

        // Navigation

        public UserShareHistorySummaly()
        {
        }
    }
}
