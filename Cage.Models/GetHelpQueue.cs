﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class GetHelpQueue : IObjectState
    {
        public int Id { get; set; }
        public Guid UserGetHelpId { get; set; }
        public Guid UserId { get; set; }
        /// <summary>
        /// Queueが有効かどうか、対象のUserGetHelp.Amountがアサインされると２日後に設定し2日間はバッチで処理しない。
        /// 期間中にすべて、または一部がキャンセルになった場合Validated後に再度マッチングする
        /// </summary>
        public DateTime Validated { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        public GetHelpQueue()
        {
            Created = Created = Updated = DateTime.UtcNow;
        }

        [NotMapped]
        public ObjectState ObjectState { get; set; }

    }
}
