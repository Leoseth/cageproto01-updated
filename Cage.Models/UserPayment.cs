﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class UserPayment : IObjectState
    {
        public int PaymentUserId { get; set; }
        public int PaymentCategory { get; set; }
        public int PaymentId { get; set; }
        public Guid UserId { get; set; }
        public decimal Amount { get; set; }
        public int Quantity { get; set; }
        public string PaymentAddress { get; set; }
        public bool Paid { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        public UserPayment()
        {
            Created = Updated = DateTime.UtcNow;
            Paid = false;
        }

        [NotMapped]
        public ObjectState ObjectState { get; set; }

    }
}
