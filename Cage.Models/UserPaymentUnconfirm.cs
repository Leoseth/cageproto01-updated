﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    /// <summary>
    /// Confirmations >= 6になるまでここ
    /// 6になったらUserPaymentHistoryに移す
    /// </summary>
    public class UserPaymentUnconfirm : IObjectState
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public int PaymentCategory { get; set; }
        public int PaymentUserId { get; set; }
        public int PaymentId { get; set; }
        public decimal Amount { get; set; }
        public int Quantity { get; set; }
        public string ReceiverAddress { get; set; }
        public string SenderAddress { get; set; }
        public string TransactionId { get; set; }
        public int Confirmations { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        // Navigation

        public UserPaymentUnconfirm()
        {
            Created = Updated = DateTime.UtcNow;
        }

        [NotMapped]
        public ObjectState ObjectState { get; set; }

    }
}
