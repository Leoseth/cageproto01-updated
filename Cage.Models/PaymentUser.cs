﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class PaymentUser :IObjectState
    {
        public Guid UserId { get; set; }
        public int PaymentUserId { get; set; }

        // Navigation
        public virtual ApplicationUser User { get; set; }

        // PaymentUserIdでFKが張れない(PaymentUser_UserIdカラムが自動生成されるのでコメント)
        //public virtual ICollection<UserPayment> UserPayments { get; set; }

        [NotMapped]
        public ObjectState ObjectState { get; set; }
    }
}
