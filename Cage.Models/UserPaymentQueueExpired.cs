﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    /// <summary>
    /// UserPaymentQueueでExpiredになったがReceiverAddressは有効なので一定期間監視する
    /// </summary>
    public class UserPaymentQueueExpired : IObjectState
    {
        public int Id { get; set; }
        public Guid UserPaymentQueueId { get; set; }
        public Guid UserId { get; set; }
        public int PaymenUsertId { get; set; }
        public int PaymentCategory { get; set; }
        public int PaymentId { get; set; }
        public decimal Amount { get; set; }
        public int Quantity { get; set; }
        public string ReceiverAddress { get; set; }
        public string TransactionId { get; set; }
        public DateTime Expired { get; set; }
        public DateTime Created { get; set; }

        public UserPaymentQueueExpired()
        {
            Created = DateTime.UtcNow;
        }

        [NotMapped]
        public ObjectState ObjectState { get; set; }
    }
}
