﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class UpgradePayment : IObjectState
    {
        public Guid UserPaymentQueueId { get; set; }
        public Guid UserId { get; set; }
        public int UpgradedUserClass { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        [NotMapped]
        public ObjectState ObjectState { get; set; }
    }
}