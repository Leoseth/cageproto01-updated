﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class ProvideHelpQueueHistory : IObjectState
    {
        public int Id { get; set; }
        public Guid UserProvideHelpId { get; set; }
        public Guid UserId { get; set; }
        public int ExpierdCount { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        [NotMapped]
        public ObjectState ObjectState { get; set; }

    }
}
