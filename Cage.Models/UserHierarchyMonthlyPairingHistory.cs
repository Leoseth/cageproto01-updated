using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class UserHierarchyMonthlyPairingHistory : IObjectState
    {
        public Guid UserId { get; set; }
        public DateTime Date { get; set; }
        public int Position { get; set; }
        public decimal Amount { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        [NotMapped]
        public ObjectState ObjectState { get; set; }

        // Navigation

        public UserHierarchyMonthlyPairingHistory()
        {
        }
    }
}
