using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class File : IObjectState
    {
        public int FileId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte[] Content { get; set; }
        public int FileType { get; set; }
        public Guid UserId { get; set; }

        public FileType FileTypeEnum => (FileType)FileType;

        [NotMapped]
        public ObjectState ObjectState { get; set; }

        // Navigation
        public virtual ApplicationUser ApplicationUser { get; set; }

        public File(HttpPostedFileBase file, FileType fileType)
        {
            FileName = System.IO.Path.GetFileName(file.FileName);
            FileType = (int) fileType;
            ContentType = file.ContentType;

            using (var reader = new System.IO.BinaryReader(file.InputStream))
            {
                Content = reader.ReadBytes(file.ContentLength);
            }
        }
    }

    public enum FileType
    {
        Avatar = 1,
        Photo = 2,
    }
}
