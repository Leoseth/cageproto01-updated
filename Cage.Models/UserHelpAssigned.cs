﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class UserHelpAssigned : IObjectState
    {
        public Guid UserGetHelpId { get; set; }
        public Guid UserProvideHelpId { get; set; }
        public Guid GhUserId { get; set; }
        public Guid PhUserId { get; set; }
        public int Status { get; set; }
        public decimal Amount { get; set; }
        public DateTime Expired { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        /// <summary>
        /// 画面側は-{n}時間で表示する。
        /// ※バッチ処理と時間差を設けて同時に同じデータを更新させない
        /// </summary>
        public DateTime DispExpired => Expired.AddHours(-1);

        public UserHelpAssigned()
        {
            Created = Created = Updated = DateTime.UtcNow;
        }

        [NotMapped]
        public ObjectState ObjectState { get; set; }

    }
}
