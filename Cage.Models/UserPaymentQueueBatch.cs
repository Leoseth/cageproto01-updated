﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    /// <summary>
    /// Batchでハンドリングされる支払い情報のキュー
    /// </summary>
    public class UserPaymentQueueBatch : IObjectState
    {
        public int Id { get; set; }
        public Guid UserPaymentQueueId { get; set; }
        public Guid UserId { get; set; }
        public int PaymentUserId { get; set; }
        public int PaymentCategory { get; set; }
        public int PaymentId { get; set; }
        public decimal Amount { get; set; }
        public int Quantity { get; set; }
        public string ReceiverAddress { get; set; }
        public string TransactionId { get; set; }
        public DateTime Expired { get; set; }
        public bool Paid { get; set; }

        [NotMapped]
        public ObjectState ObjectState { get; set; }
    }
}
