using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class AssistanceGivebackHistory : IObjectState
    {
        public int Id { get; set; }

        /// <summary>
        /// Payment From
        /// </summary>
        public Guid UserId { get; set; }
        public Guid PaymentToUserId { get; set; }
        /// <summary>
        /// Payment For
        /// </summary>
        public int PaymenCategory { get; set; }
        /// <summary>
        /// Payment Type
        /// </summary>
        public int PaymenType { get; set; }
        public string FromAddress { get; set; }
        public string ToAddress { get; set; }
        public decimal Amount { get; set; }
        public string Transaction { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        [NotMapped]
        public ObjectState ObjectState { get; set; }

        // Navigation

        public AssistanceGivebackHistory()
        {
        }
    }
}
