﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class GetHelpQueueHistory : IObjectState
    {
        public int Id { get; set; }
        public Guid UserGetHelpId { get; set; }
        public Guid UserId { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        [NotMapped]
        public ObjectState ObjectState { get; set; }

    }
}
