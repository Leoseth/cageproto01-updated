using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class UserProvideHelpMonthlyHistory : IObjectState
    {
        public Guid UserId { get; set; }
        /// <summary>
        /// yyyymm
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// new PH amount par Month
        /// </summary>
        public decimal Amount { get; set; }
        public  DateTime Created { get; set; }
        public  DateTime Updated { get; set; }

        [NotMapped]
        public ObjectState ObjectState { get; set; }

        // Navigation

        public UserProvideHelpMonthlyHistory()
        {
        }
    }
}
