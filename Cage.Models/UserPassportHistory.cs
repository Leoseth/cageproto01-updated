﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abl;
using Cage.Core;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class UserPassportHistory : IObjectState
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public int UseReason { get; set; }
        public int BeforQty { get; set; }
        public int Quantity { get; set; }
        public decimal Amount { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }


        public string DispDateTime => Created.ToLocalTime().ToSortableYmdHhMm();
        public string DispDesc => @Enum.GetName(typeof (CageCode.PassportTransactionCategory), UseReason);


        // Navigation
        public virtual ApplicationUser User { get; set; }

        public UserPassportHistory()
        {
            Created = Updated = DateTime.UtcNow;
        }

        [NotMapped]
        public ObjectState ObjectState { get; set; }

    }
}
