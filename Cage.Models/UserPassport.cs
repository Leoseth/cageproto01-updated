﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class UserPassport : IObjectState
    {
        [Key]
        public Guid UserId { get; set; }
        public int Amount { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        public bool HasPassport => Amount > 0;

        [NotMapped]
        public ObjectState ObjectState { get; set; }

        // Navigation
        public virtual ApplicationUser User { get; set; }
        public virtual ICollection<UserPassportHistory> UserPassportHistories { get; set; }

        public UserPassport()
        {
            Created = Updated = DateTime.UtcNow;
        }
    }
}
