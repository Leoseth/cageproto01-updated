using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class UserHierarchySummaly_1 : IObjectState
    {
        public Guid UserId { get; set; }
        public int DateYm { get; set; }
        public int Count { get; set; }

        [NotMapped]
        public ObjectState ObjectState { get; set; }

        // Navigation

        public UserHierarchySummaly_1()
        {
        }
    }
}
