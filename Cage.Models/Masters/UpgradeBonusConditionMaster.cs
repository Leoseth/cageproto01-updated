﻿using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models.Masters
{
    public class UpgradeBonusConditionMaster:IObjectState
    {
        public int UserClass { get; set; }
        public int AssistanceNeedLevel { get; set; }
        public int GivebackNeedLevel { get; set; }
        [NotMapped]
        public ObjectState ObjectState { get; set; }
    }
}
