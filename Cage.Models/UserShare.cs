﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models
{
    public class UserShare : IObjectState
    {
        public Guid UserId { get; set; }
        public decimal Dividend { get; set; }
        public decimal Released { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        public UserShare()
        {
            Created = Created = Updated = DateTime.UtcNow;
        }

        // Navigation
        public virtual ApplicationUser User { get; set; }

        [NotMapped]

        public ObjectState ObjectState { get; set; }
    }
}
