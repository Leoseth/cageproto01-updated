﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Db.Configurations
{
    public class UserHierarchyMap
        : EntityTypeConfiguration<UserHierarchy>
    {
        public UserHierarchyMap()
        {
            ToTable("UserHierarchies");

            // Primary Key
            HasKey(t => new { t.UserId, t.Position });

            // Properties
            Property(u => u.UserId).IsRequired().HasColumnName("UserId");
            Property(u => u.Position).IsRequired().HasColumnName("Position");
            Property(u => u.DownlineUserId).IsRequired().HasColumnName("DownlineUserId")
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_UserHierarchies_01")
                        {
                            IsUnique = true,
                            IsClustered = false,
                            Order = 1
                        }));
            Property(u => u.Level).IsOptional().HasColumnName("Level");
            Property(u => u.Created).IsRequired().HasColumnName("Created");
            Property(u => u.Updated).IsRequired().HasColumnName("Updated");
        }
    }
}