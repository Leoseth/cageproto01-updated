using System.Data.Entity.ModelConfiguration;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Db.Configurations
{
    public class UserHierarchySummalyViewMap
        : EntityTypeConfiguration<UserHierarchySummalyView>
    {
        public UserHierarchySummalyViewMap()
        {
            ToTable("UserHierarchySummalyViews");

            // Primary Key
            HasKey(r => new {r.UserId, r.DateYm});

            Property(r => r.UserId).IsRequired().HasColumnName("UserId");
            Property(r => r.DateYm).IsRequired().HasColumnName("DateYm");
            Property(r => r.MemberCount).IsRequired().HasColumnName("MemberCount");

            // Navigation
        }
    }
}