




using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Db.Configurations
{
    public class UserUnilevelMap
        : EntityTypeConfiguration<UserUnilevel>
    {
        public UserUnilevelMap()
        {
            ToTable("UserUnilevels");

            // Primary Key
            HasKey(r => r.Id);

            Property(r => r.Id).IsRequired().HasColumnName("Id");
            // added by Leonard on July 06, 2017
            Property(r => r.UplineUserId).IsRequired().HasColumnName("UplineUserId");            
            //Property(r => r.User).IsRequired().HasColumnName("User");
            Property(r => r.UserId).IsRequired().HasColumnName("UserId");
            // added by Leonard on July 06, 2017
            // Navigation
        }
    }
}