﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Db.Configurations
{
    public class UserHelpAssignedMap
        : EntityTypeConfiguration<UserHelpAssigned>
    {
        public UserHelpAssignedMap()
        {
            ToTable("UserHelpAssigneds");

            // Primary Key
            HasKey(t => new { t.UserGetHelpId, t.UserProvideHelpId });

            Property(t => t.UserGetHelpId).IsRequired().HasColumnName("UserGetHelpId");
            Property(t => t.UserProvideHelpId).IsRequired().HasColumnName("UserProvideHelpId");
            Property(t => t.GhUserId).IsRequired().HasColumnName("GhUserId");
            Property(t => t.PhUserId).IsRequired().HasColumnName("PhUserId");
            Property(t => t.Status).IsRequired().HasColumnName("Status")
                .HasColumnAnnotation(
                     "Index",
                     new IndexAnnotation(
                         new IndexAttribute("IX_UserHelpAssigneds_02")
                         {
                             IsUnique = false,
                             IsClustered = false,
                             Order = 1
                         }));
            Property(t => t.Amount).IsRequired().HasPrecision(16, 8).HasColumnName("Amount");
            Property(u => u.Expired).IsRequired().HasColumnName("Expired")
                .HasColumnAnnotation(
                     "Index",
                     new IndexAnnotation(
                         new IndexAttribute("IX_UserHelpAssigneds_01")
                         {
                             IsUnique = false,
                             IsClustered = false,
                             Order = 1
                         }));
            Property(u => u.Created).IsRequired().HasColumnName("Created");
            Property(u => u.Updated).IsRequired().HasColumnName("Updated");

            // Relationships

        }
    }
}