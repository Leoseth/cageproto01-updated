using Cage.DAL.Db.Models;
using System.Data.Entity.ModelConfiguration;

namespace Cage.DAL.Db.Configurations
{
    public class ApplicationUserDetailMap
        : EntityTypeConfiguration<ApplicationUserDetails>
    {
        public ApplicationUserDetailMap()
        {
            ToTable("AspNetUserDetails");

            // Primary Key
            HasKey(d => d.UserId);

            // Primary Key
            HasKey(r => r.UserId);

            Property(r => r.UserId).IsRequired().HasColumnName("UserId");
            // Properties
            Property(d => d.FirstName)
                .IsOptional()
                .HasMaxLength(100)
                .HasColumnName("FirstName");
            Property(d => d.LastName)
                .IsOptional()
                .HasMaxLength(100)
                .HasColumnName("LastName");
            Property(d => d.Gender)
                .IsOptional()
                .HasColumnName("Gender");
            Property(d => d.Address)
                .IsOptional()
                .HasMaxLength(100)
                .HasColumnName("Address");
            Property(d => d.City)
                .IsOptional()
                .HasMaxLength(100)
                .HasColumnName("City");
            Property(d => d.Zipcode)
                .IsOptional()
                .HasMaxLength(100)
                .HasColumnName("Zipcode");
            Property(d => d.State)
                .IsOptional()
                .HasMaxLength(100)
                .HasColumnName("State");
            Property(d => d.Country)
                .IsOptional()
                .HasMaxLength(10)
                .HasColumnName("Country");
            Property(d => d.Phone)
                .IsOptional()
                .HasMaxLength(50)
                .HasColumnName("Phone");
            Property(d => d.Mobile)
                .IsOptional()
                .HasMaxLength(50)
                .HasColumnName("Mobile");
            Property(d => d.ReferrerUserId)
                .IsRequired()
                .HasColumnName("ReferrerUserId");
            Property(d => d.ConsultantUserId)
                .IsRequired()
                .HasColumnName("ConsultantUserId");

            Property(d => d.UserType)
                .IsRequired()
                .HasColumnName("UserType");
            Property(d => d.UserClass)
                .IsRequired()
                .HasColumnName("UserClass");

            Property(u => u.BitcoinAddress)
                .IsOptional()
                .HasMaxLength(35)
                .HasColumnName("BitcoinAddress");

            // Navigation
        }
    }
}