﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Db.Configurations
{
    public class ProvideHelpQueueHistoryMap : EntityTypeConfiguration<ProvideHelpQueueHistory>
    {
        public ProvideHelpQueueHistoryMap()
        {
            ToTable("ProvideHelpQueueHistories");

            // Primary Key
            HasKey(up => up.Id);

            // Properties
            Property(t => t.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None).HasColumnName("Id");
            Property(u => u.UserProvideHelpId).IsRequired().HasColumnName("UserProvideHelpId");
            Property(u => u.UserId).IsRequired().HasColumnName("UserId");
            Property(u => u.ExpierdCount).IsRequired().HasColumnName("ExpierdCount");
            Property(u => u.Created).IsRequired().HasColumnName("Created");
            Property(u => u.Updated).IsRequired().HasColumnName("Updated");

            // Relationships

        }
    }
}
