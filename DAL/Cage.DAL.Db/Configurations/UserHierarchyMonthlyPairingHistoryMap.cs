using System.Data.Entity.ModelConfiguration;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Db.Configurations
{
    public class UserHierarchyMonthlyPairingHistoryMap
        : EntityTypeConfiguration<UserHierarchyMonthlyPairingHistory>
    {
        public UserHierarchyMonthlyPairingHistoryMap()
        {
            ToTable("UserHierarchyMonthlyPairingHistories");

            // Primary Key
            HasKey(r => new { r.UserId , r.Date});

            Property(r => r.UserId).IsRequired().HasColumnName("UserId");
            Property(r => r.Date).IsRequired().HasColumnName("Date");
            Property(r => r.Position).IsRequired().HasColumnName("Position");
            Property(r => r.Amount).IsRequired().HasColumnName("Amount");
            Property(r => r.Created).IsRequired().HasColumnName("Created");
            Property(r => r.Updated).IsRequired().HasColumnName("Updated");

            // Navigation
        }
    }
}