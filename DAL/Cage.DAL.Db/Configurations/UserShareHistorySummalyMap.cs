using System.Data.Entity.ModelConfiguration;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Db.Configurations
{
    public class UserShareHistorySummalyMap
        : EntityTypeConfiguration<UserShareHistorySummaly>
    {
        public UserShareHistorySummalyMap()
        {
            ToTable("UserShareHistorySummalies");

            // Primary Key
            HasKey(r => new { r.UserId, r.Category });

            Property(r => r.UserId).IsRequired().HasColumnName("UserId");
            Property(r => r.Category).IsRequired().HasColumnName("Category");
            Property(u => u.Amount).IsRequired().HasPrecision(16, 8).HasColumnName("Amount");
            Property(u => u.Created).IsRequired().HasColumnName("Created");
            Property(u => u.Updated).IsRequired().HasColumnName("Updated");

            // Navigation
        }
    }
}