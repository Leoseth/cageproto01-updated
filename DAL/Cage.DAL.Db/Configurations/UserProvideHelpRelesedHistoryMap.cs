﻿using System.Data.Entity.ModelConfiguration;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Db.Configurations
{
    public class UserProvideHelpRelesedHistoryMap
        : EntityTypeConfiguration<UserProvideHelpRelesedHistory>
    {
        public UserProvideHelpRelesedHistoryMap()
        {
            ToTable("UserProvideHelpRelesedHistories");

            // Primary Key
            HasKey(t => new { t.UserProvideHelpId, t.Number });

            Property(t => t.UserProvideHelpId).IsRequired().HasColumnName("UserProvideHelpId");
            Property(t => t.Number).IsRequired().HasColumnName("Number");
            Property(t => t.UserId).IsRequired().HasColumnName("UserId");
            Property(t => t.Type).IsRequired().HasColumnName("Type");
            Property(t => t.Amount).IsRequired().HasPrecision(16, 8).HasColumnName("Amount");
            Property(u => u.Created).IsRequired().HasColumnName("Created");
            Property(u => u.Updated).IsRequired().HasColumnName("Updated");

            // Relationships

        }
    }
}