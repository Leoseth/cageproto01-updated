using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Db.Configurations
{
    public class FileMap
        : EntityTypeConfiguration<File>
    {
        public FileMap()
        {
            ToTable("Files");

            // Primary Key
            HasKey(r => r.FileId);

            Property(r => r.FileName).IsRequired().HasMaxLength(255).HasColumnName("FileName");
            Property(r => r.ContentType).IsRequired().HasMaxLength(100).HasColumnName("ContentType")
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_Files_01")
                        {
                            IsUnique = false,
                            IsClustered = false,
                            Order = 2
                        }));
            Property(r => r.Content).IsRequired().HasColumnName("Content");
            Property(r => r.FileType).IsRequired().HasColumnName("FileType");
            Property(r => r.UserId).IsRequired().HasColumnName("UserId")
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_Files_01")
                        {
                            IsUnique = false,
                            IsClustered = false,
                            Order = 1
                        }));

            // Navigation
        }
    }
}