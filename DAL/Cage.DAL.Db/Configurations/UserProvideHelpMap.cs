﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Db.Configurations
{
    public class UserProvideHelpMap
        : EntityTypeConfiguration<UserProvideHelp>
    {
        public UserProvideHelpMap()
        {
            ToTable("UserProvideHelps");

            // Primary Key
            HasKey(t => t.UserProvideHelpId);

            Property(t => t.UserProvideHelpId).IsRequired().HasColumnName("UserProvideHelpId");
            Property(t => t.UserId)
                .IsRequired()
                .HasColumnName("UserId")
                .HasColumnAnnotation(
                     "Index",
                     new IndexAnnotation(
                         new IndexAttribute("IX_UserProvideHelps_01")
                         {
                             IsUnique = false,
                             IsClustered = false,
                             Order = 1
                         }));
            Property(t => t.Amount).IsRequired().HasPrecision(16, 8).HasColumnName("Amount");
            Property(t => t.Filled).IsRequired().HasPrecision(16, 8).HasColumnName("Filled");
            Property(t => t.InterestRate).IsRequired().HasColumnName("InterestRate");
            Property(t => t.Status)
                .IsRequired()
                .HasColumnName("Status")
                .HasColumnAnnotation(
                     "Index",
                     new IndexAnnotation(
                         new IndexAttribute("IX_UserProvideHelps_01")
                         {
                             IsUnique = false,
                             IsClustered = false,
                             Order = 2
                         }));
            
            Property(t => t.Released).IsRequired().HasColumnName("Released");
            Property(t => t.Activated).IsOptional().HasColumnName("Activated");
            Property(u => u.Created).IsRequired().HasColumnName("Created");
            Property(u => u.Updated).IsRequired().HasColumnName("Updated");

            // Relationships
            HasMany(u => u.UserProvideHelpAssignedHistories)
                .WithRequired()
                .HasForeignKey(ur => ur.UserProvideHelpId)
                .WillCascadeOnDelete();

            HasMany(u => u.UserProvideHelpRelesedHistories)
                .WithRequired()
                .HasForeignKey(ur => ur.UserProvideHelpId)
                .WillCascadeOnDelete();

            //TODO:test
            //MapToStoredProcedures();
        }
    }
}