using System.Data.Entity.ModelConfiguration;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Db.Configurations
{
    public class UserHierarchyMonthlyPairingMap
        : EntityTypeConfiguration<UserHierarchyMonthlyPairing>
    {
        public UserHierarchyMonthlyPairingMap()
        {
            ToTable("UserHierarchyMonthlyPairings");

            // Primary Key
            HasKey(r => new {r.UserId, r.Date});

            Property(r => r.UserId).IsRequired().HasColumnName("UserId");
            Property(r => r.Date).IsRequired().HasColumnName("Date");
            Property(r => r.Amount).IsRequired().HasColumnName("Amount");

            // Navigation
        }
    }
}