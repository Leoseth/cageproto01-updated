using System.Data.Entity.ModelConfiguration;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Db.Configurations
{
    public class UserPaymentSummalyMap
        : EntityTypeConfiguration<UserPaymentSummaly>
    {
        public UserPaymentSummalyMap()
        {
            ToTable("UserPaymentSummalies");

            // Primary Key
            HasKey(r => new {r.UserId, r.DateYm, r.Category});

            Property(r => r.UserId).IsRequired().HasColumnName("UserId");
            Property(r => r.DateYm).IsRequired().HasColumnName("DateYm");
            Property(r => r.Category).IsRequired().HasColumnName("Category");
            Property(r => r.Amount).IsRequired().HasPrecision(16, 8).HasColumnName("Amount");

            // Navigation
        }
    }
}