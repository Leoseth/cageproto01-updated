using System.Data.Entity.ModelConfiguration;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Db.Configurations
{
    public class UserProvideHelpMonthlyHistoryMap
        : EntityTypeConfiguration<UserProvideHelpMonthlyHistory>
    {
        public UserProvideHelpMonthlyHistoryMap()
        {
            ToTable("UserProvideHelpMonthlyHistories");

            // Primary Key
            HasKey(r => new { r.UserId, r.Date});

            Property(r => r.UserId).IsRequired().HasColumnName("UserId");
            Property(r => r.Date).IsRequired().HasColumnName("Date");
            Property(r => r.Amount).HasPrecision(16, 8).IsRequired().HasColumnName("Amount");
            Property(r => r.Created).IsRequired().HasColumnName("Created");
            Property(r => r.Updated).IsRequired().HasColumnName("Updated");

            // Navigation
        }
    }
}