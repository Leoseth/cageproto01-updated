﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Db.Configurations
{
    public class ProvideHelpQueueMap : EntityTypeConfiguration<ProvideHelpQueue>
    {
        public ProvideHelpQueueMap()
        {
            ToTable("ProvideHelpQueues");

            // Primary Key
            HasKey(up => up.Id);

            // Properties
            Property(t => t.Id)
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .HasColumnName("Id");
            Property(u => u.UserProvideHelpId)
                .IsRequired()
                .HasColumnName("UserProvideHelpId")
                .HasColumnAnnotation(
                     "Index",
                     new IndexAnnotation(
                         new IndexAttribute("IX_ProvideHelpQueues_01")
                         {
                             IsUnique = false,
                             IsClustered = false,
                             Order = 1
                         }));
            Property(u => u.UserId)
                .IsRequired()
                .HasColumnName("UserId")
                .HasColumnAnnotation(
                     "Index",
                     new IndexAnnotation(
                         new IndexAttribute("IX_ProvideHelpQueues_02")
                         {
                             IsUnique = false,
                             IsClustered = false,
                             Order = 1
                         }));
            Property(u => u.Validated)
                .IsRequired().
                HasColumnName("Validated")
                .HasColumnAnnotation(
                     "Index",
                     new IndexAnnotation(
                         new IndexAttribute("IX_ProvideHelpQueues_03")
                         {
                             IsUnique = false,
                             IsClustered = false,
                             Order = 1
                         }));
            Property(u => u.ExpierdCount).IsRequired().HasColumnName("ExpierdCount");
            Property(u => u.Created).IsRequired().HasColumnName("Created");
            Property(u => u.Updated).IsRequired().HasColumnName("Updated");

            // Relationships

        }
    }
}
