using System.Data.Entity.ModelConfiguration;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Db.Configurations
{
    public class ViewControlMap
        : EntityTypeConfiguration<ViewControl>
    {
        public ViewControlMap()
        {
            ToTable("ViewControls");

            // Primary Key
            HasKey(r => r.ViewName);

            Property(r => r.ViewName).IsRequired().HasMaxLength(30).HasColumnName("ViewName");
            Property(r => r.Active).IsRequired().HasColumnName("Active");
            Property(r => r.Updated).IsRequired().HasColumnName("Updated");

            // Navigation
        }
    }
}