﻿using Cage.Core;
using Cage.DAL.Db.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Cage.DAL.Db
{
    public class CageSeedData : DropCreateDatabaseIfModelChanges<CageDataContext>
    {
        private CageDataContext Context { get; set; }

        protected override void Seed(CageDataContext context)
        {
            Context = context;
            CreateProcedure();
            CreateView();
            CreateRole();
            var user = CreateRootUser();
        }

        private void InsertTransactionData()
        {
        }

        private ApplicationUser CreateRootUser()
        {
            var user = new ApplicationUser
            {
                UserName = CageSettings.RootUser,
                Email = CageSettings.RootUserEmail,
                Created = DateTime.UtcNow,
                EmailConfirmed = true,
                PhoneNumberConfirmed = true,
                PasswordHash = CageSettings.RootUserPasswordHash,
                SecurityStamp = Guid.NewGuid().ToString("D"),
                LastLogin = DateTime.UtcNow,
                UserDetails = new ApplicationUserDetails
                {
                    ReferrerUserId = Guid.NewGuid(),
                    UserType = (int)CageCode.UserType.Member,
                    UserClass = (int)CageCode.UserClass.Grade_00,
                    BitcoinAddress = CageSettings.RootUsersBitcoinAddress,
                }
            };

            user.UserUnilevel = new UserUnilevel
            {
                //UserId = user.Id,
                // modified by Leonard //
                UserId = (user.Id.ToString()),
                // modified by Leonard //
                //UplineUserId = null
                UplineUserId = ""
            };

            Context.ApplicationUsers.Add(user);

            foreach (var source in Context.ApplicationRoles.ToList())
            {
                Context.ApplicationUserRoles.Add(new ApplicationUserRole
                {
                    UserId = user.Id,
                    RoleId = source.Id
                });
            }

            Context.SaveChanges();

            return user;
        }

        private void CreateRole()
        {
            var roles = Enum.GetNames(typeof(CageCode.RolesEnum));
            roles.ToList().ForEach(r => Context.ApplicationRoles.Add(new ApplicationRole(r)));
            Context.SaveChanges();
        }

        private void ExecuteSql(List<string> sqls)
        {
            sqls.ForEach(sql =>
            {
                Context.Database.ExecuteSqlCommand(sql);
            });
            Context.SaveChanges();
        }

        private void CreateView()
        {
            var sqls = new List<string>
            {
            };

            if (sqls.Any())
                ExecuteSql(sqls);
        }

        private void CreateProcedure()
        {
            var sqls = new List<string>
            {
                @"CREATE PROCEDURE UpdateUserPassportAmount
	@UserId [uniqueidentifier],
    @Quantity [int]
AS
BEGIN
    UPDATE [dbo].[UserPassports]
    SET [Amount] = [Amount] + @Quantity, [Updated] = GETDATE()
    WHERE ([UserId] = @UserId)
END",
                @"CREATE PROCEDURE [dbo].[UpdateUserGetHelpsFilled]
	@UserGetHelpId [uniqueidentifier],
	@UserProvideHelpId [uniqueidentifier],
    @Amount decimal(16, 8)
AS
BEGIN
    UPDATE [dbo].[UserGetHelps]
    SET 
		[Filled] = [Filled] + @Amount, 
		[Updated] = GETDATE()
    WHERE 
		[UserGetHelpId] = @UserGetHelpId
END",
                @"CREATE PROCEDURE [dbo].[UpdateUserShareDividend]
	@UserId [uniqueidentifier],
    @Amount decimal(16, 8)
AS
BEGIN
    UPDATE [dbo].[UserShares]
    SET 
		[Dividend] = [Dividend] + @Amount, 
		[Updated] = GETDATE()
    WHERE 
		[UserId] = @UserId
END",
                @"CREATE PROCEDURE [dbo].[UpdateUserShareReleased]
	@UserId [uniqueidentifier],
    @Amount decimal(16, 8)
AS
BEGIN
    UPDATE [dbo].[UserShares]
    SET 
		[Released] = [Released] + @Amount, 
		[Updated] = GETDATE()
    WHERE 
		[UserId] = @UserId
END",
                @"CREATE PROCEDURE [dbo].[UpdateUserProvideHelpsRelease]
	@UserId [uniqueidentifier],
    @Amount decimal(16, 8),
	@Status [int],
	@Activated [datetime]
AS
BEGIN
    UPDATE [dbo].[UserProvideHelps]
    SET 
		[Released] = [Released] + @Amount, 
		[Status] = @Status,
		[Activated] = @Activated,
		[Updated] = GETDATE()
    WHERE 
		[UserId] = @UserId
END",
                @"CREATE PROCEDURE [dbo].[UpdateUserDetailsIncrementReferrals]
	@UserId [uniqueidentifier]
AS
BEGIN
    UPDATE [dbo].[AspNetUserDetails]
    SET 
		[Referrals] = [Referrals] + 1
    WHERE 
		[UserId] = @UserId
END",
                @"CREATE PROCEDURE [dbo].[UpsertUserProvideHelpMonthlyHistoriesAmount]
	@UserId uniqueidentifier,
	@Date datetime,
    @Amount decimal(16, 8)
AS
BEGIN
    UPDATE [dbo].[UserProvideHelpMonthlyHistories]
    SET 
		[Amount] = [Amount] + @Amount, 
		[Updated] = GETDATE()
    WHERE 
		[UserId] = @UserId AND
		Date = @Date
	if @@ROWCOUNT = 0
	BEGIN
		INSERT UserProvideHelpMonthlyHistories VALUES(@UserId, @Date, @Amount, GETDATE(), GETDATE())
	END
END",
                @"CREATE PROCEDURE [dbo].[UpsertUserPaymentSummalies]
	@UserId uniqueidentifier,
	@Category int,
    @Amount decimal(16, 8)
AS
BEGIN
	DECLARE @Date int = CONVERT(nvarchar(6), GETDATE(), 112)

    UPDATE [dbo].[UserPaymentSummalies]
    SET 
		[Amount] = [Amount] + @Amount, 
		[Updated] = GETDATE()
    WHERE 
		[UserId] = @UserId AND
		DateYm = @Date AND
		Category = @Category

	IF @@ROWCOUNT = 0
	BEGIN
		DECLARE @TotalAmount decimal(16, 8)
		SELECT 
			@TotalAmount = MAX(ISNULL(amount, 0)) 
		FROM 
			UserPaymentSummalies 
		WHERE 
			[UserId] = @UserId AND 
			Category = @Category

		INSERT 
			UserPaymentSummalies 
		VALUES 
			(@UserId, @Date, @Category, ISNULL(@TotalAmount, 0) + @Amount, GETDATE(), GETDATE())
	END
END",
                @"CREATE PROCEDURE [dbo].[UpsertUserShareHistorySummalies]
	@UserId uniqueidentifier,
	@Category int,
    @Amount decimal(16, 8)
AS
BEGIN
	DECLARE @Date int = CONVERT(nvarchar(6), GETDATE(), 112)

    UPDATE UserShareHistorySummalies
    SET 
		[Amount] = [Amount] + @Amount, 
		[Updated] = GETDATE()
    WHERE 
		[UserId] = @UserId AND
		Category = @Category

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT 
			UserShareHistorySummalies 
		VALUES 
			(@UserId, @Category, @Amount, GETDATE(), GETDATE())
	END
END"
            };

            if (sqls.Any())
                ExecuteSql(sqls);
        }
    }
}