﻿namespace Cage.DAL.Db
{
    public interface IDataContextAsync
        : Repository.Pattern.DataContext.IDataContextAsync
    {
    }
}