using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.DAL.Db.Models
{
    public class UserHierarchyMonthlyPairing : IObjectState
    {
        public Guid UserId { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }

        [NotMapped]
        public ObjectState ObjectState { get; set; }

        // Navigation

        public UserHierarchyMonthlyPairing()
        {
        }
    }
}
