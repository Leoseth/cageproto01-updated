using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using Repository.Pattern.Infrastructure;

namespace Cage.DAL.Db.Models
{
    public class File : IObjectState
    {
        public int FileId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte[] Content { get; set; }
        public int FileType { get; set; }
        public Guid UserId { get; set; }

        public FileType FileTypeEnum => (FileType)FileType;

        [NotMapped]
        public ObjectState ObjectState { get; set; }

        // Navigation
        public virtual ApplicationUser ApplicationUser { get; set; }

        public File(string fileName, string contentType, Stream inputStream, int contentLength, FileType fileType)
        {
            FileName = System.IO.Path.GetFileName(fileName);
            FileType = (int) fileType;
            ContentType = contentType;

            using (var reader = new System.IO.BinaryReader(inputStream))
            {
                Content = reader.ReadBytes(contentLength);
            }
        }
    }

    public enum FileType
    {
        Avatar = 1,
        Photo = 2,
    }
}
