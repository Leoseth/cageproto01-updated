﻿using Microsoft.AspNet.Identity.EntityFramework;
using Repository.Pattern.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

// ReSharper disable DoNotCallOverridableMethodsInConstructor

namespace Cage.DAL.Db.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class,
    // please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser
        :
            IdentityUser
                <Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>,
            IObjectState
    {
        public ApplicationUser()
        {
            Id = Guid.NewGuid();
        }

        public ApplicationUser(string username)
            : this()
        {
            UserName = username;
        }

        public ApplicationUser(Guid id, string username)
            : this()
        {
            Id = id;
            UserName = username;
        }

        #region Extended Properties

        // added by Laoenard on July 06, 2017 //
        public Guid User { get; set; }
        // added by Laoenard on July 06, 2017 //

        public DateTime Created { get; set; }

        public DateTime? LastLogin { get; set; }

        public string VerifiedToken { get; set; }

        public virtual ApplicationUserDetails UserDetails { get; set; }
        public virtual UserUnilevel UserUnilevel { get; set; }

        public virtual ICollection<File> Files { get; set; }

        [NotMapped]
        public ObjectState ObjectState { get; set; }

        #endregion Extended Properties
    }
}