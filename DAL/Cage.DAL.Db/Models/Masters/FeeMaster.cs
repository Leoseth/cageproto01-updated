﻿using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.Models.Masters
{
    public class FeeMaster : IObjectState
    {
        public int Category { get; set; }
        public int Type { get; set; }
        public decimal Amount { get; set; }

        [NotMapped]
        public ObjectState ObjectState { get; set; }
    }
}
