﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.DAL.Db.Models
{
    public class UserProvideHelpRelesedHistory : IObjectState
    {
        public Guid UserProvideHelpId { get; set; }
        public int Number { get; set; }
        public Guid UserId { get; set; }
        public int Type { get; set; }
        public decimal Amount { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        public UserProvideHelpRelesedHistory()
        {
            Created = Created = Updated = DateTime.UtcNow;
        }

        [NotMapped]
        public ObjectState ObjectState { get; set; }

    }
}
