using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abl;
using Repository.Pattern.Infrastructure;

namespace Cage.DAL.Db.Models.ViewModels
{
    public class Announcement : IObjectState
    {
        public int Id { get; set; }
        public string Subject { get; set; }
        [DataType(DataType.MultilineText)]
        public string Message { get; set; }
        public DateTime PublishDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool DelFlg { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        //TODO:parse to html
        //public string DispMessage =>
        public string DispPublishDate => PublishDate.ToLocalTime().ToSortableYmdHhMm();

        [NotMapped]
        public ObjectState ObjectState { get; set; }

        // Navigation

        public Announcement()
        {
        }
    }
}
