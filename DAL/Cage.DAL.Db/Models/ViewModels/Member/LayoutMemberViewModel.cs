﻿namespace Cage.DAL.Db.Models.ViewModels.Member
{
    public class LayoutMemberViewModel
    {
        public string AvatarImageUrl { get; set; }

        public ApplicationUser User { get; private set; }

        public LayoutMemberViewModel(ApplicationUser user)
        {
            User = user;
            AvatarImageUrl = "/images/profiles/no_img.jpg";
        }
    }
}