//using System;
//using System.ComponentModel.DataAnnotations.Schema;
//using Repository.Pattern.Infrastructure;
//using Cage.Core;

//namespace Cage.DAL.Db.Models
//{
//    public class ApplicationUserDetail : IObjectState
//    {
//        //public Guid UserId { get; set; }
//        //public string FirstName { get; set; }
//        //public string LastName { get; set; }
//        //public bool? Gender { get; set; }
//        //public string Address { get; set; }
//        //public string City { get; set; }
//        //public string Zipcode { get; set; }
//        //public string State { get; set; }
//        //public string Country { get; set; }
//        //public string Phone { get; set; }
//        //public string Mobile { get; set; }

//        //public int UserType { get; set; }
//        //public int UserClass { get; set; }

//        //public Guid ReferrerUserId { get; set; }
//        //public Guid ConsultantUserId { get; set; }

//        //public string BitcoinAddress { get; set; }

//        //public DateTime Created { get; set; }
//        //public DateTime Updated { get; set; }

//        //[NotMapped]
//        //public ObjectState ObjectState { get; set; }

//        ////Navigation
//        //public virtual ApplicationUser User { get; set; }

//        //public string UserTypeDisp => Enum.GetName(typeof(CageCode.UserType), UserType);
//        //public string UserClassDisp => Enum.GetName(typeof(CageCode.UserClass), UserClass);

//        //public ApplicationUserDetail() { }

//        public Guid UserId { get; set; }
//        public string FirstName { get; set; }
//        public string LastName { get; set; }
//        public bool? Gender { get; set; }
//        public string Address { get; set; }
//        public string City { get; set; }
//        public string Zipcode { get; set; }
//        public string State { get; set; }
//        public string Country { get; set; }
//        public string Phone { get; set; }
//        public string Mobile { get; set; }

//        public Guid ReferrerUserId { get; set; }
//        public Guid ConsultantUserId { get; set; }
//        public int Referrals { get; set; }
//        public int UserType { get; set; }
//        public int UserClass { get; set; }
//        public bool HasPosition { get; set; }
//        /// <summary>
//        /// PledgePhを一度でもしていればTrue
//        /// </summary>
//        public bool HasPledgePh { get; set; }
//        /// <summary>
//        /// pledgePhを一度でもしていればTrue
//        /// </summary>
//        public bool HasFirstPh { get; set; }

//        public string BitcoinAddress { get; set; }

//        [NotMapped]
//        public ObjectState ObjectState { get; set; }

//        // Navigation
//        public virtual ApplicationUser User { get; set; }


//        public string FullName => ($"{FirstName} {LastName}").Trim();
//        public string ListName => ($"{LastName}, {FirstName}").Trim();
//        public string UserTypeDisp => Enum.GetName(typeof(CageCode.UserType), UserType);
//        public string UserClassDisp => Enum.GetName(typeof(CageCode.UserClass), UserClass);
//        public string UserClassImageUrl => $"/images/class/{UserClass}.png";
//        public CageCode.UserClass? NextUserClass => UserClass + 1 < 8 ? (CageCode.UserClass)UserClass + 1 : (CageCode.UserClass?)null;
//        public string NextUserClassDisp
//            => NextUserClass == null ? string.Empty : Enum.GetName(typeof(CageCode.UserClass), NextUserClass);
//    }
//}