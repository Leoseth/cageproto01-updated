﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using Repository.Pattern.Infrastructure;

namespace Cage.DAL.Db.Models
{
    public class UserProvideHelp : IObjectState
    {
        public Guid UserProvideHelpId { get; set; }
        public Guid UserId { get; set; }
        public decimal Amount { get; set; }
        public decimal Filled { get; set; }
        public decimal InterestRate { get; set; }
        public int Status { get; set; }
        public decimal Released { get; set; }
        public DateTime? Activated { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        public decimal Remained => Amount - Filled;

        /// <summary>
        /// msecで計算
        /// </summary>
        public decimal Divinded
            => Math.Round(
                (decimal)((Activated ?? DateTime.UtcNow) - Created).TotalMilliseconds * Amount * ((InterestRate / 100) / (86400 * 1000)), 8, MidpointRounding.AwayFromZero
                ) - Released;

        public decimal DivindedAndCapital => Divinded + Amount;

        public string DispDivinded => Divinded.ToString(CultureInfo.InvariantCulture);

        public string DispDivindedAndCapital => DivindedAndCapital.ToString(CultureInfo.InvariantCulture);

        // Navigation
        public virtual ICollection<UserHelpAssigned> UserProvideHelpAssignedHistories { get; set; }
        public virtual ICollection<UserProvideHelpRelesedHistory> UserProvideHelpRelesedHistories { get; set; }

        public UserProvideHelp()
        {
            UserProvideHelpId = Guid.NewGuid();
            Created = Created = Updated = DateTime.UtcNow;
        }

        [NotMapped]
        public ObjectState ObjectState { get; set; }

    }
}
