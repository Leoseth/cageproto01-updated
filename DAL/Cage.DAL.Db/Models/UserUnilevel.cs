



using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.DAL.Db.Models
{
    public class UserUnilevel : IObjectState
    {
        public int Id { get; set; }

        // added by Leonard on July 06, 2017
        public string UplineUserId { get; set; }
        public string UserId { get; set; }
        // added by Leonard on July 06, 2017

        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        
        [NotMapped]
        public ObjectState ObjectState { get; set; }

        // Navigation

        // added by Leonard on July 06, 2017
        public virtual ApplicationUser User { get; set; }
        // added by Leonard on July 06, 2017

        public UserUnilevel()
        {
            // added by Leonard on July 06, 2017
            Created = Updated = DateTime.UtcNow;
    }
    }
}
