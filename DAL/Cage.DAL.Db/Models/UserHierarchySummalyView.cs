using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.DAL.Db.Models
{
    public class UserHierarchySummalyView : IObjectState
    {
        [Key, Column(Order = 1)]
        public Guid UserId { get; set; }
        [Key, Column(Order = 2)]
        public int DateYm { get; set; }
        public int MemberCount { get; set; }


        [NotMapped]
        public ObjectState ObjectState { get; set; }

        // Navigation

        public UserHierarchySummalyView()
        {
        }
    }
}
