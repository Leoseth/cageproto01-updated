using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.DAL.Db.Models
{
    public class UserPaymentSummaly : IObjectState
    {
        public Guid UserId { get; set; }
        public int DateYm { get; set; }
        public int Category { get; set; }
        public decimal Amount { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        [NotMapped]
        public ObjectState ObjectState { get; set; }

        // Navigation

        public UserPaymentSummaly()
        {
        }
    }
}
