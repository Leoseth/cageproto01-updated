﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity.EntityFramework;
using Repository.Pattern.Infrastructure;

namespace Cage.DAL.Db.Models
{
    public class ApplicationUserClaim
        : IdentityUserClaim<Guid>, IObjectState
    {
        [NotMapped]
        public ObjectState ObjectState { get; set; }
    }
}