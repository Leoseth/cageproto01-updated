﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Cage.DAL.Db.Models
{
    public class UserHierarchy : IObjectState
    {
        public Guid UserId { get; set; }
        public int Position { get; set; }
        public Guid DownlineUserId { get; set; }
        public int? Level { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        [NotMapped]
        public ObjectState ObjectState { get; set; }

        // Navigation
        public virtual ApplicationUser User { get; set; }

        public UserHierarchy()
        {
            Created = Updated = DateTime.UtcNow;
        }
    }
}
