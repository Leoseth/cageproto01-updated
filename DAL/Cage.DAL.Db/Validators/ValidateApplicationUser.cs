﻿using System.Data.Entity.Validation;
using System.Linq;
using Cage.DAL.Db.Models;
using Identity.Resources;
using Resources;

namespace Cage.DAL.Db.Validators
{
    public static partial class DataContextExtension
    {
        public static void
            ValidateApplicationUser(
            this CageDataContext dbContext,
            DbEntityValidationResult result)
        {
            var entity = result.Entry.Entity as ApplicationUser;
            if (entity == null)
            {
                return;
            }

            var checkUserName = dbContext
                .ApplicationUsers.FirstOrDefault(x => x.UserName == entity.UserName);

            if ((checkUserName != null) && (checkUserName.Id != entity.Id))
            {
                result.ValidationErrors.Add(
                    new DbValidationError(
                        // A {0} with the {1} of '{2}' is already registered ({3})
                        "UserName",
                        string.Format(
                            ModelValidationResources.NonUniqueField_NoReference,
                            AccountResources.User_Account,
                            AccountResources.UserName, entity.UserName)));
            }

            var checkEmail = dbContext.ApplicationUsers.FirstOrDefault(x => x.Email == entity.Email);

            if ((checkEmail != null) && (checkEmail.Id != entity.Id))
            {
                result.ValidationErrors.Add(
                    new DbValidationError(
                        // A {0} with the {1} of '{2}' is already registered ({3})
                        "Email",
                        string.Format(
                            ModelValidationResources.NonUniqueField_NoReference,
                            AccountResources.User_Account,
                            AccountResources.Email, entity.Email)));
            }
        }
    }
}