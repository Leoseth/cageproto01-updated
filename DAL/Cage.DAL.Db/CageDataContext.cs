﻿using Cage.DAL.Db.Configurations;
using Cage.DAL.Db.Models;
using Cage.DAL.Db.Validators;
using NLog;
using Repository.Pattern.Ef6;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Reflection;

namespace Cage.DAL.Db
{
    public partial class CageDataContext
        : DataContext, IDataContextAsync
    {
        protected static readonly NLog.ILogger Logger =
            LogManager.GetLogger(Assembly.GetExecutingAssembly().FullName,
            MethodBase.GetCurrentMethod().DeclaringType);

        #region DataSets

        public DbSet<ApplicationRole> ApplicationRoles { get; set; }
        public DbSet<ApplicationUserClaim> ApplicationUserClaims { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<ApplicationUserRole> ApplicationUserRoles { get; set; }
        public DbSet<ApplicationUserDetails> ApplicationUserDetails { get; set; }
        
        //added by Leonard //
        public DbSet<UserPaymentSummaly> UserPaymentSummalies { get; set; }
        public DbSet<ProvideHelpQueue> ProvideHelpQueues { get; set; }
        public DbSet<ProvideHelpQueueHistory> ProvideHelpQueueHistories { get; set; }
        public DbSet<UserHelpAssigned> UserHelpAssigneds { get; set; }
        public DbSet<UserHierarchy> UserHierarchies { get; set; }
        public DbSet<UserHierarchyMonthlyPairing> UserHierarchyMonthlyPairings { get; set; }
        public DbSet<UserHierarchyMonthlyPairingHistory> UserHierarchyMonthlyPairingHistories { get; set; }
        public DbSet<UserHierarchySummalyView> UserHierarchySummalyViews { get; set; }
        public DbSet<UserProvideHelp> UserProvideHelps { get; set; }
        public DbSet<UserProvideHelpMonthlyHistory> UserProvideHelpMonthlyHistories { get; set; }
        public DbSet<UserProvideHelpRelesedHistory> UserProvideHelpRelesedHistories { get; set; }
        public DbSet<UserShareHistorySummaly> UserShareHistorySummalies { get; set; }
        //added by leonard //

        public DbSet<UserUnilevel> UserUnilevels { get; set; }

        public DbSet<File> Files { get; set; }
        public DbSet<ViewControl> ViewControl { get; set; }

        // Views

        #endregion DataSets

        #region Constructors

        public CageDataContext()
            : base("Name=WebApp")
        {
            // Default URF configuration
            // Configuration.LazyLoadingEnabled = false;
            // Configuration.ProxyCreationEnabled = false;

            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;

            // ロギングをデリゲート
#if DEBUG
            Database.Log = p => Debug.WriteLine(p);
#else
            Database.Log = p => Logger.Debug(p);
#endif
        }

        public static CageDataContext Create()
        {
            return new CageDataContext();
        }

        static CageDataContext()
        {
            Database.SetInitializer(new CageSeedData());

            //Database.SetInitializer
            //    (new CreateDatabaseIfNotExists<AntDataContext>());
        }

        #endregion Constructors

        #region OnModelCreating

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // ASP.NET User Identity
            modelBuilder.Configurations.Add(new ApplicationRoleMap());
            modelBuilder.Configurations.Add(new ApplicationUserClaimMap());
            modelBuilder.Configurations.Add(new ApplicationUserLoginMap());
            modelBuilder.Configurations.Add(new ApplicationUserMap());
            modelBuilder.Configurations.Add(new ApplicationUserRoleMap());
            modelBuilder.Configurations.Add(new ApplicationUserDetailMap());

            //added by Leonard
            modelBuilder.Configurations.Add(new UserPaymentSummalyMap());
            modelBuilder.Configurations.Add(new ProvideHelpQueueMap());
            modelBuilder.Configurations.Add(new ProvideHelpQueueHistoryMap());
            modelBuilder.Configurations.Add(new UserHelpAssignedMap());
            modelBuilder.Configurations.Add(new UserHierarchyMap());
            modelBuilder.Configurations.Add(new UserHierarchyMonthlyPairingMap());
            modelBuilder.Configurations.Add(new UserHierarchyMonthlyPairingHistoryMap());
            modelBuilder.Configurations.Add(new UserHierarchySummalyViewMap());
            modelBuilder.Configurations.Add(new UserProvideHelpMap());
            modelBuilder.Configurations.Add(new UserProvideHelpMonthlyHistoryMap());
            modelBuilder.Configurations.Add(new UserProvideHelpRelesedHistoryMap());
            modelBuilder.Configurations.Add(new UserShareHistorySummalyMap());
            // added by Leonard

            // Project
            modelBuilder.Configurations.Add(new UserUnilevelMap());
            modelBuilder.Configurations.Add(new FileMap());
            modelBuilder.Configurations.Add(new ViewControlMap());

            // Ignore views
        }

        #endregion OnModelCreating

        #region Validation

        // See http://msdn.microsoft.com/en-gb/data/gg193959.aspx
        // and http://stackoverflow.com/a/18736484/236860

        protected override DbEntityValidationResult
            ValidateEntity(
            DbEntityEntry entityEntry,
            IDictionary<object, object> items)
        {
            // Base validation for Data Annotations, IValidatableObject
            var result = base.ValidateEntity(entityEntry, items);

            // Only validate entities new/updated entities
            if ((result.Entry.State != EntityState.Added) &&
                (result.Entry.State != EntityState.Modified))
            {
                return result;
            }

            // Validate User Identity
            this.ValidateApplicationUser(result);
            this.ValidateApplicationRole(result);

            return result;
        }

        #endregion Validation
    }
}