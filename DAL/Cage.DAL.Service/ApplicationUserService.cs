﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Cage.DAL.Db.Models;
using Cage.DAL.Db.Models.ViewModels;
using Cage.DAL.Repository;
using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service.Interfaces;
using Service.Pattern;

namespace Cage.DAL.Service
{
    public class ApplicationUserService
        : Service<ApplicationUser>, IApplicationUserService
    {
        private readonly ICageRepositoryAsync<ApplicationUser> _repository;


        public ApplicationUserService(
            ICageRepositoryAsync<ApplicationUser> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public async Task<ApplicationUser>
            GetUserDetailsAsync(Guid userId)
        {
            return await _repository
                .Queryable()
                .Include(e => e.Roles)
                .FirstOrDefaultAsync(e => e.Id == userId);
        }

        public async Task<ApplicationUser> GetUserDetailsAsync(string userName)
        {
            return await _repository
                .Queryable()
                .Include(e => e.Roles)
                .FirstOrDefaultAsync(e => e.UserName.Equals(userName));
        }

        public IEnumerable<ApplicationUserViewModel>
            GetUsersInRole(Guid roleId)
        {
            return _repository.GetUsersInRole(roleId);
        }
    }
}