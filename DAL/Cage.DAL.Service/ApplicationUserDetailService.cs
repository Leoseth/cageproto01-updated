using Cage.DAL.Db.Models;
using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service.Interfaces;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cage.DAL.Service
{
    public class ApplicationUserDetailService
        : Service<ApplicationUserDetails>, IApplicationUserDetailService
    {
        private readonly ICageRepositoryAsync<ApplicationUserDetails> _repository;

        public ApplicationUserDetailService(
            ICageRepositoryAsync<ApplicationUserDetails> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<ApplicationUserDetails>> GetApplicationUserDetails(IList<Guid> uids)
        {
            return await _repository.Query(x => uids.Contains(x.UserId)).SelectAsync();
        }

        public async Task<IDictionary<UserCategoryForUpgrade, ApplicationUserDetails>>
            GetApplicationUserDetailsForUpgrade(Guid me, Guid refe, Guid up)
        {
            var uds = await GetApplicationUserDetails(new List<Guid> { me, refe, up });
            var applicationUserDetailses = uds as ApplicationUserDetails[] ?? uds.ToArray();
            var dict = new Dictionary<UserCategoryForUpgrade, ApplicationUserDetails>
            {
                {UserCategoryForUpgrade.Me, applicationUserDetailses.FirstOrDefault(x => x.UserId == me)},
                {UserCategoryForUpgrade.Ref, applicationUserDetailses.FirstOrDefault(x => x.UserId == refe)},
                {UserCategoryForUpgrade.Up, applicationUserDetailses.FirstOrDefault(x => x.UserId == up)}
            };

            return dict;
        }

        public int UpdateUserDetailsIncrementReferrals(Guid userId)
        {
            return _repository.ExecuteQuery("dbo.UpdateUserDetailsIncrementReferrals {0}", userId);
        }
    }
}