using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cage.DAL.Db.Models;
using Service.Pattern;

namespace Cage.DAL.Service.Interfaces
{
    public interface IFileService
        : IService<File>
    {
        Task<IEnumerable<File>> GetData(Guid userId, int contentType);
    }
}