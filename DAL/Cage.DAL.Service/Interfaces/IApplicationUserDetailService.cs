using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Service.Pattern;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Service.Interfaces
{
    public interface IApplicationUserDetailService : IService<ApplicationUserDetails>
    {
        Task<IEnumerable<ApplicationUserDetails>> GetApplicationUserDetails(IList<Guid> uids);
        Task<IDictionary<UserCategoryForUpgrade, ApplicationUserDetails>> GetApplicationUserDetailsForUpgrade(Guid Me, Guid refe, Guid up);
        int UpdateUserDetailsIncrementReferrals(Guid userId);
    }

    public enum UserCategoryForUpgrade
    {
        Me,
        Ref,
        Up,
    }
}

