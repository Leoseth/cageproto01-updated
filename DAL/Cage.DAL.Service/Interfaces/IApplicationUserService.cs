using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cage.DAL.Db.Models;
using Cage.DAL.Db.Models.ViewModels;
using Service.Pattern;

namespace Cage.DAL.Service.Interfaces
{
    public interface IApplicationUserService
        :IService<ApplicationUser>
    {
        Task<ApplicationUser> GetUserDetailsAsync(Guid userId);
        Task<ApplicationUser> GetUserDetailsAsync(string userName);
        IEnumerable<ApplicationUserViewModel> GetUsersInRole(Guid roleId);
    }
}