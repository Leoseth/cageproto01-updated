﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cage.Models;
using Service.Pattern;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Service.Interfaces
{
    public interface IUserProvideHelpService : IService<UserProvideHelp>
    {
        Task<IEnumerable<UserProvideHelp>> GetUserProvideHelp(Guid userId);
        Task<IEnumerable<UserProvideHelp>> GetEffectiveUserProvideHelps(Guid userId);
        IEnumerable<UserProvideHelp> GetEffectiveUserProvideHelps(IEnumerable<UserProvideHelp> userProvideHelps);
        Task<decimal> GetEffectiveProvideHelpAmounts(Guid userId);
        decimal GetEffectiveProvideHelpAmounts(IEnumerable<UserProvideHelp> userProvideHelps);
        decimal GetAllowdProvideHelpValue(IEnumerable<UserProvideHelp> userProvideHelps);
        int UpdateUserProvideHelpsRelease(Guid userId, decimal amount, int status, DateTime activated);
    }
}
