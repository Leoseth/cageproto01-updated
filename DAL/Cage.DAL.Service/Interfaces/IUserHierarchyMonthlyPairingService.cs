using System;
using System.Collections.Generic;
using Service.Pattern;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Service.Interfaces
{
    public interface IUserHierarchyMonthlyPairingService
        : IService<UserHierarchyMonthlyPairing>
    {
        IEnumerable<UserHierarchyMonthlyPairing> GetUserHierarchyMonthlyPairings(Guid userId, DateTime date);
    }
}