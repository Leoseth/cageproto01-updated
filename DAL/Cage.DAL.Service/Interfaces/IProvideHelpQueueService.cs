﻿using System;
using Cage.DAL.Db.Models;
using Service.Pattern;

namespace Cage.DAL.Service.Interfaces
{
    public interface IProvideHelpQueueService : IService<ProvideHelpQueue>
    {
        ProvideHelpQueue GetProvideHelpQueue(Guid userId, Guid userProvideHelpId);
    }
}
