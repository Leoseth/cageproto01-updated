using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Service.Pattern;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Service.Interfaces
{
    public interface IUserHierarchyMonthlyPairingHistoryService
        : IService<UserHierarchyMonthlyPairingHistory>
    {
        Task<IEnumerable<UserHierarchyMonthlyPairingHistory>> GetUserHierarchyMonthlyPairingHistoriesByAfterTargetDate(Guid userId, DateTime dateYyyyMm);
    }
}