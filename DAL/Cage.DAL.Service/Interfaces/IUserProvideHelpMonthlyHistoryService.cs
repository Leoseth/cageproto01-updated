using System;
using Cage.DAL.Db.Models;
using Service.Pattern;

namespace Cage.DAL.Service.Interfaces
{
    public interface IUserProvideHelpMonthlyHistoryService
        : IService<UserProvideHelpMonthlyHistory>
    {
        int UpsertUserProvideHelpMonthlyHistoriesAmount(Guid userId, DateTime date, decimal amount);
    }
}