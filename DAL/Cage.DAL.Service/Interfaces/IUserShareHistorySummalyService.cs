using System;
using Cage.Core;
using Cage.DAL.Db.Models;
using Service.Pattern;

namespace Cage.DAL.Service.Interfaces
{
    public interface IUserShareHistorySummalyService
        : IService<UserShareHistorySummaly>
    {
        int UserShareHistorySummalies(Guid userId, CageCode.ShareCategory shreCategory,decimal amount);
    }
}