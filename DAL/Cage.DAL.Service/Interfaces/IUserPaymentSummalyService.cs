using System;
using Cage.Core;
using Cage.DAL.Db.Models;
using Service.Pattern;

namespace Cage.DAL.Service.Interfaces
{
    public interface IUserPaymentSummalyService
        : IService<UserPaymentSummaly>
    {
        int UpsertUserPaymentSummalies(Guid userId, CageCode.PaymentSummaryCategory paymentSummaryCategory,
            decimal amount);
    }
}