using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Service.Pattern;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Service.Interfaces
{
    public interface IUserHierarchyService
        : IService<UserHierarchy>
    {
        UserHierarchy GetUserHierarchyByDownuserId(Guid downlineUid);
        Task<IEnumerable<UserHierarchy>> GetUserHierarchyAsync(Guid uplineUid);

        IEnumerable<UserHierarchy> GetUserHierarchy(Guid userId);
        IEnumerable<UserHierarchy> GetUserHierarchyByLevel(Guid userId, int level);
        IEnumerable<UserHierarchy> GetUpUserHierarchys(Guid userId);
        IEnumerable<UserHierarchy> GetUpUserHierarchysByLevel(Guid userId, int level);
        UserHierarchy GetUpUserHierarchysByLevelFirstOrDefault(Guid userId, int level);
    }
}