﻿using Cage.DAL.Db.Models;
using Service.Pattern;

namespace Cage.DAL.Service.Interfaces
{
    public interface IUserProvideHelpRelesedHistoryService : IService<UserProvideHelpRelesedHistory>
    {
    }
}
