﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cage.Core;
using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service.Interfaces;
using Cage.Models;
using Service.Pattern;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Service
{
    public class UserProvideHelpService : Service<UserProvideHelp>, IUserProvideHelpService
    {
        private readonly ICageRepositoryAsync<UserProvideHelp> _repository;

        public UserProvideHelpService(ICageRepositoryAsync<UserProvideHelp> repository) : base(repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<UserProvideHelp>> GetUserProvideHelp(Guid userId)
        {
            return await _repository.Query(x => x.UserId.Equals(userId)).SelectAsync();

        }

        public async Task<IEnumerable<UserProvideHelp>> GetEffectiveUserProvideHelps(Guid userId)
        {
            return
                await
                    _repository.Query(
                        x => x.UserId.Equals(userId) && CageCode.EffectiveProvideHelpStatus.Contains(x.Status))
                        .SelectAsync();
        }

        public IEnumerable<UserProvideHelp> GetEffectiveUserProvideHelps(IEnumerable<UserProvideHelp> userProvideHelps)
        {
            return userProvideHelps.Where(x => CageCode.EffectiveProvideHelpStatus.Contains(x.Status));
        }

        public async Task<decimal> GetEffectiveProvideHelpAmounts(Guid userId)
        {
            return (await GetEffectiveUserProvideHelps(userId)).Sum(x => x.Amount);
        }

        public decimal GetEffectiveProvideHelpAmounts(IEnumerable<UserProvideHelp> userProvideHelps)
        {
            return userProvideHelps.Sum(x => x.Amount);
        }

        public decimal GetAllowdProvideHelpValue(IEnumerable<UserProvideHelp> userProvideHelps)
        {
            var effectiveProvideHelpAmounts = GetEffectiveProvideHelpAmounts(userProvideHelps);
            return CageSettings.ProvideHelpLimit - effectiveProvideHelpAmounts;
        }

        public int UpdateUserProvideHelpsRelease(Guid userId, decimal amount, int status, DateTime activated)
        {
            return _repository.ExecuteQuery("dbo.UpdateUserProvideHelpsRelease {0}, {1}, {2}, {3}", userId, amount, status, activated);
        }
    }
}