﻿using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service.Interfaces;
using Cage.DAL.Db.Models;
using Service.Pattern;

namespace Cage.DAL.Service
{
    public class UserHelpAssignedService : Service<UserHelpAssigned>, IUserHelpAssignedService
    {
        private readonly ICageRepositoryAsync<UserHelpAssigned> _repository;

        public UserHelpAssignedService(ICageRepositoryAsync<UserHelpAssigned> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}