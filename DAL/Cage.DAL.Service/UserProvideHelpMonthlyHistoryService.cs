using System;
using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service.Interfaces;
using Cage.DAL.Db.Models;
using Service.Pattern;

namespace Cage.DAL.Service
{
    public class UserProvideHelpMonthlyHistoryService
        : Service<UserProvideHelpMonthlyHistory>, IUserProvideHelpMonthlyHistoryService
    {
        private readonly ICageRepositoryAsync<UserProvideHelpMonthlyHistory> _repository;

        public UserProvideHelpMonthlyHistoryService(
            ICageRepositoryAsync<UserProvideHelpMonthlyHistory> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public int UpsertUserProvideHelpMonthlyHistoriesAmount(Guid userId, DateTime date, decimal amount)
        {
            return _repository.ExecuteQuery("dbo.UpsertUserProvideHelpMonthlyHistoriesAmount {0}, {1}, {2}", userId, date, amount);
        }
    }
}