﻿using System;
using System.Threading.Tasks;
using Cage.DAL.Db;

namespace Cage.DAL.Service
{
    public interface IStoredProcedureService
    {
    }

    public class StoredProcedureService : IStoredProcedureService
    {
        private readonly ICageStoredProcedures _storedProcedures;

        public StoredProcedureService(ICageStoredProcedures storedProcedures)
        {
            _storedProcedures = storedProcedures;
        }
    }
}