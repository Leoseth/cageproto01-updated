﻿using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service.Interfaces;
using Cage.DAL.Db.Models;
using Service.Pattern;

namespace Cage.DAL.Service
{
    public class UserShareHistoryService : Service<UserShareHistory>, IUserShareHistoryService
    {
        private readonly ICageRepositoryAsync<UserShareHistory> _repository;

        public UserShareHistoryService(ICageRepositoryAsync<UserShareHistory> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
