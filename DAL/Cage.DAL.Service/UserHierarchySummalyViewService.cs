using Cage.DAL.Db;
using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service.Interfaces;
using Service.Pattern;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Service
{
    public class UserHierarchySummalyViewService
        : Service<UserHierarchySummalyView>, IUserHierarchySummalyViewService
    {
        private readonly ICageRepositoryAsync<UserHierarchySummalyView> _repository;

        public UserHierarchySummalyViewService(
            ICageRepositoryAsync<UserHierarchySummalyView> repository)
            : base(repository)
        {
            _repository = repository;
        }
    }
}