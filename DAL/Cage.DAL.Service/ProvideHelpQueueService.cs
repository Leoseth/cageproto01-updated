﻿using System;
using System.Linq;
using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service.Interfaces;
using Cage.DAL.Db.Models;
using Service.Pattern;

namespace Cage.DAL.Service
{
    public class ProvideHelpQueueService : Service<ProvideHelpQueue>, IProvideHelpQueueService
    {
        private readonly ICageRepositoryAsync<ProvideHelpQueue> _repository;

        public ProvideHelpQueueService(ICageRepositoryAsync<ProvideHelpQueue> repository) : base(repository)
        {
            _repository = repository;
        }

        public ProvideHelpQueue GetProvideHelpQueue(Guid userId, Guid userProvideHelpId)
        {
            return _repository.Query(x => x.UserProvideHelpId.Equals(userProvideHelpId)).Select().FirstOrDefault();
        }
    }
}
