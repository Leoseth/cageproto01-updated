using Cage.DAL.Db.Models;
using Cage.DAL.Repository.Infrastructure;
using Service.Pattern;

namespace Cage.DAL.Service
{

    public interface ITestService
        : IService<Test>
    {
    }

    public class TestService
        : Service<Test>, ITestService
    {
        private readonly ICageRepositoryAsync<Test> _repository;

        public TestService(
            ICageRepositoryAsync<Test> repository)
            : base(repository)
        {
            _repository = repository;
        }
    }
}