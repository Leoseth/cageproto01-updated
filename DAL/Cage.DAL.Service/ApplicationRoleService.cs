﻿using Cage.DAL.Db.Models;
using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service.Interfaces;
using Service.Pattern;

namespace Cage.DAL.Service
{
    public class ApplicationRoleService
        : Service<ApplicationRole>, IApplicationRoleService
    {
        private readonly ICageRepositoryAsync<ApplicationRole> _repository;

        public ApplicationRoleService(
            ICageRepositoryAsync<ApplicationRole> repository)
            : base(repository)
        {
            _repository = repository;
        }
    }
}