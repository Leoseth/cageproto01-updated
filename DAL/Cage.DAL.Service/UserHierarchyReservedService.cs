﻿using Cage.DAL.Db;
using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service.Interfaces;
using Service.Pattern;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Service
{
    public class UserHierarchyReservedService : Service<UserHierarchyReserved>, IUserHierarchyReservedService
    {
        private readonly ICageRepositoryAsync<UserHierarchyReserved> _repository;

        public UserHierarchyReservedService(ICageRepositoryAsync<UserHierarchyReserved> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
