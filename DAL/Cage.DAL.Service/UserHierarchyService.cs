﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service.Interfaces;
using Service.Pattern;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Service
{
    public class UserHierarchyService : Service<UserHierarchy>, IUserHierarchyService
    {
        private readonly ICageRepositoryAsync<UserHierarchy> _repository;

        public UserHierarchyService(ICageRepositoryAsync<UserHierarchy> repository) : base(repository)
        {
            _repository = repository;
        }

        public UserHierarchy GetUserHierarchyByDownuserId(Guid downlineUid)
        {
            return _repository.Query(x => x.DownlineUserId == downlineUid).Select().First();
        }

        public async Task<IEnumerable<UserHierarchy>> GetUserHierarchyAsync(Guid uplineUid)
        {
            return await _repository.Query(x => x.UserId == uplineUid).SelectAsync();
        }

        // with level


        public IEnumerable<UserHierarchy> GetUserHierarchy(Guid userId)
        {
            return _repository.SelectQuery(Queries.GetUserHierarchyBase + @"
SELECT 
    UserId, Position, DownlineUserId, Level, Created, Updated
FROM 
    uhCTE",
                new SqlParameter("@userId", userId.ToString())).OrderBy(x => x.Level);
        }

        public IEnumerable<UserHierarchy> GetUserHierarchyByLevel(Guid userId, int level)
        {
            var userIdStr = userId.ToString();
            return _repository.SelectQuery(Queries.GetUserHierarchyBase + @"
SELECT 
    UserId, Position, DownlineUserId, Level, Created, Updated
FROM 
    uhCTE
WHERE Level <= @level", new SqlParameter("@userId", userIdStr), new SqlParameter("@level", level));
        }

        /// <summary>
        /// 自分の上位のユーザをルートまで取得
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<UserHierarchy> GetUpUserHierarchys(Guid userId)
        {
            return _repository.SelectQuery(Queries.GetUpUserHierarchysBase + @"
SELECT 
	UserId, Position, DownlineUserId, LEVEL * -1 AS Level, Created, Updated
FROM uhCTE 
ORDER BY Level", new SqlParameter("@userId", userId.ToString()));
        }

        /// <summary>
        /// 自分の上位のユーザを指定レベルまで取得
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        public IEnumerable<UserHierarchy> GetUpUserHierarchysByLevel(Guid userId, int level)
        {
            return _repository.SelectQuery(Queries.GetUpUserHierarchysBase + @"
SELECT 
	UserId, Position, DownlineUserId, LEVEL * -1 AS Level, Created, Updated 
FROM uhCTE 
WHERE LEVEL * -1 < @level
ORDER BY Level", new SqlParameter("@userId", userId.ToString()), new SqlParameter("@level", level));
        }

        /// <summary>
        /// 指定レベルの上位ユーザを取得する(取得レコードのUserIdが対象)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        public UserHierarchy GetUpUserHierarchysByLevelFirstOrDefault(Guid userId, int level)
        {
            return GetUpUserHierarchysByLevel(userId, level).OrderByDescending(x => x.Level).FirstOrDefault();
        }
    }
}
