﻿namespace Cage.DAL.Service
{
    internal static class Queries
    {
        internal const string GetUserHierarchyBase = @";WITH uhCTE (
	UserId ,Position ,DownlineUserId ,LEVEL, Created, Updated
	)
AS (
	SELECT 
		UserId ,Position, DownlineUserId, 1 AS LEVEL, Created, Updated
	FROM 
		dbo.UserHierarchies t1
	WHERE UserId = @userId
	UNION ALL
	SELECT 
		c.UserId, c.Position, c.DownlineUserId, cte.LEVEL + 1 AS LEVEL, c.Created, c.Updated
	FROM 
		dbo.UserHierarchies c
	INNER JOIN uhCTE AS cte ON c.UserId = cte.DownlineUserId
	)";

        internal const string GetUpUserHierarchysBase = @";WITH uhCTE (
	UserId ,Position ,DownlineUserId ,LEVEL, Created, Updated
	)
AS (
	SELECT 
		UserId ,Position, DownlineUserId, 0 AS LEVEL, Created, Updated
	FROM 
		dbo.UserHierarchies
	WHERE DownlineUserId = @userId
	UNION ALL
	SELECT 
		c.UserId, c.Position, c.DownlineUserId, cte.LEVEL - 1 AS LEVEL, c.Created, c.Updated
	FROM 
		dbo.UserHierarchies c
	INNER JOIN uhCTE AS cte ON c.DownlineUserId = cte.UserId
    )";
    }
}
