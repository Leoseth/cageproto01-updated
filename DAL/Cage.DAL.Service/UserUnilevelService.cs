




using Cage.DAL.Db.Models;
using Cage.DAL.Repository.Infrastructure;
using Service.Pattern;

namespace Cage.DAL.Service
{

    public interface IUserUnilevelService
        : IService<UserUnilevel>
    {
    }

    public class UserUnilevelService
        : Service<UserUnilevel>, IUserUnilevelService
    {
        private readonly ICageRepositoryAsync<UserUnilevel> _repository;

        public UserUnilevelService(
            ICageRepositoryAsync<UserUnilevel> repository)
            : base(repository)
        {
            _repository = repository;
        }
    }
}