using System;
using Cage.Core;
using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service.Interfaces;
using Cage.DAL.Db.Models;
using Service.Pattern;

namespace Cage.DAL.Service
{
    public class UserPaymentSummalyService
        : Service<UserPaymentSummaly>, IUserPaymentSummalyService
    {
        private readonly ICageRepositoryAsync<UserPaymentSummaly> _repository;

        public UserPaymentSummalyService(
            ICageRepositoryAsync<UserPaymentSummaly> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public int UpsertUserPaymentSummalies(Guid userId, CageCode.PaymentSummaryCategory paymentSummaryCategory, decimal amount)
        {
            return _repository.ExecuteQuery("dbo.UpsertUserPaymentSummalies {0}", userId, (int)paymentSummaryCategory, amount);
        }
    }
}