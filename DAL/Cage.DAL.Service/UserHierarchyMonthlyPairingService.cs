using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service.Interfaces;
using Service.Pattern;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Service
{
    public class UserHierarchyMonthlyPairingService
        : Service<UserHierarchyMonthlyPairing>, IUserHierarchyMonthlyPairingService
    {
        private readonly ICageRepositoryAsync<UserHierarchyMonthlyPairing> _repository;

        public UserHierarchyMonthlyPairingService(
            ICageRepositoryAsync<UserHierarchyMonthlyPairing> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public IEnumerable<UserHierarchyMonthlyPairing> GetUserHierarchyMonthlyPairings(Guid userId, DateTime date)
        {
            return _repository.SelectQuery(Queries.GetUpUserHierarchysBase + @"
SELECT 
    DownlineUserId AS UserId, uphmh.Date, uphmh.Amount
FROM 
    uhCTE uh
	LEFT OUTER JOIN
	UserProvideHelpMonthlyHistories uphmh
ON
	uh.DownlineUserId = uphmh.UserId
WHERE 
	Date >= @date AND
	Date IS NOT NULL", new SqlParameter("@userId", userId.ToString()), new SqlParameter("@date", date));
        }
    }
}