using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cage.DAL.Db.Models;
using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service.Interfaces;
using Service.Pattern;

namespace Cage.DAL.Service
{
    public class FileService
        : Service<File>, IFileService
    {
        private readonly ICageRepositoryAsync<File> _repository;

        public FileService(
            ICageRepositoryAsync<File> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public Task<IEnumerable<File>> GetData(Guid userId, int contentType)
        {
            return _repository.Query(x => x.FileType == (int)FileType.Avatar).SelectAsync();
        }
    }
}