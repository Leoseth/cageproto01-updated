﻿using System;
using System.Threading.Tasks;
using Cage.Core;
using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service.Interfaces;

namespace Cage.DAL.Service.Manager
{
    public class ProvideHelpManager : BaseManager
    {
        public readonly IUserProvideHelpService UserProvideHelpService;
        public readonly IUserProvideHelpRelesedHistoryService UserProvideHelpRelesedHistoryService;
        private readonly IUserProvideHelpMonthlyHistoryService _userProvideHelpMonthlyHistoryService;
        public readonly IUserHelpAssignedService UserHelpAssignedService;
        public readonly IProvideHelpQueueService ProvideHelpQueueService;
        private readonly IProvideHelpQueueHistoryService _provideHelpQueueHistoryService;

        public ProvideHelpManager(
            ICageUnitOfWorkAsync unitOfWork,
            IUserProvideHelpService userProvideHelpService, 
            IUserProvideHelpRelesedHistoryService userProvideHelpRelesedHistoryService,
            IUserProvideHelpMonthlyHistoryService userProvideHelpMonthlyHistoryService,
            IUserHelpAssignedService userHelpAssignedService, 
            IProvideHelpQueueService provideHelpQueueService, 
            IProvideHelpQueueHistoryService provideHelpQueueHistoryService
            ) : base(unitOfWork)
        {
            UserProvideHelpService = userProvideHelpService;
            UserProvideHelpRelesedHistoryService = userProvideHelpRelesedHistoryService;
            _userProvideHelpMonthlyHistoryService = userProvideHelpMonthlyHistoryService;
            UserHelpAssignedService = userHelpAssignedService;
            ProvideHelpQueueService = provideHelpQueueService;
            _provideHelpQueueHistoryService = provideHelpQueueHistoryService;
        }

        public int UpsertUserProvideHelpMonthlyHistoriesAmount(Guid userId, decimal amount)
        {
            var date = DateTime.UtcNow;
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            return UpsertUserProvideHelpMonthlyHistoriesAmount(userId, firstDayOfMonth, amount);
        }

        public int UpsertUserProvideHelpMonthlyHistoriesAmount(Guid userId, DateTime date, decimal amount)
        {
            return _userProvideHelpMonthlyHistoryService.UpsertUserProvideHelpMonthlyHistoriesAmount(userId, date, amount);
        }

        public async Task<int> CancelProvideHelpAsync(Guid userId, Guid userProvideHelpId)
        {
            var updted = DateTime.UtcNow;

            // ProvideHelpQueue
            // ProvideHelpQueueHistory
            var provideHelpQueue =
                ProvideHelpQueueService.GetProvideHelpQueue(userId, userProvideHelpId);
            if (provideHelpQueue != null)
            {
                _provideHelpQueueHistoryService.Insert(provideHelpQueue);
                await ProvideHelpQueueService.DeleteAsync(provideHelpQueue.Id);
            }

            // UserProvideHelp
            var updatUph = await UserProvideHelpService.FindAsync(userProvideHelpId);
            updatUph.Status = (int)CageCode.ProvideHelpStatus.Canceled;
            updatUph.Activated = updted;
            updatUph.Updated = updted;
            UserProvideHelpService.Update(updatUph);

            // TODO:マッチング前のはずなので、対象のUserHelpAssignedsはないはず
            // UserHelpAssigneds
            var uphahs =
                await UserHelpAssignedService.Query(x => x.UserProvideHelpId.Equals(userProvideHelpId)).SelectAsync();
            foreach (var uphah in uphahs)
            {
                uphah.Status = (int)CageCode.HelpAssignedStatus.Canceled;
                uphah.Updated = updted;
                UserHelpAssignedService.Update(uphah);
            }
            return await UnitOfWork.SaveChangesAsync();
        }
    }
}
