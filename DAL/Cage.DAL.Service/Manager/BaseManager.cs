﻿using Cage.DAL.Repository.Infrastructure;

namespace Cage.DAL.Service.Manager
{
    public class BaseManager
    {
        protected static readonly NLog.ILogger Logger = NLog.LogManager.GetCurrentClassLogger();
        protected readonly ICageUnitOfWorkAsync UnitOfWork;
        public BaseManager(ICageUnitOfWorkAsync unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }
    }
}
