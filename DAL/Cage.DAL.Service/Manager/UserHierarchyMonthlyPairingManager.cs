﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cage.Core;
using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service.Interfaces;

namespace Cage.DAL.Service.Manager
{
    public class UserHierarchyMonthlyPairingManager : BaseManager
    {
        private readonly IUserHierarchyService _userHierarchyService;
        private readonly IUserHierarchyMonthlyPairingService _userHierarchyMonthlyPairingService;
        private readonly IUserHierarchyMonthlyPairingHistoryService _userHierarchyMonthlyPairingHistoryService;

        public UserHierarchyMonthlyPairingManager(
            ICageUnitOfWorkAsync unitOfWork,
            IUserHierarchyService userHierarchyService,
            IUserHierarchyMonthlyPairingService userHierarchyMonthlyPairingService, 
            IUserHierarchyMonthlyPairingHistoryService userHierarchyMonthlyPairingHistoryService) : base(unitOfWork)
        {
            _userHierarchyService = userHierarchyService;
            _userHierarchyMonthlyPairingService = userHierarchyMonthlyPairingService;
            _userHierarchyMonthlyPairingHistoryService = userHierarchyMonthlyPairingHistoryService;
        }

        public async Task<decimal> GetNextPairingValue(Guid userId)
        {
            var values = await GetPairingValues(userId);
            var minValue = values.Where(x => !x.Key.Equals(CageCode.UserPosition.Center)).Min(x => x.Value);
            return minValue + values[CageCode.UserPosition.Center];
        }

        //TODO:1時間キャッシュ
        public async Task<IDictionary<CageCode.UserPosition, decimal>> GetPairingValues(Guid userId)
        {
            var model = new Dictionary<CageCode.UserPosition, decimal>();

            var date = DateTime.UtcNow.Date.AddMonths(-3);
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);

            // 過去n月分の消化済みShare
            var usedValues =
                await _userHierarchyMonthlyPairingHistoryService.GetUserHierarchyMonthlyPairingHistoriesByAfterTargetDate(
                    userId, firstDayOfMonth);
            var uhl = _userHierarchyService.GetUserHierarchyByLevel(userId, 1).ToArray();
            // 各ポジションの新規PH、3か月分
            foreach (CageCode.UserPosition value in Enum.GetValues(typeof(CageCode.UserPosition)))
            {
                var downUser = uhl.FirstOrDefault(x => x.Position == (int)value);
                if (downUser == null)
                {
                    model.Add(value, 0);
                    continue;
                }

                var monthly = _userHierarchyMonthlyPairingService.GetUserHierarchyMonthlyPairings(downUser.UserId, firstDayOfMonth).ToArray();
                var monthlyAmount = monthly.Any() ? monthly.Sum(x => x.Amount) : 0;

                // 過去3か月分の消費分を減算
                var usedValueParDate = usedValues.Where(x => x.Position == (int)value);
                foreach (var userHierarchyMonthlyPairingHistory in usedValueParDate)
                {
                    var less = userHierarchyMonthlyPairingHistory.Amount;
                    monthlyAmount -= less;
                }

                model.Add(value, monthlyAmount);
            }

            return model;
        }
    }
}
