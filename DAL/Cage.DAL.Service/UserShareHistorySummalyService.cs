using System;
using Cage.Core;
using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service.Interfaces;
using Cage.DAL.Db.Models;
using Service.Pattern;

namespace Cage.DAL.Service
{
    public class UserShareHistorySummalyService
        : Service<UserShareHistorySummaly>, IUserShareHistorySummalyService
    {
        private readonly ICageRepositoryAsync<UserShareHistorySummaly> _repository;

        public UserShareHistorySummalyService(
            ICageRepositoryAsync<UserShareHistorySummaly> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public int UserShareHistorySummalies(Guid userId, CageCode.ShareCategory shreCategory, decimal amount)
        {
            return _repository.ExecuteQuery("dbo.UserShareHistorySummalies {0}", userId, (int)shreCategory, amount);
        }
    }
}