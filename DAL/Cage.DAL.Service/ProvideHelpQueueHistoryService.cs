using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service.Interfaces;
using Cage.DAL.Db.Models;
using Service.Pattern;

namespace Cage.DAL.Service
{
    public class ProvideHelpQueueHistoryService : Service<ProvideHelpQueueHistory>, IProvideHelpQueueHistoryService
    {
        private readonly ICageRepositoryAsync<ProvideHelpQueueHistory> _repository;

        public ProvideHelpQueueHistoryService(ICageRepositoryAsync<ProvideHelpQueueHistory> repository) : base(repository)
        {
            _repository = repository;
        }

        public void Insert(ProvideHelpQueue queue)
        {
            _repository.Insert(new ProvideHelpQueueHistory
            {
                Id = queue.Id,
                UserProvideHelpId = queue.UserProvideHelpId,
                UserId = queue.UserId,
                ExpierdCount = queue.ExpierdCount,
                Created = queue.Created,
                Updated = queue.Updated
            });
        }
    }
}