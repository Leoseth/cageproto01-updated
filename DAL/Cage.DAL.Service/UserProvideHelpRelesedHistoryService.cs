﻿using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service.Interfaces;
using Cage.DAL.Db.Models;
using Service.Pattern;

namespace Cage.DAL.Service
{
    public class UserProvideHelpRelesedHistoryService : Service<UserProvideHelpRelesedHistory>, IUserProvideHelpRelesedHistoryService
    {
        private readonly ICageRepositoryAsync<UserProvideHelpRelesedHistory> _repository;

        public UserProvideHelpRelesedHistoryService(ICageRepositoryAsync<UserProvideHelpRelesedHistory> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}