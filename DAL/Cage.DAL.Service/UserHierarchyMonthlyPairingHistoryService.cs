using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service.Interfaces;
using Service.Pattern;
using Cage.DAL.Db.Models;

namespace Cage.DAL.Service
{
    public class UserHierarchyMonthlyPairingHistoryService
        : Service<UserHierarchyMonthlyPairingHistory>, IUserHierarchyMonthlyPairingHistoryService
    {
        private readonly ICageRepositoryAsync<UserHierarchyMonthlyPairingHistory> _repository;

        public UserHierarchyMonthlyPairingHistoryService(
            ICageRepositoryAsync<UserHierarchyMonthlyPairingHistory> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<UserHierarchyMonthlyPairingHistory>>
            GetUserHierarchyMonthlyPairingHistoriesByAfterTargetDate(Guid userId, DateTime dateYyyyMm)
        {
            return await _repository.Query(x => x.UserId == userId && x.Date >= dateYyyyMm).SelectAsync();
        }
    }
}