﻿using Cage.DAL.Db;
using Repository.Pattern.Ef6;

namespace Cage.DAL.Repository.Infrastructure
{
    public class CageUnitOfWork : UnitOfWork, ICageUnitOfWorkAsync
    {
        public CageUnitOfWork(IDataContextAsync dataContextAsync)
            : base(dataContextAsync)
        {
        }
    }
}