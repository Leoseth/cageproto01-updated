﻿using Repository.Pattern.Infrastructure;
using Repository.Pattern.Repositories;

namespace Cage.DAL.Repository.Infrastructure
{
    public interface ICageRepositoryAsync<TEntity>
        : IRepositoryAsync<TEntity> where TEntity : class, IObjectState
    {
    }
}