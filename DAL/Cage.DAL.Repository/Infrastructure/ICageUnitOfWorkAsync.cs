﻿using Repository.Pattern.UnitOfWork;

namespace Cage.DAL.Repository.Infrastructure
{
    public interface ICageUnitOfWorkAsync : IUnitOfWorkAsync
    {
    }
}