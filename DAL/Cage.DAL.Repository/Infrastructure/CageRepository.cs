﻿using Cage.DAL.Db;
using Repository.Pattern.Ef6;
using Repository.Pattern.Infrastructure;

namespace Cage.DAL.Repository.Infrastructure
{
    public class CageRepository<TEntity>
        : Repository<TEntity>, ICageRepositoryAsync<TEntity>
        where TEntity : class, IObjectState
    {
        public CageRepository(
            IDataContextAsync context,
            ICageUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {
        }
    }
}