﻿using System;
using System.Linq;
using Cage.DAL.Db.Models;
using Repository.Pattern.Ef6;

namespace Cage.DAL.Repository.Queries
{
    public class ApplicationUserQuery : QueryObject<ApplicationUser>
    {
        public ApplicationUserQuery SelectByRole(Guid roleId)
        {
            And(x => (
                (x.EmailConfirmed) &&
                (x.Roles.Any(r => r.RoleId == roleId))
                ));

            return this;
        }

        public ApplicationUserQuery SelecByName(string name)
        {
            And(x => x.UserName.Equals(name));
            return this;
        }
    }
}