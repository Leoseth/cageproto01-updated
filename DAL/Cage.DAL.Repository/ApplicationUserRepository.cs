﻿using System;
using System.Collections.Generic;
using Cage.DAL.Db.Models;
using Cage.DAL.Db.Models.ViewModels;
using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Repository.Queries;

namespace Cage.DAL.Repository
{
    public static class ApplicationUserRepository
    {
        public static IEnumerable<ApplicationUserViewModel>
            GetUsersInRole(
                this ICageRepositoryAsync<ApplicationUser> repository,
                Guid roleId)
        {
            var query = new ApplicationUserQuery().SelectByRole(roleId);

            return repository
                .Query(query)
                .Select(
                    u =>
                        new ApplicationUserViewModel
                        {
                            UserId = u.Id,
                            Email = u.Email,
                        });
        }
    }
}