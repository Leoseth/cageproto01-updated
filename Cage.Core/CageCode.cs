﻿using System;
using System.Linq;
using System.ComponentModel;
using Abl;
using Identity.Resources;

namespace Cage.Core
{
    public class CageCode
    {
        public const string ReferrerCookieKey = "referre";

        public static readonly int[] UserPositions = Enum.GetValues(typeof(UserPosition)).Cast<int>().ToArray();

        public const string ConsultantCookieKey = "consultant";

        public static readonly int[] EffectiveProvideHelpStatus =
       {
            (int) ProvideHelpStatus.InQueue,
            (int) ProvideHelpStatus.Assigned,
            (int) ProvideHelpStatus.OnHold,
            (int) ProvideHelpStatus.Active
        };

        public static readonly int[] EffectiveGetHelpStatus =
        {
            (int) GetHelpStatus.InQueue,
            (int) GetHelpStatus.Assigned,
            //TODO:Endedを有効に含むか？
            (int) GetHelpStatus.Ended
        };

        public enum RolesEnum
        {
            Subscriber = 10,
            Immigrant = 15,
            Member = 500,
            Administrator = 1000,
        }

        public enum UserType
        {
            [LocalizedDescription("Member", typeof(AccountResources))]
            Member = 0,

            [LocalizedDescription("Administrator", typeof(AccountResources))]
            Administrator = 1,
        }
        public enum PlacementMode
        {
            Auto = 0,
            Manual = 1,
        }
        public enum UserClass
        {
            [LocalizedDescription("Grade_00", typeof(WebResources))]
            Grade_00 = 0,

            [LocalizedDescription("Grade_01", typeof(WebResources))]
            Grade_01 = 1,
        }

        /// <summary>
        /// Provide Helpのヘッダーステータス用
        /// </summary>
        public enum ProvideHelpStatus
        {
            [LocalizedDescription("InQueue", typeof(WebResources))]
            InQueue = 0,
            [LocalizedDescription("Assigned", typeof(WebResources))]
            Assigned = 10,
            [LocalizedDescription("OnHold", typeof(WebResources))]
            OnHold = 20,     //0.5％？のやつで、リリース前
            [LocalizedDescription("Active", typeof(WebResources))]
            Active = 30,
            [LocalizedDescription("Ended", typeof(WebResources))]
            Ended = 40,
            [LocalizedDescription("Canceled", typeof(WebResources))]
            Canceled = 50,
            [LocalizedDescription("Expired", typeof(WebResources))]
            Expired = 60,
        }

        public enum HelpAssignedStatus
        {
            [LocalizedDescription("Pending", typeof(WebResources))]
            Pending = 0,
            [LocalizedDescription("Completed", typeof(WebResources))]
            Completed = 10,
            [LocalizedDescription("Expired", typeof(WebResources))]
            Expired = 20,
            [LocalizedDescription("Canceled", typeof(WebResources))]
            Canceled = 30,
        }

        public static readonly int MaxUserClass = Enum.GetValues(typeof(UserClass)).Cast<int>().Max();

        public enum ReferrerMode
        {
            Default,
            Manual,
        }
        public enum FeeCategory
        {
            Upgrade = 0,
            Passport = 1,
            ProvideHelp = 2,
        }

        public enum BonusCategory
        {
            Passport = 0,
            ProvideHelp = 1,
        }
        public enum AssistanceGivebackPaymentType
        {
            [LocalizedDescription("PA", typeof(WebResources))]
            PA = 0,
            [LocalizedDescription("RGB", typeof(WebResources))]
            RGB = 10,
        }
        public enum PaymentSummaryCategory
        {
            [LocalizedDescription("Passport", typeof(WebResources))]
            Passport = 10,
            [LocalizedDescription("Ph", typeof(WebResources))]
            Ph = 20,
            [LocalizedDescription("Upgrade", typeof(WebResources))]
            // Sent Assistance
            Upgrade = 30,
            [LocalizedDescription("PA", typeof(WebResources))]
            // Received Assistance
            PA = 40,
            [LocalizedDescription("RGB", typeof(WebResources))]
            // Received Giveback
            RGB = 50,
        }
        public enum ShareCategory
        {
            [LocalizedDescription("Release", typeof(WebResources))]
            Release = 0,
            [LocalizedDescription("ProvideHelp", typeof(WebResources))]
            ProvideHelp = 10,
            [LocalizedDescription("PR", typeof(WebResources))]
            PR = 0,
            [LocalizedDescription("PO", typeof(WebResources))]
            PO = 10,
            [LocalizedDescription("PHC", typeof(WebResources))]
            PHC = 20,
            [LocalizedDescription("PHD", typeof(WebResources))]
            PHD = 30,
            [LocalizedDescription("PHR", typeof(WebResources))]
            PHR = 40,
            [LocalizedDescription("PHO", typeof(WebResources))]
            PHO = 50,
            [LocalizedDescription("FP", typeof(WebResources))]
            FP = 60,
        }
        public enum GetHelpStatus
        {
            InQueue = 0,
            Assigned = 10,
            Ended = 20,
            Canceled = 50,
        }
        public enum UserPosition
        {
            [LocalizedDescription("Left", typeof(AccountResources))]
            Left = 0,
            [LocalizedDescription("Center", typeof(AccountResources))]
            Center = 1,
            [LocalizedDescription("Right", typeof(AccountResources))]
            Right = 2
        }
        public enum PassportTransactionCategory
        {
            Free = 0,
            Purchased = 10,
            PA = 20,        // Provide Assistance
            PH = 30,
            GH = 40,
            Upgrade = 50,
        }

        //TODO: config
        public const int ProvideHelpMaturityDays = 20;

    }
}