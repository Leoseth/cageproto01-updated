﻿using Abl;

namespace Cage.Core
{
    public class CageSettings : AppSettingsWrapper
    {
        //<!-- Site settings -->
        public static readonly string SiteName = GetSetting("SiteName");

        public static readonly string SiteDomain = GetSetting("SiteDomain");
        public static readonly string SiteDesc = GetSetting("SiteDesc");

        //<!-- Identity settings -->
        public static readonly string LoginPath = GetSetting("LoginPath");

        public static readonly string LoggedInControllerName = GetSetting("LoggedInControllerName");
        public static readonly string LoggedInActionName = GetSetting("LoggedInActionName");
        public static readonly string RootUser = GetSetting("RootUser");
        public static readonly string RootUserEmail = GetSetting("RootUserEmail");
        public static readonly string RootUserPasswordHash = GetSetting("RootUserPasswordHash");
        public static readonly string RootUsersBitcoinAddress = GetSetting("RootUsersBitcoinAddress");

        //<!-- Bitcoin settings-->
        public static readonly string BlockchainInfoUrl = GetSetting("BlockchainInfoUrl");
        public static readonly string BlockchainCheckAddressUrl = BlockchainInfoUrl + "address/{0}";
        public static readonly string BlockchainCheckTxUrl = BlockchainInfoUrl + "tx/{0}";
        public static readonly string BitcoinPrivateKey = GetSetting("BitcoinPrivateKey");
        public static readonly string BitcoinPassword = GetSetting("BitcoinPassword");
        public static readonly string BitcoinExtPubKey = GetSetting("BitcoinExtPubKey");

        //<!-- Payment Settings -->
        public static readonly int PaymentTimeoutMin = GetSettingAsInt("PaymentTimeoutMin");
        public static readonly decimal PassportPrice = GetSettingAsDecimal("PassportPrice");
        public static readonly decimal[] UpgradeFees = GetSettingAsDecimal("UpgradeFees", ';');

        //<!-- Service Settings -->
        public static readonly decimal PledgePhInterestRates = GetSettingAsDecimal("PledgePhInterestRates");
        public static readonly decimal ActivePhInterestRates = GetSettingAsDecimal("ActivePhInterestRates");
        public static readonly int EffectiveGhTimeSpanDays = GetSettingAsInt("EffectiveGhTimeSpanDays");
        public static readonly decimal ProvideHelpLimit = GetSettingAsDecimal("ProvideHelpLimit");
        public static readonly decimal GetHelpLimit = GetSettingAsDecimal("GetHelpLimit");
        public static readonly decimal[] ProvideHelpValues = GetSettingAsDecimal("ProvideHelpValues", ';');
        public static readonly decimal[] GetHelpValues = GetSettingAsDecimal("GetHelpValues", ';');

        //<!-- Bonus Settings -->
        public static readonly decimal[] PassportBonus = GetSettingAsDecimal("PassportBonus", ';');

        //<!-- Batch Settings-->
        /// <summary>
        /// マッチングでHelp登録からキューでマッチングするまでの日数(n日遅延させる)
        /// </summary>
        public static readonly int MatchingDelayDayForPh = GetSettingAsInt("MatchingDelayDayForPh");
        public static readonly int MatchingDelayDayForGh = GetSettingAsInt("MatchingDelayDayForGh");

        //<!-- DEBUG -->
        public static readonly string[] DebugUserEmails = GetSetting("DebugUserEmails", ';');

        //<!-- Content settings -->

        //<!-- API settings -->
        public static readonly string WebsiteThumbnailApiUrl = GetSetting("WebsiteThumbnailApiUrl");

        //<!-- DEBUG -->
        public static readonly bool DebugMode = GetSettingAsBool("DebugMode");
    }
}