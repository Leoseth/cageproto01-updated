﻿using System;
using System.Runtime.Caching;
using System.Threading.Tasks;
using Abl;

namespace Cage.Core.Cache
{
    //public interface ICacheClient
    //{
    //    T Get<T>(string key);
    //    bool Add<T>(string key, T value, TimeSpan expire);
    //    bool Set<T>(string key, T value, TimeSpan expire);
    //    bool Remove(string key);
    //    long Increment(string key, uint amount);
    //}

    public static class CacheManager
    {
        public static T AddOrGetExisting<T>(string key, Func<T> valueFactory, TimeSpan validFor)
        {
            ObjectCache cache = MemoryCache.Default;
            var value = cache.AddOrGetExisting<T>(key,
                valueFactory,
                new CacheItemPolicy
                {
                    AbsoluteExpiration = DateTimeOffset.Now.Add(-validFor),
                });
            return value;
        }

        public static Task<T> AddOrGetExistingAsync<T>(string key, Func<Task<T>> valueFactory, TimeSpan validFor)
        {
            ObjectCache cache = MemoryCache.Default;
            var value = cache.AddOrGetExistingAsync<T>(key,
                valueFactory,
                new CacheItemPolicy
                {
                    AbsoluteExpiration = DateTimeOffset.Now.Add(-validFor),
                });
            return value;
        }
    }
}
