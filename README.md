### How do I get set up? ###
 
#### initial setting
 

1. create CageTask catalog on MSSQL(default localhost).
 
2. F5 Run project on Visual Studio 2015, 2017

#### Add table with classes
1.

         Open generate.tt to change {className} to save.
src\Template\Generate.tt
2. 
         Modify fields on Cage.DAL.Db.Models.{className}


3. Mofify configurations Cage.DAL.Db.Configurations.{className}Map

4. Modify service Cage.DAL.Service.{className}Service

5. Add to DataContext on CageDataContext

6. Add to Autofac container on Startup.ConfigureContainer.cs