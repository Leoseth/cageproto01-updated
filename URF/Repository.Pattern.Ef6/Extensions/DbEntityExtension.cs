﻿using System.Data.Entity.Infrastructure;
using System.Linq;

namespace Repository.Pattern.Ef6.Extensions
{
    internal static class DbEntityExtension
    {
        public static DbPropertyEntry SafeGetProperty(this DbEntityEntry entry, string propertyName)
        {
            if (entry.CurrentValues.PropertyNames.Contains(propertyName))
            {
                return entry.Property(propertyName);
            }

            return null;
        }
    }
}
