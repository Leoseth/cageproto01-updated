﻿using Cage.Core;
using Cage.DAL.Db.Models.ViewModels.Member;
using Cage.Web.Core;
using System.Web.Mvc;

namespace Cage.Web.Areas.member.Controllers
{
    [AuthorizeRole(CageCode.RolesEnum.Member)]
    public class MemberController : BaseMemberController
    {
        // GET: member/Member
        public ActionResult Index()
        {
            var model = new LayoutMemberViewModel(ApplicationUser);
            //TODO:dashboard here
            return View(model);
        }
    }
}