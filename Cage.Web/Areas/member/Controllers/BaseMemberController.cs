﻿using Cage.DAL.Db.Models;
using Cage.DAL.Db.Models.ViewModels.Member;
using Cage.DAL.Repository.Infrastructure;
using Cage.Web.Controllers.Base;
using Microsoft.AspNet.Identity;

namespace Cage.Web.Areas.member.Controllers
{
    public class BaseMemberController : BaseAttributesController
    {
        protected LayoutMemberViewModel LayoutMemberViewModel { get; private set; }
        protected ApplicationUser ReffererUser => UserManager.FindById(ApplicationUserDetails.ReferrerUserId);

        public BaseMemberController()
        {
        }

        public BaseMemberController(
            ICageUnitOfWorkAsync unitOfWork
            ) : base(unitOfWork)
        {
        }
    }
}