﻿
namespace Cage.Web.Models.RoleManager
{
    public class DeleteRoleViewModel
    {
        public string Role { get; set; }
    }
}