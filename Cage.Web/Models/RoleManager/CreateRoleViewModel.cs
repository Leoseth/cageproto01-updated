﻿
namespace Cage.Web.Models.RoleManager
{
    public class CreateRoleViewModel
    {
        public string Role { get; set; }
    }
}