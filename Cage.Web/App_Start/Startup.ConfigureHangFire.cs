﻿using System.Configuration;
using Autofac;
using Cage.Web.Hangfire;
using Hangfire;
using Owin;

namespace Cage.Web
{
    public partial class Startup
    {
        private static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["HangFire"].ConnectionString;
        //private static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["WebApp"].ConnectionString;

        public static void ConfigureHangfire(IAppBuilder app)
        {
            IContainer container = CreateContainerForHangfire();
            var config = GlobalConfiguration.Configuration;
            config.UseActivator(new ContainerJobActivator(container));
            config.UseSqlServerStorage(ConnectionString);

            app.UseHangfireDashboard();
            app.UseHangfireServer();

            // Cron job
        }

        private static IContainer CreateContainerForHangfire()
        {
            ContainerBuilder builder = new ContainerBuilder();

            RegisterDALs(builder);
            RegisterBackgroundTasks(builder);

            IContainer container = builder.Build();
            return container;
        }


        private static void RegisterBackgroundTasks(ContainerBuilder builder)
        {
            //TODO:後で消す
            builder.RegisterType<Test>().InstancePerDependency();
        }
    }
}
