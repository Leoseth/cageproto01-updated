﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Cage.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.LowercaseUrls = true;

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            //routes.MapRoute(
            //   name: "index",
            //   url: "",
            //   defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //);

            //routes.MapRoute(
            //   name: "search",
            //   url: "search",
            //   defaults: new { controller = "Home", action = "Search", searchTxt = UrlParameter.Optional }
            //);

            routes.MapRoute(
               name: "about",
               url: "about",
               defaults: new { controller = "Home", action = "About" }
            );

            routes.MapRoute(
               name: "contact",
               url: "contact",
               defaults: new { controller = "Home", action = "Contact" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults:new{controller = "Home", action = "Index", id = UrlParameter.Optional}
            );

        }
    }
}