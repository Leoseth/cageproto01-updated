﻿
using System;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
//using Autofac.Integration.WebApi;
using Cage.DAL.Db;
using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service;
using Cage.DAL.Service.Interfaces;
using Cage.DAL.Service.Manager;
using Cage.DAL.Db.Models;
//using Cage.DAL.Db.Models.Masters;
using Identity;
using Microsoft.AspNet.Identity;
using Owin;


namespace Cage.Web
{
    /// <summary>
    /// Register types into the Autofac Inversion of Control (IOC) container. Autofac makes it easy to register common
    /// MVC types like the <see cref="UrlHelper"/> using the <see cref="AutofacWebTypesModule"/>. Feel free to change
    /// this to another IoC container of your choice but ensure that common MVC types like <see cref="UrlHelper"/> are
    /// registered. See http://autofac.readthedocs.org/en/latest/integration/aspnet.html.
    /// </summary>
    public partial class Startup
    {
        public static void ConfigureContainer(IAppBuilder app)
        {
            IContainer container = CreateContainer();
            app.UseAutofacMiddleware(container);

            // Register MVC Types
            app.UseAutofacMvc();
        }

        private static IContainer CreateContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();
            Assembly assembly = Assembly.GetExecutingAssembly();

            //builder.RegisterApiControllers(assembly);

            RegisterServices(builder);
            RegisterDALs(builder);
            RegisterMvc(builder, assembly);

            IContainer container = builder.Build();

            SetWebApiDependencyResolver(container);
            SetMvcDependencyResolver(container);

            return container;
        }

        /// <summary>
        /// Sets the ASP.NET WebApi dependency resolver.
        /// </summary>
        /// <param name="container">The container.</param>
        private static void SetWebApiDependencyResolver(IContainer container)
        {
            HttpConfiguration config = GlobalConfiguration.Configuration;
            //config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        /// <summary>
        /// Sets the ASP.NET MVC dependency resolver.
        /// </summary>
        /// <param name="container">The container.</param>
        private static void SetMvcDependencyResolver(IContainer container)
        {
            DependencyResolver.SetResolver(
                new AutofacDependencyResolver(container));
        }

        private static void RegisterServices(ContainerBuilder builder)
        {
            // Provide injection support for HttpContext
            // See: http://stackoverflow.com/a/15639347
            builder.RegisterModule(new AutofacWebTypesModule());
        }

        private static void RegisterMvc(
            ContainerBuilder builder,
            Assembly assembly)
        {
            // Register Common MVC Types
            builder.RegisterModule<AutofacWebTypesModule>();

            // Register MVC Filters
            builder.RegisterFilterProvider();

            // Register MVC Controllers
            builder.RegisterControllers(assembly);
        }

        private static void RegisterDALs(ContainerBuilder builder)
        {
            // Data Context
            builder.RegisterType<CageDataContext>()
                .As<IDataContextAsync>()
                .InstancePerRequest()
                .InstancePerLifetimeScope();

            // Unit of Work
            builder.RegisterType<CageUnitOfWork>()
                .As<ICageUnitOfWorkAsync>()
                .InstancePerRequest()
                .InstancePerLifetimeScope();

            RegisterIdentity(builder);

            RegisterApplicationData(builder);
            RegisterManager(builder);
        }

        private static void RegisterIdentity(ContainerBuilder builder)
        {
            // Wire up the IdentityRoleManager
            builder.RegisterType<ApplicationRoleStore>()
                .As<IRoleStore<ApplicationRole, Guid>>()
                .InstancePerRequest();

            builder.RegisterType<ApplicationRoleManager>()
                .InstancePerRequest();

            // Repositories
            builder.RegisterType<CageRepository<ApplicationUser>>()
                .As<ICageRepositoryAsync<ApplicationUser>>()
                .InstancePerRequest();

            builder.RegisterType<CageRepository<ApplicationRole>>()
                .As<ICageRepositoryAsync<ApplicationRole>>()
                .InstancePerRequest();

            // Services
            builder.RegisterType<ApplicationUserService>()
                .As<IApplicationUserService>()
                .InstancePerRequest();

            builder.RegisterType<ApplicationRoleService>()
                .As<IApplicationRoleService>()
                .InstancePerRequest();

            // Manager
            builder.RegisterType<ApplicationUserStore>().As<IUserStore<ApplicationUser, Guid>>().InstancePerDependency();
            builder.RegisterType<ApplicationUserManager>().InstancePerDependency();
            builder.RegisterType<UserManager<ApplicationUser, Guid>>().InstancePerDependency();
        }

        private static void RegisterManager(ContainerBuilder builder)
        {
            // added by Leonard //
            builder.RegisterType<ProvideHelpManager>()
            .InstancePerRequest()
            .InstancePerDependency();

            builder.RegisterType<UserHierarchyMonthlyPairingManager>()
                .InstancePerRequest()
                .InstancePerDependency();
            // added by Leonard //
        }

        private static void RegisterApplicationData(ContainerBuilder builder)
        {
            // Add additional repository and service registrations here

            // Repositories

            // added by Leonard //
            builder.RegisterType<CageRepository<ApplicationUserDetails>>()
                .As<ICageRepositoryAsync<ApplicationUserDetails>>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<CageRepository<UserHierarchy>>()
                .As<ICageRepositoryAsync<UserHierarchy>>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<CageRepository<UserHierarchyMonthlyPairing>>()
                .As<ICageRepositoryAsync<UserHierarchyMonthlyPairing>>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<CageRepository<UserHierarchyMonthlyPairingHistory>>()
                .As<ICageRepositoryAsync<UserHierarchyMonthlyPairingHistory>>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<CageRepository<UserHierarchySummalyView>>()
                .As<ICageRepositoryAsync<UserHierarchySummalyView>>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<CageRepository<ProvideHelpQueue>>()
                .As<ICageRepositoryAsync<ProvideHelpQueue>>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<CageRepository<ProvideHelpQueueHistory>>()
                .As<ICageRepositoryAsync<ProvideHelpQueueHistory>>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<CageRepository<UserHelpAssigned>>()
                .As<ICageRepositoryAsync<UserHelpAssigned>>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<CageRepository<UserPaymentSummaly>>()
                .As<ICageRepositoryAsync<UserPaymentSummaly>>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<CageRepository<UserProvideHelp>>()
                .As<ICageRepositoryAsync<UserProvideHelp>>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<CageRepository<UserProvideHelpMonthlyHistory>>()
                .As<ICageRepositoryAsync<UserProvideHelpMonthlyHistory>>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<CageRepository<UserProvideHelpRelesedHistory>>()
                .As<ICageRepositoryAsync<UserProvideHelpRelesedHistory>>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<CageRepository<UserShareHistorySummaly>>()
                .As<ICageRepositoryAsync<UserShareHistorySummaly>>()
                .InstancePerRequest()
                .InstancePerDependency();
            //added by Leonard //

            builder.RegisterType<CageRepository<UserUnilevel>>()
                .As<ICageRepositoryAsync<UserUnilevel>>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<CageRepository<File>>()
                .As<ICageRepositoryAsync<File>>()
                .InstancePerRequest()
                .InstancePerDependency();

            
            // Services

            // added by Leonard //
            builder.RegisterType<ApplicationUserDetailService>()
                .As<IApplicationUserDetailService>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<UserHierarchyService>()
                .As<IUserHierarchyService>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<UserHierarchyMonthlyPairingService>()
                .As<IUserHierarchyMonthlyPairingService>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<UserHierarchyMonthlyPairingHistoryService>()
                .As<IUserHierarchyMonthlyPairingHistoryService>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<UserHierarchySummalyViewService>()
                .As<IUserHierarchySummalyViewService>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<ProvideHelpQueueService>()
                .As<IProvideHelpQueueService>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<ProvideHelpQueueHistoryService>()
                .As<IProvideHelpQueueHistoryService>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<UserHelpAssignedService>()
                .As<IUserHelpAssignedService>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<UserPaymentSummalyService>()
                .As<IUserPaymentSummalyService>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<UserProvideHelpService>()
                .As<IUserProvideHelpService>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<UserProvideHelpMonthlyHistoryService>()
                .As<IUserProvideHelpMonthlyHistoryService>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<UserProvideHelpRelesedHistoryService>()
                .As<IUserProvideHelpRelesedHistoryService>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<UserShareHistoryService>()
                .As<IUserShareHistoryService>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<UserShareHistorySummalyService>()
                .As<IUserShareHistorySummalyService>()
                .InstancePerRequest()
                .InstancePerDependency();
            // added by Leonard //

            builder.RegisterType<UserUnilevelService>()
                .As<IUserUnilevelService>()
                .InstancePerRequest()
                .InstancePerDependency();

            builder.RegisterType<FileService>()
                .As<IFileService>()
                .InstancePerRequest()
                .InstancePerDependency();

            // Service(View)
        }
    }
}