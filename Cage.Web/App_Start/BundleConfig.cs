﻿using System.Web.Optimization;

namespace Cage.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // 開発と学習には、Modernizr の開発バージョンを使用します。次に、実稼働の準備が
            // できたら、http://modernizr.com にあるビルド ツールを使用して、必要なテストのみを選択します。
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/moment").Include("~/Scripts/moment-{version}.js"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/jquery.css").Include(
                      "~/Content/query-ui.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/magnific-popup.css",
                      "~/Content/site.css"));

            /* Lib */
            bundles.Add(new StyleBundle("~/Content/font-awesome").Include("~/Content/font-awesome.css"));

            /* Lib(original) */
            bundles.Add(new ScriptBundle("~/utils/js").Include("~/Scripts/site/utils.js"));

            /* Global */
            bundles.Add(new ScriptBundle("~/site.global/js").Include("~/Scripts/site/global.js"));

            /* Home */
            bundles.Add(new ScriptBundle("~/bundles/fancybox").Include(
                "~/Scripts/jquery.fancybox.js",
                "~/Scripts/jquery.fancybox-media.js"));
            bundles.Add(new ScriptBundle("~/jquery.flexslider/js").Include("~/Scripts/jquery.flexslider-{version}.js"));
            bundles.Add(new ScriptBundle("~/owl.carousel/js").Include("~/Scripts/owl.carousel-{version}.js"));
            bundles.Add(new ScriptBundle("~/site.home/js").Include("~/Scripts/site/home.js"));
            bundles.Add(new ScriptBundle("~/site.register/js").Include("~/Scripts/site/register.js"));

            bundles.Add(new StyleBundle("~/jquery.fancybox/css").Include("~/Content/jquery.fancybox-{version}.css"));
            bundles.Add(new StyleBundle("~/jquery.flexslider/css").Include("~/Content/flexslider-{version}.css"));
            bundles.Add(new StyleBundle("~/owl.carousel/css").Include("~/Content/owl.carousel-{version}.css"));
            bundles.Add(new StyleBundle("~/site.home/css").Include("~/Content/site.home.css"));

            /* Member */
            bundles.Add(new StyleBundle("~/site.member/css").Include(
                "~/Content/site.member.css",
                "~/Content/site.member.custom.css"));
        }
    }
}