﻿using System;
using Autofac;
using Hangfire;

namespace Cage.Web
{
    public class ContainerJobActivator : JobActivator
    {
        private readonly IContainer _container;

        [ThreadStatic]
        private static string _jobId;
        public static string JobId { get { return _jobId; } set { _jobId = value; } }

        public ContainerJobActivator(IContainer container)
        {
            _container = container;
        }

        public override object ActivateJob(Type type)
        {
            return _container.Resolve(type);
        }

        public void OnPerforming(BackgroundJob job)
        {
            JobId = job.Id;
        }
    }
}