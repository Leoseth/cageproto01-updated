﻿$(function () {
    //alert("this is for register");
    //console.log("this is for register");
    var emailS = $("[name=Email]");
    var usernameS = $("#username");
    var referrerS = $("#referrer");
    var consultantS = $("#consultant");
    var usericonS = $("#usericon");
    var reficonS = $("#referrericon");
    var consultanticonS = $("#consultanticon");

    $(document).ready(function () {
        checkUsername(usernameS, usericonS, true);
        checkUsername(referrerS, reficonS, true);
        checkUsername(consultantS, consultanticonS, true);
    });

    function checkUsername(nameS, iconS, exist) {
        var alias = nameS.val();
        if (alias === "") {
            iconS.html('<i class="fa fa-remove text-danger"></i>');
            return;
        }

        var loadUrl = "/account/alias?name=" + fixedEncodeURIComponent(alias);
        //alert(fixedEncodeURIComponent(alias));
        //alert(loadUrl);
        //console.log(fixedEncodeURIComponent(alias));
        //console.log(loadUrl);
        $.ajax({
            url: loadUrl,
            success: function (result) {
                if ((result.alias === alias) === exist) {
                    iconS.html('<i class="fa fa-check text-success"></i>');
                } else {
                    iconS.html('<i class="fa fa-remove text-danger"></i>');
                }
            }, error: function (result) {
                iconS.html('<i class="fa fa-remove text-danger"></i>');
            }
        });
    }

    emailS.keyup(function () {
        var username = getUsername();
        usernameS.val(username);
        checkUsername(usernameS, usericonS, false);
    });

    emailS.change(function () {
        var username = getUsername();
        usernameS.val(username);
        checkUsername(usernameS, usericonS, false);
    });

    usernameS.keyup(function () {
        checkUsername(usernameS, usericonS, false);
    });

    usernameS.change(function () {
        checkUsername(usernameS, usericonS), false;
    });

    function getUsername() {
        var un = emailS.val();
        var index = un.indexOf("@");
        if (index > 0)
            return un.slice(0, index);
        return un;
    }
});