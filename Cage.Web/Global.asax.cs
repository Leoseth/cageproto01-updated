﻿using System;
using System.Data.Entity.Validation;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Abl.Mvc.Logger;
using NLog;

using System.Collections.Generic;

namespace Cage.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected static readonly NLog.ILogger Logger =
            LogManager.GetLogger(
                Assembly.GetExecutingAssembly().FullName,
                MethodBase.GetCurrentMethod().DeclaringType);

        protected void Application_Start()
        {
            ConfigureViewEngines();
            ConfigureAntiForgeryTokens();
        }

        protected void Application_BeginRequest()
        {
            Response.Headers.Remove("Server");
            Response.Headers.Remove("X-AspNet-Version");
            Response.Headers.Remove("X-AspNetMvc-Version");
        }

        protected void Application_PostAuthenticateRequest()
        {
            //if (User.IsInRole(Enum.GetName(typeof (AntCode.RolesEnum), AntCode.RolesEnum.Immigrant)))
            //{
            //}
        }

        /// <summary>
        /// Configures the view engines. By default, Asp.Net MVC includes the Web Forms (WebFormsViewEngine) and 
        /// Razor (RazorViewEngine) view engines. You can remove view engines you are not using here for better
        /// performance.
        /// </summary>
        private static void ConfigureViewEngines()
        {
            // Only use the RazorViewEngine.
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());
        }


        /// <summary>
        /// Configures the anti-forgery tokens. See 
        /// http://www.asp.net/mvc/overview/security/xsrfcsrf-prevention-in-aspnet-mvc-and-web-pages
        /// </summary>
        private static void ConfigureAntiForgeryTokens()
        {
            // Rename the Anti-Forgery cookie from "__RequestVerificationToken" to "f". This adds a little security 
            // through obscurity and also saves sending a few characters over the wire. Sadly there is no way to change 
            // the form input name which is hard coded in the @Html.AntiForgeryToken helper and the 
            // ValidationAntiforgeryTokenAttribute to  __RequestVerificationToken.
            // <input name="__RequestVerificationToken" type="hidden" value="..." />
            AntiForgeryConfig.CookieName = "f";

            //TODO:support SSL
            // If you have enabled SSL. Uncomment this line to ensure that the Anti-Forgery 
            // cookie requires SSL to be sent across the wire. 
            // AntiForgeryConfig.RequireSsl = true;
        }

        protected void Application_Error()
        {
            var ex = Server.GetLastError();           
            
            var httpEx = ex as HttpException;
            if (httpEx != null)
            {
                if(httpEx.GetHttpCode() == (int)HttpStatusCode.NotFound) { 
                    // ignore
                    return;
                }

                //if (ex is HttpUnhandledException)
                //{
                //    System.Diagnostics.Trace.WriteLine(ex.Message);    // VisualStudioの出力に表示
                //}
            }
            else if (ex is DbEntityValidationException)
            {
                var dbevex = ex as DbEntityValidationException;
                foreach (var errors in dbevex.EntityValidationErrors)
                {
                    foreach (var error in errors.ValidationErrors)
                    {
                        System.Diagnostics.Trace.WriteLine(error.ErrorMessage);    // VisualStudioの出力に表示
                    }
                }
            }

            //
            // 例外情報と内部例外情報をログとして出力する処理など
            try
            {
                var request = new HttpRequestWrapper(Request);
                Logger.Error(ex, LogEntry.GetLogEntry(ex.Message, request));
            }
            catch (Exception logEx)
            {
                Logger.Error(logEx, "Logging Error.");
            }                        
                // 例外をクリア（カスタムエラーページを設定している場合はコメントアウトすること）
                //TODO:custom error page
                //Server.ClearError();
            }
    }
}