﻿using System.Threading.Tasks;
using System.Web.Mvc;

namespace Cage.Web.Controllers
{
    [Authorize]
    public class FileController : Controller
    {
        private readonly IFileService _fileService;

        public FileController(IFileService fileService)
        {
            _fileService = fileService;
        }

        // GET: File
        public async Task<ActionResult> Index(int id)
        {
            var fileToRetrieve = await _fileService.FindAsync(id);
            return File(fileToRetrieve.Content, fileToRetrieve.ContentType);
        }
    }
}