﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Cage.Core;
using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service.Interfaces;
using Cage.DAL.Service.Manager;
using Cage.DAL.Db.Models;
using Cage.DAL.Db.Models.ViewModels.Member;

using System.Data.Entity.Validation;
using System.Collections.Generic;

namespace Cage.Web.Controllers.Member
{

    //public class DashboardController : Areas.member.Controllers.BaseMemberController
    public class DashboardController : Areas.member.Controllers.BaseMemberController
    {
        private readonly IUserHierarchySummalyViewService _userHierarchySummalyViewService;
        private readonly IUserPaymentSummalyService _userPaymentSummalyService;
        private readonly IUserShareHistorySummalyService _userShareHistorySummalyService;

        private readonly ProvideHelpManager _provideHelpManager;
        private readonly UserHierarchyMonthlyPairingManager _userHierarchyMonthlyPairingManager;

        public DashboardController(
            ICageUnitOfWorkAsync unitOfWork,
            IUserHierarchySummalyViewService userHierarchySummalyViewService,
            IUserPaymentSummalyService userPaymentSummalyService,
            IUserShareHistorySummalyService userShareHistorySummalyService,
            ProvideHelpManager provideHelpManager,
            UserHierarchyMonthlyPairingManager userHierarchyMonthlyPairingManager
            ) : base(unitOfWork)
        {
            _userHierarchySummalyViewService = userHierarchySummalyViewService;
            _userPaymentSummalyService = userPaymentSummalyService;
            _userShareHistorySummalyService = userShareHistorySummalyService;
            _provideHelpManager = provideHelpManager;
            _userHierarchyMonthlyPairingManager = userHierarchyMonthlyPairingManager;
        }

        // GET: Dashboard
        public async Task<ActionResult> Index()
        {
            var ups = await _userPaymentSummalyService.Queryable().Where(x => x.UserId == UserIdGuid).ToArrayAsync();

            var model = new DashboardViewModel(ApplicationUser)
            {
                ReffererUser = ReffererUser,
                AssistanceGivebackSummaryView = await GetAssistanceGivebackSummaly(ups),
                UserShareViewModel = await GetUserShareAsync(),
                PairingViewModel = new DashboardPairingViewModel
                {
                    CurrentPairing = await _userHierarchyMonthlyPairingManager.GetNextPairingValue(UserIdGuid)
                }
            };

            if (model.UserDetails.HasPledgePh)
                model.PhViewModel = await GetProvideHelpAsync();

            model.MembersSlotViewModel = new DashboardMembersSlotViewModel
            {
                User = model.User,
                UserDetails = model.UserDetails
            };

            //TODO:UserClass毎のハンドリング
            return View(model);
        }

        private async Task<DashboardAssistanceGivebackSummalyViewModel> GetAssistanceGivebackSummaly(UserPaymentSummaly[] userPaymentSummalies)
        {
            var sentAssistance = userPaymentSummalies.Where(x => x.Category == (int)CageCode.PaymentSummaryCategory.Upgrade);

            var pa = userPaymentSummalies
                .Where(x => x.UserId == UserIdGuid && x.Category == (int)CageCode.PaymentSummaryCategory.PA);
            var rgb = userPaymentSummalies.Where(x => x.Category == (int)CageCode.AssistanceGivebackPaymentType.RGB);

            var model = new DashboardAssistanceGivebackSummalyViewModel
            {
                SentAssistance = sentAssistance.Sum(x => x.Amount),
                ReceivedAssistance = pa.Any() ? pa.Sum(x => x.Amount) : 0,
                ReceivedGiveback = rgb.Any() ? rgb.Sum(x => x.Amount) : 0,
                UserHierarchySummalyViews = await _userHierarchySummalyViewService.Queryable().Where(x => x.UserId == UserIdGuid).ToListAsync()
            };
            return model;
        }

        private async Task<DashboardPhViewModel> GetProvideHelpAsync()
        {
            var effectiveUserProvideHelps = await _provideHelpManager.UserProvideHelpService.GetEffectiveUserProvideHelps(UserIdGuid);
            var userProvideHelps = effectiveUserProvideHelps as UserProvideHelp[] ?? effectiveUserProvideHelps.ToArray();
            if (!userProvideHelps.Any())
                return null;

            var totalPh =
                _provideHelpManager.UserProvideHelpService.GetEffectiveProvideHelpAmounts(userProvideHelps);
            var totalDivinded = userProvideHelps.Sum(x => x.Divinded);
            return new DashboardPhViewModel
            {
                TotalPh = totalPh,
                TotalDividend = totalDivinded
            };
        }

        private async Task<DashboardUserShareViewModel> GetUserShareAsync()
        {
            var model = new DashboardUserShareViewModel();
            var data = await _userShareHistorySummalyService.Queryable().Where(x => x.UserId == UserIdGuid).ToArrayAsync();
            if (!data.Any())
                return model;

            var pr = data.FirstOrDefault(x => x.Category == (int)CageCode.ShareCategory.PR);
            if (pr != null)
                model.PassportReferral = pr.Amount;

            var po = data.FirstOrDefault(x => x.Category == (int)CageCode.ShareCategory.PO);
            if (po != null)
                model.PassportOveriding = po.Amount;

            var phc = data.FirstOrDefault(x => x.Category == (int)CageCode.ShareCategory.PHC);
            if (phc != null)
                model.PhCapital = phc.Amount;

            var phd = data.FirstOrDefault(x => x.Category == (int)CageCode.ShareCategory.PHD);
            if (phd != null)
                model.PhDividend = phd.Amount;

            var phr = data.FirstOrDefault(x => x.Category == (int)CageCode.ShareCategory.PHR);
            if (phr != null)
                model.PhReferral = phr.Amount;

            var pho = data.FirstOrDefault(x => x.Category == (int)CageCode.ShareCategory.PHO);
            if (pho != null)
                model.PhOveriding = pho.Amount;

            var fp = data.FirstOrDefault(x => x.Category == (int)CageCode.ShareCategory.FP);
            if (fp != null)
                model.FlexPairing = fp.Amount;

            return model;
        }
    }
}