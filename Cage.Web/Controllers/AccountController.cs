﻿using Cage.Core;
using Cage.DAL.Db.Models;
using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service.Interfaces;
using Cage.Web.Controllers.Base;
using Cage.Web.Core;
//using Cage.Models;
using Identity;
using Identity.Resources;
using Identity.UI.ViewModels.Account;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Repository.Pattern.Infrastructure;
using System;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Cage.Web.Areas.Admin.Controllers
{
    // See
    // http://www.asp.net/mvc/overview/security/create-an-aspnet-mvc-5-web-app-with-email-confirmation-and-password-reset
    // For additional setup/configuration options

    public class AccountController : BaseIdentityController
    {
        private readonly IApplicationUserDetailService _userDetailService;

        #region CTORs
        public AccountController(ICageUnitOfWorkAsync unitOfWork) : base(unitOfWork)
        {          
        }

        //added by Leonard on July 07, 2017//
        public AccountController() {
        }
        //added by leonard on July 07, 2017//

        public AccountController(
            ICageUnitOfWorkAsync unitOfWork,
            ApplicationSignInManager signInManager)
            : base(unitOfWork, signInManager)
        {
        }

        public AccountController(
            ApplicationUserManager userManager,
            ApplicationSignInManager signInManager,
            IApplicationUserDetailService auds)
            : base(userManager, signInManager)
        {
            this._userDetailService = auds;

        }

        #endregion CTORs

        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(
            LoginViewModel model,
            string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // Require the user to have a confirmed email before they can log on.
            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                ModelState.AddModelError("", AccountResources.LoginCtrl_InvalidLoginAttempt);
                return View(model);
            }

            if (!await UserManager.IsEmailConfirmedAsync(user.Id))
            {
                return View(
                    "ConfirmEmailBeforeLogin",
                    new ResendEmailConfirmationViewModel
                    {
                        Email = user.Email
                    });
            }

            // This doesn't count login failures towards account lockout To enable password failures
            // to trigger account lockout, change to shouldLockout: true
            var result =
                await
                    SignInManager.PasswordSignInAsync(
                        user.UserName, model.Password, model.RememberMe,
                        shouldLockout: false);

            switch (result)
            {
                case SignInStatus.Success:
                    var lastLogin = await SetLastLoginDateTime(user);
                    return RedirectToLocal(returnUrl);

                case SignInStatus.LockedOut:
                    return View("Lockout");

                case SignInStatus.RequiresVerification:
                    var token = await SetLastGeneratedTokenAsync(user);
                    return RedirectToAction(
                        "SendVerificationMessage",
                        new
                        {
                            Token = token,
                            ReturnUrl = returnUrl,
                            RememberMe = model.RememberMe,
                        });

                default:
                    ModelState.AddModelError(
                        "", AccountResources.LoginCtrl_InvalidLoginAttempt);
                    return View(model);
            }
        }

        // POST: /Account/ResendEmailConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResendEmailConfirmation(
            ResendEmailConfirmationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Error");
            }

            // Require the user to have a confirmed email before they can log on.
            var user = await UserManager.FindByEmailAsync(model.Email);

            if (user != null)
            {
                // Send an email with this link
                await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);

                await
                    SendEmailConfirmationTokenAsync(user);

                return View(
                    "ConfirmationEmailSent",
                    new ConfirmationEmailSentViewModel
                    {
                        Email = user.Email
                    });
            }

            ViewBag.ErrorMessage =
                AccountResources.UnableToResendEmailConfirmation;
            return View("Error");
        }

        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(
            string provider,
            string returnUrl,
            bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return
                View(
                    new VerifyCodeViewModel
                    {
                        Provider = provider,
                        ReturnUrl = returnUrl,
                        RememberMe = rememberMe
                    });
        }

        /// <summary>
        /// Verify from url link that clicked by user.
        /// </summary>
        /// <param name="email">The user email</param>
        /// <param name="token">Then generated token</param>
        /// <param name="rememberMe">The option from user to remember.</param>
        /// <returns>Redirect to veried page with message success or fail.</returns>
        [AllowAnonymous]
        public async Task<ActionResult> VerifyFromUrl(
            string email,
            string token,
            string rememberMe)
        {
            var hasBeenVerified = await SignInManager.HasBeenVerifiedAsync();
            var queryParams = HttpContext.Request.QueryString;
            var remember = Boolean.Parse(queryParams["remember"]);

            var model = new VerifyFromURLViewModel();
            model.RememberMe = remember;
            model.RememberBrowser = false;

            // Require that the user has already logged in via username/password or external login
            if (!hasBeenVerified)
            {
                return View("Error");
            }

            var userId = await SignInManager.GetVerifiedUserIdAsync();

            if (userId == Guid.Empty)
            {
                if (!User.Identity.IsAuthenticated)
                {
                    return RedirectToAction("Login");
                }

                model.VerifiedStatus = true;
                model.VerifiedMessage = "You already verified.!";
                return RedirectToAction("VerifiedFromURL", "Account", model);
            }

            var user = await UserManager.FindByIdAsync(userId);
            var generatedToken = queryParams["t"];

            if (user.VerifiedToken == generatedToken)
            {
                // The following code protects for brute force attacks against the two factor codes.
                // If a user enters incorrect codes for a specified amount of time then the user
                // account will be locked out for a specified amount of time. You can configure the
                // account lockout settings in IdentityConfig
                var result =
                    await
                        SignInManager.TwoFactorSignInAsync(
                            "Email Code", generatedToken,
                            isPersistent: model.RememberMe,
                            rememberBrowser: model.RememberBrowser);

                switch (result)
                {
                    case SignInStatus.Success:
                        model.VerifiedStatus = true;
                        model.VerifiedMessage = "Your signing in has been successfully verified.!";
                        await SetLastLoginDateTime(userId);
                        return RedirectToAction("VerifiedFromURL", "Account", model);

                    case SignInStatus.LockedOut:
                        return View("Lockout");
                    // case SignInStatus.Failure:
                    default:
                        model.VerifiedStatus = false;
                        model.VerifiedMessage = "Your signing in verified fail.!";
                        ModelState.AddModelError(
                            "", AccountResources.VerifyCodeCtrl_InvalidCode);
                        return RedirectToAction("VerifiedFromURL", "Account", model);
                }
            }

            return
                  RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Redirect to verified from url page by refreshing
        /// </summary>
        /// <param name="model">The VerifyFromURLViewModel</param>
        /// <returns></returns>
        public ActionResult VerifiedFromURL(VerifyFromURLViewModel model)
        {
            return View(model);
        }

        // POST: /Account/VerifyCode Submit Verify Code Here
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. If a
            // user enters incorrect codes for a specified amount of time then the user account will
            // be locked out for a specified amount of time. You can configure the account lockout
            // settings in IdentityConfig
            var result =
                await
                    SignInManager.TwoFactorSignInAsync(
                        model.Provider, model.Code,
                        isPersistent: model.RememberMe,
                        rememberBrowser: model.RememberBrowser);

            switch (result)
            {
                case SignInStatus.Success:
                    var userId = await SignInManager.GetVerifiedUserIdAsync();
                    await SetLastLoginDateTime(userId);
                    return RedirectToLocal(model.ReturnUrl);

                case SignInStatus.LockedOut:
                    return View("Lockout");
                // case SignInStatus.Failure:
                default:
                    ModelState.AddModelError(
                        "", AccountResources.VerifyCodeCtrl_InvalidCode);
                    return View(model);
            }
        }

        // GET: /Account/Register
        [AllowAnonymous]
        //public async Task<ActionResult> Register(string @ref, string cons)
        public async Task<ActionResult> Register(string @ref, string cons, string id)
        {
            await InitRefferer(@ref, cons, id);
            //var referrer = id;

            var referrer = GetCookie(CageCode.ReferrerCookieKey);
            var consultant = GetCookie(CageCode.ConsultantCookieKey);

            if (string.IsNullOrEmpty(referrer))
                referrer = CageSettings.RootUser;

            if (string.IsNullOrEmpty(consultant))
                consultant = CageSettings.RootUser;

            var model = new RegisterViewModel
            {
                Referrer = referrer,
                Consultant = consultant
            };
            return View(model);
        }
        
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            if (model.ReferrerMode == Enum.GetName(typeof(CageCode.ReferrerMode), CageCode.ReferrerMode.Default))
                model.Referrer = CageSettings.RootUser;

            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user != null)
            {
                Console.Write("not empty user");
                Console.WriteLine("not empty user");
                ModelState.AddModelError("Email",
                    string.Format(AccountResources.AccountController_Register_Email_exists, model.Email));
                return View(model);
            }

            var userName = await UserManager.FindByNameAsync(model.UserName);
            if (userName != null)
            {
                Console.Write("not empty username");
                Console.WriteLine("not empty username");
                ModelState.AddModelError("Username",
                    string.Format(AccountResources.AccountController_Register_Username_exists, model.UserName));
                return View(model);
            }

            // HACK:can possible move to Cage.DAL.Db.Validators
            var referrerUser = await UserManager.FindByNameAsync(model.Referrer);
            if (referrerUser == null)
            {
                Console.Write("empty referrerUser");
                Console.WriteLine("empty referrerUser");
                ModelState.AddModelError("Referrer",
                    string.Format(AccountResources.AccountController_Register_Referrer_not_exist, model.Referrer));
                return View(model);
            }

            var referrerUserDetails = referrerUser.UserDetails;
            if (referrerUserDetails.UserType != (int)CageCode.UserType.Member)
            {
                ModelState.AddModelError("Referrer",
                    string.Format(AccountResources.AccountController_Register_Referrer_is_not_a_member, model.Referrer));
                return View(model);
            }

            // HACK:can possible move to Cage.DAL.Db.Validators
            var consultantUser = await UserManager.FindByNameAsync(model.Consultant);
            //if (consultantUser == null)
            //{
            //    ModelState.AddModelError("Consultant",
            //        string.Format(AccountResources.AccountController_Register_Consultant_not_exist, model.Consultant));
            //    return View(model);
            //}

            //var consultantUserDetails = consultantUser.UserDetails;
            //if (consultantUserDetails.UserType != (int)CageCode.UserType.Member)
            //{
            //    ModelState.AddModelError("Consultant",
            //        string.Format(AccountResources.AccountController_Register_Consultant_is_not_a_member, model.Consultant));
            //    return View(model);
            //}

            var result = await CreateNewUser(model, referrerUser, consultantUser);

            if (result.Succeeded)
            {
                return View(
                    "ConfirmationEmailSent",
                    new ConfirmationEmailSentViewModel
                    {
                        Email = model.Email
                    });
            }

            AddErrors(result);
            return View(model);
        }

        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId.IsNullOrWhiteSpace() || code == null)
            {
                return View("Error");
            }

            Guid guidUid;
            if (!Guid.TryParse(userId, out guidUid))
            {
                return View("Error");
            }

            var result = await UserManager.ConfirmEmailAsync(guidUid, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(
            ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByEmailAsync(model.Email);
                if (user == null ||
                    !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset
                // please visit http://go.microsoft.com/fwlink/?LinkID=320771
                //
                // Send an email with this link
                string code =
                    await UserManager.GeneratePasswordResetTokenAsync(user.Id);

                var callbackUrl = Url.Action(
                    "ResetPassword", "Account",
                    new { userId = user.Id, code = code },
                    protocol: Request.Url.Scheme);

                await UserManager.SendEmailAsync(
                    user.Id,
                    AccountResources.ResetPassword,
                    String.Format(
                        AccountResources.ForgotPasswordCtrl_Message,
                        callbackUrl));

                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(
            ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await UserManager.FindByEmailAsync(model.Email);

            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result =
                await
                    UserManager.ResetPasswordAsync(
                        user.Id, model.Code, model.Password);

            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }

            AddErrors(result);

            return View();
        }

        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(
                provider,
                Url.Action(
                    "ExternalLoginCallback", "Account",
                    new { ReturnUrl = returnUrl }));
        }

        /// <summary>
        /// Send verification message to client.
        /// </summary>
        /// <param name="token">The generated token.</param>
        /// <param name="returnUrl">The return url.</param>
        /// <param name="rememberMe">The remember me.</param>
        /// <returns>
        /// Redirect to page send verification message, to let's client know that the message has
        /// been sent.
        /// </returns>
        [AllowAnonymous]
        public async Task<ActionResult> SendVerificationMessage(
            string token,
            string returnUrl,
            bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == Guid.Empty)
            {
                return View("Error");
            }

            await SendVerifyURL(userId, token, rememberMe);

            return
            View(
                new SendCodeViewModel
                {
                    ReturnUrl = returnUrl,
                    RememberMe = rememberMe
                });
        }

        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(
            string returnUrl,
            bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == Guid.Empty)
            {
                return View("Error");
            }
            var userFactors =
                await UserManager.GetValidTwoFactorProvidersAsync(userId);

            var factorOptions =
                userFactors.Select(
                    purpose =>
                        new SelectListItem { Text = purpose, Value = purpose })
                    .ToList();
            return
                View(
                    new SendCodeViewModel
                    {
                        Providers = factorOptions,
                        ReturnUrl = returnUrl,
                        RememberMe = rememberMe
                    });
        }

        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (
                !await
                    SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }

            return RedirectToAction(
                "VerifyCode",
                new
                {
                    Provider = model.SelectedProvider,
                    ReturnUrl = model.ReturnUrl,
                    RememberMe = model.RememberMe
                });
        }

        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo =
                await AuthenticationManager.GetExternalLoginInfoAsync();

            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result =
                await
                    SignInManager.ExternalSignInAsync(
                        loginInfo, isPersistent: false);

            switch (result)
            {
                case SignInStatus.Success:
                    var user = await UserManager.FindByNameAsync(loginInfo.Email);
                    await SetLastLoginDateTime(user);
                    return RedirectToLocal(returnUrl);

                case SignInStatus.LockedOut:
                    return View("Lockout");

                case SignInStatus.RequiresVerification:
                    return RedirectToAction(
                        "SendCode",
                        new { ReturnUrl = returnUrl, RememberMe = false });
                // case SignInStatus.Failure:
                default:

                    bool forceInitialLocalAccount =
                        Convert.ToBoolean(
                            ConfigurationManager.AppSettings["ForceInitialLocalAccount"]);

                    if (forceInitialLocalAccount)
                    {
                        return View(
                            "UnassignedExternalLogin",
                            new UnassignedExternalLoginViewModel
                            {
                                Provider = loginInfo.Login.LoginProvider
                            });
                    }

                    // If the user does not have a local account, then prompt the user to create one
                    // - or you could do it automatically
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;

                    return View(
                        "ExternalLoginConfirmation",
                        new ExternalLoginConfirmationViewModel
                        {
                            Email = loginInfo.Email
                        });
            }
        }

        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(
            ExternalLoginConfirmationViewModel model,
            string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info =
                    await AuthenticationManager.GetExternalLoginInfoAsync();

                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }

                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                    ObjectState = ObjectState.Added,
                    Created = DateTime.UtcNow
                };

                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result =
                        await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await
                            SignInManager.SignInAsync(
                                user, isPersistent: false,
                                rememberBrowser: false);

                        return RedirectToLocal(returnUrl);
                    }
                }

                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        // POST: /Account/Logout
        [HttpPost]
        [ValidateAntiForgeryToken]
        [IgnoreAuthorizeCheckUsersHighestAuthority]
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut(
                DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToLocal(null);
        }

        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> Alias(string name)
        {
            if (name.IsNullOrWhiteSpace())
                return Json(new { alias = "" }, JsonRequestBehavior.AllowGet);

            var user = await UserManager.FindByNameAsync(name);
            if (user == null)
                return Json(new { alias = user?.UserName }, JsonRequestBehavior.AllowGet);

            var userDetails = user.UserDetails;
            if (userDetails.UserType != (int)CageCode.UserType.Member)
                return Json(new { alias = user?.UserName }, JsonRequestBehavior.AllowGet);

            return Json(new { alias = user?.UserName }, JsonRequestBehavior.AllowGet);
        }

        #region Helpers

        private async Task<DateTime?> SetLastLoginDateTime(Guid userId)
        {
            var user = await UserManager.FindByIdAsync(userId);
            return await SetLastLoginDateTime(user);
        }

        private async Task<DateTime?> SetLastLoginDateTime(ApplicationUser user)
        {
            DateTime? lastLogin = user.LastLogin;
            user.LastLogin = DateTime.UtcNow;
            await UserManager.UpdateAsync(user);
            return lastLogin;
        }

        /// <summary>
        /// Set The Lastest Generated Token To Current Login User Who Has 2 Step Verification
        /// </summary>
        /// <param name="user">ApplicationUser</param>
        /// <returns>String Token</returns>
        private async Task<string> SetLastGeneratedTokenAsync(ApplicationUser user, string provider = "Email Code")
        {
            string token = await UserManager.GenerateTwoFactorTokenAsync(user.Id, provider);
            user.VerifiedToken = token;
            await UserManager.UpdateAsync(user);
            return token;
        }

        /// <summary>
        /// Send verify url link
        /// </summary>
        /// <param name="userId">The user id</param>
        /// <param name="token">the generated token</param>
        /// <returns></returns>
        private async Task SendVerifyURL(Guid userId, string token, bool rememberMe)
        {
            string baseUrl = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority);
            var user = await UserManager.FindByIdAsync(userId);
            IdentityMessage message = new IdentityMessage();
            message.Subject = "Verification Message";
            message.Body = "Please click the url to verified your login. <br/>";
            message.Body += $"{baseUrl}/Account/VerifyFromUrl?remember={rememberMe}&e={user.Email}&t={token}";
            message.Destination = user.Email;
            await UserManager.EmailService.SendAsync(message);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }        
        
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            if (string.IsNullOrEmpty(CageSettings.LoggedInControllerName))
                return View(CageSettings.LoggedInActionName);
            else
                //return RedirectToAction("Index","Dashboard");
                return RedirectToAction(CageSettings.LoggedInActionName, CageSettings.LoggedInControllerName);
                //return RedirectToAction(CageSettings.LoggedInActionName, "Dashboard");            
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(
                string provider,
                string redirectUri,
                string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties
                {
                    RedirectUri = RedirectUri
                };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext()
                    .Authentication.Challenge(properties, LoginProvider);
            }
        }

        /*
        ** Added by Abilitation to support multiple registration points
        */

        private async Task<IdentityResult> CreateNewUser(RegisterViewModel model, ApplicationUser referrerUser, ApplicationUser consultantUser)
        {
            var user = new ApplicationUser
            {
                UserName = model.UserName,
                Email = model.Email,
                Created = DateTime.UtcNow,
                UserDetails = new ApplicationUserDetails
                {
                    ReferrerUserId = referrerUser.Id,
                    //ConsultantUserId = consultantUser.Id,
                    UserType = (int)CageCode.UserType.Member,
                    UserClass = (int)CageCode.UserClass.Grade_00
                }
            };

            user.UserUnilevel = new UserUnilevel
            {
                //UserId = user.Id,
                //UplineUserId = referrerUser.Id

                // modified by Leonard //
                UserId = user.Id.ToString(),
                UplineUserId = referrerUser.Id.ToString()
                // modified by Leonard 
            };

            var result = await UserManager.CreateAsync(user, model.Password);

            if (!result.Succeeded) return result;

            await UserManager.AddToRoleAsync(user.Id, Enum.GetName(typeof(CageCode.RolesEnum), CageCode.RolesEnum.Member));

            // Userに紐付くその他のテーブル
            //{
            //  _userDetailService.UpdateUserDetailsIncrementReferrals(referrerUser.Id);                                                           
            //}

        // Comment the following line to prevent log in until the user is confirmed.await SignInManager.SignInAsync(user,
        // isPersistent: false, rememberBrowser: false);

        // For more information on how to enable account confirmation and password reset please
        // visit http://go.microsoft.com/fwlink/?LinkID=320771

        // Send an email with this link
        var emailConfirmationToken = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);

#if DEBUG
            // ignore EmailConfirmation when debug mode is true.
            if (CageSettings.DebugMode)
            {
                await UserManager.ConfirmEmailAsync(user.Id, emailConfirmationToken);
                return result;
            }
#endif
            await
                SendEmailConfirmationTokenAsync(user);

            return result;
        }

        // See http://www.asp.net/mvc/overview/security/create-an-aspnet-mvc-5-web-app-with-email-confirmation-and-password-reset
        private async Task<string> SendEmailConfirmationTokenAsync(
            ApplicationUser user)
        {
            var userId = user.Id;
            string code =
                await UserManager.GenerateEmailConfirmationTokenAsync(userId);

            var callbackUrl = Url.Action(
                "ConfirmEmail", "Account",
                new
                {
                    userId = userId,
                    code = code
                }, protocol: Request.Url.Scheme);

            var subject = AccountResources.RegisterEmailConfirmation_Subject;
            var body = string.Format(AccountResources.RegisterEmailConfirmation_Message,
                user.UserName,
                user.Email,
                CageSettings.SiteName,
                CageSettings.SiteDomain,
                callbackUrl);

            await UserManager.SendEmailAsync(userId, subject, body);

            return callbackUrl;
        }

        #endregion Helpers
    }
}