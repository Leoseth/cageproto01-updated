﻿using System;
using System.Linq;
using System.Web.Mvc;
using Cage.Core;
using System.Web.Mvc.Html;
using Cage.DAL.Db.Models.ViewModels.Home;
using Cage.Web.Controllers.Base;
using System.Threading.Tasks;

namespace Cage.Web.Controllers
{
    [AllowAnonymous]
    public class HomeController : BaseIdentityController
    {
        public async Task<ActionResult> Index(string @ref, string cons, string id)
        {
            await InitRefferer(@ref, cons, id);

            var viewModel = new IndexViewModel
            {
                PageName = "TODO:pagename"
            };

            return View(viewModel);
        }

        [Route("about")]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        [Route("contact")]
        public ActionResult Contact()              
        {         
            ViewBag.Message = "Your contact page.";
            return View();
        }       
    }
}