﻿using Abl;
using Cage.Core;
using Cage.DAL.Repository.Infrastructure;
using Cage.DAL.Service.Interfaces;
using Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

using System.Data.Entity.Validation;

namespace Cage.Web.Controllers.Base
{
    [Authorize]
    public abstract class BaseIdentityController
        : BaseAttributesController
    {
        // Used for XSRF protection when adding external logins
        protected const string XsrfKey = "XsrfId";

        private ApplicationSignInManager _signInManager;

        #region CTORs

        protected BaseIdentityController()
        {
        }

        protected BaseIdentityController(ICageUnitOfWorkAsync unitOfWork) : base(unitOfWork)
        {
        }

        protected BaseIdentityController(
            ICageUnitOfWorkAsync unitOfWork,
            ApplicationSignInManager signInManager) : base(unitOfWork)
        {
            SignInManager = signInManager;
        }

        protected BaseIdentityController(ApplicationUserManager userManager,
            ApplicationSignInManager signInManager) : base(userManager)
        {
            this.SignInManager = signInManager;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _signInManager?.Dispose();
            }

            base.Dispose(disposing);
        }

        #endregion CTORs

        #region Properties

        protected ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ??
                       HttpContext.GetOwinContext()
                           .Get<ApplicationSignInManager>();
            }
            private set { _signInManager = value; }
        }

        protected IAuthenticationManager AuthenticationManager
            => HttpContext.GetOwinContext().Authentication;

        #endregion Properties

        protected async Task<IList<UserLoginInfo>> GetAssignedExternalLogins(
            Guid userId)
        {
            return await UserManager.GetLoginsAsync(User.Identity.GetUserId().ToGuid());
        }

        protected IList<AuthenticationDescription> GetUnassignedExternalLogins(
            IList<UserLoginInfo> userLogins)
        {
            return
                AuthenticationManager.GetExternalAuthenticationTypes()
                    .Where(
                        auth =>
                            userLogins.All(
                                ul =>
                                    auth.AuthenticationType != ul.LoginProvider))
                    .ToList();
        }

        protected async Task<bool> InitRefferer(string @ref, string cons, string id)
        {
            var referrerUser = !string.IsNullOrEmpty(@ref) ? await UserManager.FindByNameAsync(@ref) : null;
            var consultantUser = !string.IsNullOrEmpty(cons) ? await UserManager.FindByNameAsync(cons) : null;
            if (referrerUser != null)
                SetCookie(CageCode.ReferrerCookieKey, referrerUser.Id);
            if (consultantUser != null)
                SetCookie(CageCode.ConsultantCookieKey, consultantUser.Id);

            return true;
        }

        protected void SetCookie(string key, Guid userId)
        {
            Response.Cookies[key].Value = userId.ToString();
            Response.Cookies[key].Expires = DateTime.UtcNow.AddDays(30);
        }

        protected string GetCookie(string key)
        {
            if (Request.Cookies[key] != null)
            {
                return Request.Cookies[key].Value;
            }
            return string.Empty;
        }
    }
}