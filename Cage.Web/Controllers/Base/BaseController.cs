﻿using Cage.DAL.Db.Models;
using Cage.DAL.Repository.Infrastructure;
using Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NLog;
using System;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace Cage.Web.Controllers.Base
{
    public abstract class BaseController : Controller
    {
        protected static readonly NLog.ILogger Logger = LogManager.GetLogger(Assembly.GetExecutingAssembly().FullName,
            MethodBase.GetCurrentMethod().DeclaringType);

        protected readonly ICageUnitOfWorkAsync UnitOfWorkAsync;

        protected string UserId => User.Identity.GetUserId();
        protected Guid UserIdGuid => Guid.Parse(UserId);

        private ApplicationUser _applicationUser;

        protected ApplicationUser ApplicationUser
            => _applicationUser ?? (_applicationUser = UserManager.FindById(UserIdGuid));

        private ApplicationUserDetails _applicationUserDetails;

        protected ApplicationUserDetails ApplicationUserDetails
            => _applicationUserDetails ?? (_applicationUserDetails = ApplicationUser.UserDetails);

        private ApplicationUserManager _userManager;

        protected ApplicationUserManager UserManager
            => _userManager ?? (_userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>());

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _userManager?.Dispose();
            }

            base.Dispose(disposing);
        }

        protected BaseController()
        {
        }

        protected BaseController(ICageUnitOfWorkAsync unitOfWork)
        {
            UnitOfWorkAsync = unitOfWork;
        }

        protected BaseController(ApplicationUserManager userManager)
        {
            this._userManager = userManager;
        }
    }
}