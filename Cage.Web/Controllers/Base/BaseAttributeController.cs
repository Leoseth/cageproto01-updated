﻿using Cage.DAL.Repository.Infrastructure;
using Identity;
using System.Web.Mvc;

using Cage.Core;
using Cage.Web.Core;


namespace Cage.Web.Controllers.Base
{
    [Authorize]
    //[AuthorizeCheckUsersHighestAuthority(CageCode.RolesEnum.Member, "Member", "Index")]
    [AuthorizeCheckUsersHighestAuthority(CageCode.RolesEnum.Subscriber, "Dashboard", "Index")]
    [AuthorizeCheckUsersHighestAuthority(CageCode.RolesEnum.Immigrant, "Upgrade", "Paid")]
    public class BaseAttributesController : BaseController
    {
        public BaseAttributesController()
        {
        }

        public BaseAttributesController(ICageUnitOfWorkAsync unitOfWork) : base(unitOfWork)
        {
        }

        public BaseAttributesController(ApplicationUserManager userManager) : base(userManager) { }
    }

}