﻿using System.Configuration;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace Cage.Web.Services.Identity
{
    public class SmsMessagingService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Grab the Twilio account details from Web.config
            var twilioSid = ConfigurationManager
                .AppSettings["IdentityTwilioSid"];
            var twilioAuthToken = ConfigurationManager
                .AppSettings["IdentityTwilioAuthToken"];
            var twilioNumber = ConfigurationManager
                .AppSettings["IdentityTwilioNumber"];

            // Plug in your sms service here to send a text message.
            TwilioClient.Init(twilioSid, twilioAuthToken);

            MessageResource.Create(
                    @from: new PhoneNumber(twilioNumber), // From number, must be an SMS-enabled Twilio number
                    to: new PhoneNumber(message.Destination), // To number, if using Sandbox see note above
                    body: message.Body); // Message content

            return Task.FromResult(0);
        }
    }
}